#pragma once

#include "IPrediction.h"

struct Prediction : IPrediction
{
	static PREDICTION_OUTPUT GetPrediction(AIHeroClient* unit, float delay);
	static PREDICTION_OUTPUT GetPrediction(AIHeroClient* unit, float delay, float radius);
	static PREDICTION_OUTPUT GetPrediction(AIHeroClient* unit, float delay, float radius, float speed);
	static PREDICTION_OUTPUT GetPrediction(AIHeroClient* unit, float delay, float radius, float speed, std::vector<SkillshotCollision> collisionableObjects);
	static PREDICTION_OUTPUT GetPrediction(PREDICTION_INPUT input);
	static PREDICTION_OUTPUT GetStandardPrediction(PREDICTION_INPUT input);
	static PREDICTION_OUTPUT GetPositionOnPath(PREDICTION_INPUT input, std::vector<Vector3> path, float speed = -1);

	PREDICTION_OUTPUT GetPrediction(PREDICTION_INPUT input, bool ft, bool checkCollision) override;
};