#include "EventHandler.hpp"
#include "Vars.h"

std::vector<ONKEYPRESSCALLBACK> EventHandler::KeyPressHandlers{};
std::vector<RENDERSCENECALLBACK> EventHandler::GameSceneHandlers{};
std::vector<LIBRARYCALLBACK> EventHandler::LibraryCallHandlers{};
std::vector<ONAIATTACKCALLBACK> EventHandler::AIAttackHandlers{};
std::vector<ONAICASTATTACKCALLBACK> EventHandler::AICastAttackHandlers{};
std::vector<ONAIMOVECALLBACK> EventHandler::AIMoveHandlers{};
std::vector<ONAICASTATTACKCALLBACK> EventHandler::AIProcessSpellHandlers{};
std::vector<ONLOADCALLBACK> EventHandler::LoadHandlers{};
std::vector<GAMEOBJECTSCALLBACK> EventHandler::ObjectCreateHandlers{};
std::vector<GAMEOBJECTSCALLBACK> EventHandler::ObjectDeleteHandlers{};
std::vector<GAMEOBJECTSCALLBACK> EventHandler::ObjectUpdateHandlers{};
std::vector<ONUNITRECALLCALLBACK> EventHandler::AIRecallHandlers{};
std::vector<RENDERSCENECALLBACK> EventHandler::OverlaySceneHandlers{};
std::vector<ONPLAYERSHOPCALLBACK> EventHandler::LocalPlayerShopHandlers{};
std::vector<ONAIBUFFCREATEDELETECALLBACK> EventHandler::AIBuffUpdateHandlers{};
std::vector<ONPLAYERPREATTACK> EventHandler::PreAttackHandlers{};
std::vector<ONPLAYERPOSTATTACK> EventHandler::PostAttackHandlers{};
std::vector<ONPLAYERPRECAST> EventHandler::PreCastHandlers{};
std::vector<ONTICK> EventHandler::TickHandlers{};

void __cdecl EventHandler::OnKeyPress(bool KeyDown, unsigned int VirtualKey, unsigned short Repeated, unsigned char ScanCode, bool IsExtended, bool IsAltDown, bool PreviousState, bool TransitionState, void * UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	for (auto handler : KeyPressHandlers)
		handler(KeyDown, VirtualKey, Repeated, ScanCode, IsExtended, IsAltDown, PreviousState, TransitionState, nullptr);
}

void __cdecl EventHandler::OnGameScene(void * UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	for (auto handler : GameSceneHandlers)
		handler(nullptr);
}

bool __cdecl EventHandler::OnLibraryCall(const char * ImportName, unsigned int Version, void * Data, size_t * SizeOfData, void * UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	for (auto handler : LibraryCallHandlers)
		handler(ImportName, Version, Data, SizeOfData, nullptr);

	return true;
}

void __cdecl EventHandler::OnAIAttack(void * AI, void * TargetObject, bool StartAttack, bool StopAttack, void * UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	for (auto handler : AIAttackHandlers)
		handler(AI, TargetObject, StartAttack, StopAttack, nullptr);
}

void __cdecl EventHandler::OnAICastAttack(void * AI, PSDK_SPELL_CAST SpellCast, void * UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	for (auto handler : AICastAttackHandlers)
		handler(AI, SpellCast, nullptr);
}

void __cdecl EventHandler::OnAIMove(void * AI, bool Move, bool Stop, void * UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	for (auto handler : AIMoveHandlers)
		handler(AI, Move, Stop, nullptr);
}

void __cdecl EventHandler::OnAIProcessSpell(void * AI, PSDK_SPELL_CAST SpellCast, void * UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	for (auto handler : AIProcessSpellHandlers)
		handler(AI, SpellCast, nullptr);
}

void __cdecl EventHandler::OnLoad(void * UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	for (auto handler : LoadHandlers)
		handler(nullptr);
}

bool __cdecl EventHandler::OnObjectCreate(void * Object, unsigned int NetworkID, void * UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	for (auto handler : ObjectCreateHandlers)
		handler(Object, NetworkID, nullptr);

	return true;
}

bool __cdecl EventHandler::OnObjectDelete(void * Object, unsigned int NetworkID, void * UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	for (auto handler : ObjectDeleteHandlers)
		handler(Object, NetworkID, nullptr);

	return true;
}

bool __cdecl EventHandler::OnObjectUpdate(void * Object, unsigned int NetworkID, void * UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	for (auto handler : ObjectUpdateHandlers)
		handler(Object, NetworkID, nullptr);

	return true;
}

void __cdecl EventHandler::OnAIRecall(void * Unit, const char * Name, const char * Type, void * UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	for (auto handler : AIRecallHandlers)
		handler(Unit, Name, Type, nullptr);
}

void __cdecl EventHandler::OnOverlayScene(void * UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	for (auto handler : OverlaySceneHandlers)
		handler(nullptr);
}

void EventHandler::OnLocalPlayerShop(int Event, int Slot, int Extra, void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	for (auto handler : LocalPlayerShopHandlers)
		handler(Event, Slot, Extra, nullptr);
}

void EventHandler::OnAIBuffUpdate(void* AI, bool Created, unsigned char Type, float StartTime, float EndTime,
	const char* Name, void* CasterObject, unsigned CasterID, int Stacks, bool HasCount, int Count, PSDK_SPELL Spell,
	void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	for (auto handler : AIBuffUpdateHandlers)
		handler(AI, Created, Type, StartTime, EndTime, Name, CasterObject, CasterID, Stacks, HasCount, Count, Spell, nullptr);
}

std::map<unsigned int, unsigned int> g_LastTickHandlerExec;
void EventHandler::OnTickHandler(void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	const unsigned int gameTicks = pSDK->Game->GetGameTicks();
	const size_t tickFuncs = TickHandlers.size();

	for (size_t i = 0; i < tickFuncs; i++)
	{
		if (gameTicks - g_LastTickHandlerExec[i] < 30)
			continue;

		TickHandlers.at(i)();
	}
}

void EventHandler::RegisterEvent(eEventType eventType, void* function)
{
	if (eventType == eEventType::KeyPress)
	{
		KeyPressHandlers.push_back(static_cast<ONKEYPRESSCALLBACK>(function));
		if (KeyPressHandlers.size() == 1)
			gSdkContext->_SdkRegisterOnKeyPress(OnKeyPress, nullptr);
	}
	else if (eventType == eEventType::GameScene)
	{
		GameSceneHandlers.push_back(static_cast<RENDERSCENECALLBACK>(function));
		if (GameSceneHandlers.size() == 1)
			gSdkContext->_SdkRegisterGameScene(OnGameScene, nullptr);
	}
	else if (eventType == eEventType::LibraryCall)
	{
		LibraryCallHandlers.push_back(static_cast<LIBRARYCALLBACK>(function));
		if (LibraryCallHandlers.size() == 1)
			gSdkContext->_SdkRegisterLibrary(OnLibraryCall, nullptr);
	}
	else if (eventType == eEventType::AIAttack)
	{
		AIAttackHandlers.push_back(static_cast<ONAIATTACKCALLBACK>(function));
		if (AIAttackHandlers.size() == 1)
			gSdkContext->_SdkRegisterOnAIAttack(OnAIAttack, nullptr);
	}
	else if (eventType == eEventType::AICastAttack)
	{
		AICastAttackHandlers.push_back(static_cast<ONAICASTATTACKCALLBACK>(function));
		if (AICastAttackHandlers.size() == 1)
			gSdkContext->_SdkRegisterOnAICastAttack(OnAICastAttack, nullptr);
	}
	else if (eventType == eEventType::AIMove)
	{
		AIMoveHandlers.push_back(static_cast<ONAIMOVECALLBACK>(function));
		if (AIMoveHandlers.size() == 1)
			gSdkContext->_SdkRegisterOnAIMove(OnAIMove, nullptr);
	}
	else if (eventType == eEventType::AIProcessSpell)
	{
		AIProcessSpellHandlers.push_back(static_cast<ONAICASTATTACKCALLBACK>(function));
		if (AIProcessSpellHandlers.size() == 1)
			gSdkContext->_SdkRegisterOnAIProcessSpell(OnAIProcessSpell, nullptr);
	}
	else if (eventType == eEventType::Load)
	{
		LoadHandlers.push_back(static_cast<ONLOADCALLBACK>(function));
		if (LoadHandlers.size() == 1)
			gSdkContext->_SdkRegisterOnLoad(OnLoad, nullptr);
	}
	else if (eventType == eEventType::ObjectCreate)
	{
		ObjectCreateHandlers.push_back(static_cast<GAMEOBJECTSCALLBACK>(function));
		if (ObjectCreateHandlers.size() == 1)
			gSdkContext->_SdkRegisterOnObjectCreate(OnObjectCreate, nullptr);
	}
	else if (eventType == eEventType::ObjectDelete)
	{
		ObjectDeleteHandlers.push_back(static_cast<GAMEOBJECTSCALLBACK>(function));
		if (ObjectDeleteHandlers.size() == 1)
			gSdkContext->_SdkRegisterOnObjectDelete(OnObjectDelete, nullptr);
	}
	else if (eventType == eEventType::ObjectUpdate)
	{
		ObjectUpdateHandlers.push_back(static_cast<GAMEOBJECTSCALLBACK>(function));
		if (ObjectUpdateHandlers.size() == 1)
			gSdkContext->_SdkRegisterOnObjectUpdate(OnObjectUpdate, nullptr);
	}
	else if (eventType == eEventType::AIRecall)
	{
		AIRecallHandlers.push_back(static_cast<ONUNITRECALLCALLBACK>(function));
		if (AIRecallHandlers.size() == 1)
			gSdkContext->_SdkRegisterOnUnitRecall(OnAIRecall, nullptr);
	}
	else if (eventType == eEventType::OverlayScene)
	{
		OverlaySceneHandlers.push_back(static_cast<RENDERSCENECALLBACK>(function));
		if (OverlaySceneHandlers.size() == 1)
			gSdkContext->_SdkRegisterOverlayScene(OnOverlayScene, nullptr);
	}
	else if (eventType == eEventType::LocalPlayerShop)
	{
		LocalPlayerShopHandlers.push_back(static_cast<ONPLAYERSHOPCALLBACK>(function));
		if (LocalPlayerShopHandlers.size() == 1)
			gSdkContext->_SdkRegisterOnShopLocalPlayer(OnLocalPlayerShop, nullptr);
	}
	else if (eventType == eEventType::AIBuffUpdate)
	{
		AIBuffUpdateHandlers.push_back(static_cast<ONAIBUFFCREATEDELETECALLBACK>(function));
		if (AIBuffUpdateHandlers.size() == 1)
			gSdkContext->_SdkRegisterOnBuffCreateDelete(OnAIBuffUpdate, nullptr);
	}
	else if (eventType == eEventType::PreAttack)
	{
		PreAttackHandlers.push_back(static_cast<ONPLAYERPREATTACK>(function));
	}
	else if (eventType == eEventType::PostAttack)
	{
		PostAttackHandlers.push_back(static_cast<ONPLAYERPOSTATTACK>(function));
	}
	else if (eventType == eEventType::PreCast)
	{
		PreCastHandlers.push_back(static_cast<ONPLAYERPRECAST>(function));
	}
	else if (eventType == eEventType::Tick)
	{
		TickHandlers.push_back(static_cast<ONTICK>(function));
		if (TickHandlers.size() == 1)
			RegisterEvent(eEventType::GameScene, OnTickHandler);
	}
}

void EventHandler::Initialize()
{
	// NO NEED FOR THIS, BUT KEEPING IT FOR POSSIBLE FUTURE UPDATES
}