#pragma once

#include <Windows.h>
#include "../sdkapi.h"
#include <cmath>
#include <vector>
#include "GameObjects.h"

struct IEntityManager
{
	virtual std::shared_ptr<AIClient> GetAIByID(unsigned int netID) = 0;
	virtual std::shared_ptr<AIClient> GetAIByObject(void* object) = 0;

	virtual std::vector<std::shared_ptr<AIClient>> GetMinions(float range, bool ally, bool enemy) = 0;
	virtual std::vector<std::shared_ptr<AIClient>> GetLaneMinions(float range, bool ally, bool enemy) = 0;
	virtual std::vector<std::shared_ptr<AIClient>> GetJungle(float range) = 0;
	virtual std::vector<std::shared_ptr<AIHeroClient>> GetHeroes(float range, bool ally, bool enemy) = 0;
	virtual std::vector<std::shared_ptr<AITurretClient>> GetTurrets(float range, bool ally, bool enemy) = 0;
	virtual std::vector<std::shared_ptr<AIClient>> GetInhibitors(float range, bool ally, bool enemy) = 0;
	virtual std::vector<std::shared_ptr<AIClient>> GetNexus(float range, bool ally, bool enemy) = 0;
};
