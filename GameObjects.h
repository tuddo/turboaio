#pragma once

#include <Windows.h>
#include "../sdkapi.h"
#include "Math.hpp"
#include <string>
#include <vector>

struct sDamageInfo
{
	float PhysicalDamage;
	float MagicalDamage;
	float TrueDamage;

	float GetTotal() const { return PhysicalDamage + MagicalDamage + TrueDamage; }
};

struct GameObject
{
	// Static informations
	void* Object = nullptr;

	// Functions
	virtual Vector3 GetAcceleration();
	virtual Vector3 GetPosition();
	virtual std::string GetName();

	// Type checks
	virtual bool IsAI();
	virtual bool IsMinion();
	virtual bool IsTurret();
	virtual bool IsHero();
	virtual bool IsLaneMinion();
	virtual bool IsMonster();
};

enum eDamageType
{
	Physical,
	Magical,
	True
};
struct sNavData
{
	SDKVECTOR StartPosition;
	SDKVECTOR EndPosition;
	size_t NextWaypoint;
	size_t NumberOfWaypoints;
	SDKVECTOR* Waypoints;
	SDKVECTOR Velocity;
	bool IsDash;
	float DashSpeed;
	float DashGravity;

	float Length() const
	{
		float length = .0f;

		if (NumberOfWaypoints < 1)
			return 0.f;

		for (size_t i = 0; i < NumberOfWaypoints - 1; i++)
		{
			const SDKVECTOR firstPath = Waypoints[i];
			const SDKVECTOR secondPath = Waypoints[i + 1];

			const float distance = sqrt(powf(abs(secondPath.x - firstPath.x), 2) + powf(fabs(secondPath.y - firstPath.y), 2));
			length += distance;
		}

		return length;
	}
};
struct lsDamageProperty
{
	float Base;
	float Bonus;
	float Total;
};
struct lsPenetrationProperty
{
	float Flat;
	float Percent;
};
struct sBuffInfo
{
	int Type;
	std::string Name;
	int Count;
	int Stacks;
	float StartTime;
	float EndTime;
};
struct AIClient : GameObject
{
	//// Basic properties
	// Turbo basic properties
	virtual SDK_HEALTH GetHealth();
	virtual SDK_ABILITY_RESOURCE GetAbilityResource();
	virtual lsPenetrationProperty GetArmorPenetration();
	virtual lsPenetrationProperty GetMagicPenetration();
	virtual float GetArmor();
	virtual float GetMagicResist();
	virtual float GetAttackRange();
	virtual float GetRealAttackRange();
	virtual float GetAttackDelay();
	virtual float GetAttackCastDelay();
	virtual lsDamageProperty GetAttackDamage();
	virtual float GetAbilityPower();
	virtual float GetCriticalStrike();
	virtual std::string GetAIName();
	virtual int GetTypeID();
	virtual int GetTypeFlags();
	virtual int GetTeamID();
	virtual int GetTargetability();
	virtual float GetMovementSpeed();
	virtual sNavData GetNavData();
	virtual float GetBoundingRadius();
	virtual SDK_SPELL GetBasicAttack();
	virtual int GetLevel();
	virtual unsigned int GetNetworkID();
	virtual Vector3 GetServerPosition();
	virtual int GetProjectileSpeed();
	virtual float GetAttackSpeed();

	// Damage calculations based on properties
	virtual float GetPhysicalDamage(const std::shared_ptr<AIClient>& target, float damage);
	virtual float GetMagicalDamage(const std::shared_ptr<AIClient>& target, float damage);

	//// Object checks
	// Basic checks
	virtual bool IsDead();
	virtual bool IsVisible();
	virtual bool IsVisibleOnScreen();
	virtual bool IsInvulnerable();
	virtual bool IsAlly();
	virtual bool IsEnemy();
	virtual bool IsMoving();

	// Range checks
	virtual bool IsMelee();
	virtual bool IsRanged();
	virtual bool IsValid(float range, bool teamCheck = true, Vector3 from = Vector3());

	// Crowd control checks
	virtual bool IsStunned();
	virtual bool IsSnared();
	virtual bool IsAsleep();
	virtual bool IsKnockedUp();
	virtual bool IsCharmed();
	virtual bool IsFeared();
	virtual bool IsTaunted();
	virtual bool IsSilenced();
	virtual bool IsBlinded();
	virtual bool IsRooted();
	virtual bool IsEntangled();
	virtual bool IsSlowed();
	virtual bool IsCrippled();
	virtual bool IsDisarmed();
	virtual bool IsSuppressed();
	virtual bool IsInStasis();

	// Space oriented things
	virtual float GetDistance(Vector3 target);
	virtual float GetDistanceSquared(Vector3 target);

	// Others
	virtual sBuffInfo GetBuff(std::string buffName);
	virtual std::vector<sBuffInfo> GetBuffs();
	virtual bool HasBuff(std::string buffName);
	virtual bool HasBuffType(int buffType);
	virtual bool HasItem(int itemID);
	virtual std::vector<SDK_ITEM> GetItems();
	virtual bool Compare(const std::shared_ptr<AIClient>& object);
};

struct AIHeroClient : AIClient
{
	// Properties
	virtual float GetLethality();

	// Spells
	virtual bool CanCastSpell(int spellSlot);
	virtual bool CanCastSpellOnTarget(int spellSlot, const std::shared_ptr<AIClient>& target);
	virtual SDK_SPELL GetSpell(int spellSlot);
};

struct AILocalPlayer : AIHeroClient
{
	int TeamID;

	// Spells
	virtual void CastSpell(int spellSlot);
	virtual void CastSpell(int spellSlot, const std::shared_ptr<AIClient>& target);
	virtual void CastSpell(int spellSlot, Vector3 targetLocation);
	virtual void Attack(const std::shared_ptr<AIClient>& target, bool pet = false);

	// Damage calculations
	virtual sDamageInfo GetAutoAttackDamage(const std::shared_ptr<AIClient>& target);
	virtual float GetTimeToHit(const std::shared_ptr<AIClient>& target);
};

enum class eTurretType : int
{
	None,
	Outer,
	Inner,
	Inhibitor,
	Nexus,
	Fountain
};
struct AITurretClient : AIClient
{
	virtual eTurretType GetTurretType();
};