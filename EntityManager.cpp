#include "EntityManager.h"
#include "Vars.h"
#include "ObjectManager.hpp"
#include <algorithm>
#include "Damage.hpp"

std::shared_ptr<AIClient> EntityManager::GetAIByID(unsigned int netID)
{
	auto laneMinions = GetMinions(30000, true, true);
	const auto laneMinionIter = std::find_if(laneMinions.begin(), laneMinions.end(), [netID](std::shared_ptr<AIClient> laneMinion)
	{
		return laneMinion->GetNetworkID() == netID;
	});
	if (laneMinionIter != laneMinions.end())
		return *laneMinionIter;

	auto jungle = GetJungle(30000);
	const auto jungleMinionIter = std::find_if(jungle.begin(), jungle.end(), [netID](std::shared_ptr<AIClient> jungleMinion)
	{
		return jungleMinion->GetNetworkID() == netID;
	});
	if (jungleMinionIter != jungle.end())
		return *jungleMinionIter;

	auto heroes = GetHeroes(30000, true, true);
	const auto heroIter = std::find_if(heroes.begin(), heroes.end(), [netID](std::shared_ptr<AIHeroClient> hero)
	{
		return hero->GetNetworkID() == netID;
	});
	if (heroIter != heroes.end())
		return *heroIter;

	auto turrets = GetTurrets(30000, true, true);
	const auto turretIter = std::find_if(turrets.begin(), turrets.end(), [netID](std::shared_ptr<AITurretClient> turret)
	{
		return turret->GetNetworkID() == netID;
	});
	if (turretIter != turrets.end())
		return *turretIter;

	return nullptr;
}

std::shared_ptr<AIClient> EntityManager::GetAIByObject(void * object)
{
	int objectType;
	gSdkContext->_SdkGetObjectTypeID(object, &objectType);

	if (objectType == ObjectType::typeAIMinion)
	{
		auto laneMinions = GetMinions(30000, true, true);
		const auto laneMinionIter = std::find_if(laneMinions.begin(), laneMinions.end(), [object](std::shared_ptr<AIClient> laneMinion)
		{
			return laneMinion->Object == object;
		});
		if (laneMinionIter != laneMinions.end())
			return *laneMinionIter;

		auto jungle = GetJungle(30000);
		const auto jungleMinionIter = std::find_if(jungle.begin(), jungle.end(), [object](std::shared_ptr<AIClient> jungleMinion)
		{
			return jungleMinion->Object == object;
		});
		if (jungleMinionIter != jungle.end())
			return *jungleMinionIter;
	}
	else if (objectType == ObjectType::typeAIHeroClient)
	{
		auto heroes = GetHeroes(30000, true, true);
		const auto heroIter = std::find_if(heroes.begin(), heroes.end(), [object](std::shared_ptr<AIHeroClient> hero)
		{
			return hero->Object == object;
		});
		if (heroIter != heroes.end())
			return *heroIter;
	}
	else if (objectType == ObjectType::typeAITurret)
	{
		auto turrets = GetTurrets(30000, true, true);
		const auto turretIter = std::find_if(turrets.begin(), turrets.end(), [object](std::shared_ptr<AITurretClient> turret)
		{
			return turret->Object == object;
		});
		if (turretIter != turrets.end())
			return *turretIter;
	}
	else if (objectType == ObjectType::typeObjBarracksDampener)
	{
		auto inhibitors = GetInhibitors(30000, true, true);
		const auto inhibitorIter = std::find_if(inhibitors.begin(), inhibitors.end(), [object](std::shared_ptr<AIClient> inhibitor)
		{
			return inhibitor->Object == object;
		});
		if (inhibitorIter != inhibitors.end())
			return *inhibitorIter;
	}
	else if (objectType == ObjectType::typeObjHQ)
	{
		auto nexuses = GetNexus(30000, true, true);
		const auto nexusIter = std::find_if(nexuses.begin(), nexuses.end(), [object](std::shared_ptr<AIClient> nexus)
		{
			return nexus->Object == object;
		});
		if (nexusIter != nexuses.end())
			return *nexusIter;
	}

	return nullptr;
}

std::vector<std::shared_ptr<AIClient>> EntityManager::GetMinions(float range, bool ally, bool enemy) { return ObjectManager::GetMinions(ally, enemy, range); }
std::vector<std::shared_ptr<AIClient>> EntityManager::GetLaneMinions(float range, bool ally, bool enemy) { return ObjectManager::GetLaneMinions(ally, enemy, range); }
std::vector<std::shared_ptr<AIClient>> EntityManager::GetJungle(float range) { return ObjectManager::GetJungle(range); }
std::vector<std::shared_ptr<AIHeroClient>> EntityManager::GetHeroes(float range, bool ally, bool enemy) { return ObjectManager::GetHeroes(ally, enemy, range); }
std::vector<std::shared_ptr<AITurretClient>> EntityManager::GetTurrets(float range, bool ally, bool enemy) { return ObjectManager::GetTurrets(ally, enemy, range); }
std::vector<std::shared_ptr<AIClient>> EntityManager::GetInhibitors(float range, bool ally, bool enemy) { return ObjectManager::GetInhibitors(ally, enemy, range); }
std::vector<std::shared_ptr<AIClient>> EntityManager::GetNexus(float range, bool ally, bool enemy) { return ObjectManager::GetNexus(ally, enemy, range); }