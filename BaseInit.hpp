#pragma once

// Base Interfaces
#include "IMenu.h"
#include "IOrbwalker.h"
#include "IEntityManager.h"
#include "ITargetSelector.h"
#include "IPrediction.h"
#include "IHealthPrediction.h"
#include "IGame.h"
#include "IEventHandler.h"
#include "ICollision.h"

using namespace std;

struct sSDK
{
	// SDK Interfaces
	shared_ptr<IMenu> Menu = nullptr;
	shared_ptr<IOrbwalker> Orbwalker = nullptr;
	shared_ptr<IEntityManager> EntityManager = nullptr;
	shared_ptr<AILocalPlayer> Player = nullptr;
	shared_ptr<ITargetSelector> TargetSelector = nullptr;
	shared_ptr<IPrediction> Prediction = nullptr;
	shared_ptr<IHealthPrediction> HealthPrediction = nullptr;
	shared_ptr<IGame> Game = nullptr;
	shared_ptr<IEventHandler> EventHandler = nullptr;
	shared_ptr<ICollision> Collision = nullptr;
};

class BaseInit
{
public:
	static void OnDraw(void* UserData);

	static void Initialize(PSDK_CONTEXT context);
};