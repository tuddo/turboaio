#pragma once

#include "IEntityManager.h"
#include "Math.hpp"

enum class eEventType : int
{
	KeyPress,
	GameScene,
	LibraryCall,
	AIAttack,
	AICastAttack,
	AIMove,
	AIProcessSpell,
	Load,
	ObjectCreate,
	ObjectDelete,
	ObjectUpdate,
	AIRecall,
	OverlayScene,
	LocalPlayerShop,
	AIBuffUpdate,
	PreAttack,
	PostAttack,
	PreCast,
	Tick
};

typedef void(*ONPLAYERPREATTACK)(std::shared_ptr<AIClient> Target);
typedef void(*ONPLAYERPOSTATTACK)(std::shared_ptr<AIClient> Target);
typedef void(*ONPLAYERPRECAST)(std::shared_ptr<AIClient> Target, Vector3 Position, SDK_SPELL Spell);
typedef void(*ONTICK)();

struct IEventHandler
{
	virtual void RegisterEvent(eEventType eventType, void* function) = 0;
};