#pragma once

#include "IHealthPrediction.h"
#include "IEntityManager.h"
#include <map>
#include <forward_list>

using namespace std;

enum class AttackState : int
{
	None,
	Detected,
	Completed
};

struct AutoAttack
{
	double Damage;
	int SNetworkId;
	int NetworkId;
	int MNetworkId;
	shared_ptr<AIClient> Sender;
	shared_ptr<AIClient> Target;
	int AnimationDelay;
	Vector3 StartPosition;
	int DetectTime;
	int ExtraDelay;
	AttackState AttackStatus;

	AutoAttack(shared_ptr<AIClient> sender, shared_ptr<AIClient> target);

	bool CanRemoveAttack() const;
	float Distance() const;
	int ElapsedTime() const;
	int ETA();

	virtual bool IsValid();
	virtual bool HasReached();
	virtual int LandTime();
};

struct MeleeAttack : AutoAttack
{
	MeleeAttack(shared_ptr<AIClient> sender, shared_ptr<AIClient> target, int extraDelay);

	bool HasReached() override;
	int LandTime() override;
};

struct RangedAttack : AutoAttack
{
	void* Missile;
	float MissileSpeed;

	RangedAttack(shared_ptr<AIClient> sender, shared_ptr<AIClient> target, void* mc, int extraDelay);

	bool IsValid() override;
	bool HasReached() override;
	int LandTime() override;
	int TimeToLand();
	Vector3 EstimatedPosition();
};

struct Attacks
{
	static map<int, forward_list<shared_ptr<AutoAttack>>> InboundAttacks;

	static void AddMeleeAttack(const shared_ptr<AIClient>& sender, const shared_ptr<AIClient>& target, int extraDelay);
	static void AddRangedAttack(const shared_ptr<AIClient>& sender, const shared_ptr<AIClient>& target, void* mc, int extraDelay);
	static forward_list<shared_ptr<AutoAttack>>& GetInboundAttacks(int unitId);
};

struct HealthPrediction : IHealthPrediction
{
private:
	static float GetAngle(Vector2 v1, Vector2 v2);
public:
	static void OnProcessAutoAttack(void* AI, void* TargetObject, bool StartAttack, bool StopAttack, void* UserData);
	static void OnObjectCreate(void* Object, unsigned int NetworkID, void* UserData);
	static void OnObjectDelete(void* Object, unsigned int NetworkID, void* UserData);
	static void OnUpdate();
	static void OnAICastAttack(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData);

	static void Initialize();

	static int TimeForAutoToReachTarget(shared_ptr<AIClient> Target);
	static float CalculateMissileTravelTime(shared_ptr<AIClient> Target, Vector2 SourcePosition, float MissileDelay, float MissileSpeed);
	static float GetHealthPredictionLaneClear(std::shared_ptr<AIClient> AI, int Time);

	static float GetPrediction(std::shared_ptr<AIClient> AI, int Time);

	float GetHealthPrediction(std::shared_ptr<AIClient> AI, int Time) override;
};