//
#include <Windows.h>
#include "Initializer.hpp"
#include "Menu.hpp"
#include "ImGui/imgui.h"
#include "ImGui/imgui_impl_dx9.h"
#include "BaseInit.hpp"
#include "HealthPrediction.hpp"
#include "TargetSelector.hpp"
#include "Orbwalker.hpp"

HWND Initializer::GameHWND = nullptr;
WNDPROC Initializer::OldWND = nullptr;

extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

LRESULT CALLBACK NewWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam))
	{
		if (Menu::DrawWindow)
			return DefWindowProc(hWnd, msg, wParam, lParam);
	}

	return CallWindowProc(Initializer::OldWND, hWnd, msg, wParam, lParam);
}

void Initializer::Initialize(PSDK_CONTEXT context, HINSTANCE hinstDLL)
{
	UNREFERENCED_PARAMETER(hinstDLL);

	context->_SdkUiGetWindow(&GameHWND);

	void* pDevice;
	context->_SdkGetD3DDevice(&pDevice);

	// Initialize ImGui
	ImGuiIO& io = ImGui::GetIO();
	io.DeltaTime = 1.f / 60.f;
	io.IniFilename = nullptr;
	io.Fonts->AddFontFromFileTTF(R"(C:\Windows\Fonts\Arialbd.ttf)", 14.f);
	ImGui_ImplDX9_Init(Initializer::GameHWND, static_cast<IDirect3DDevice9*>(pDevice));

	OldWND = reinterpret_cast<WNDPROC>(SetWindowLongPtr(GameHWND, GWL_WNDPROC, static_cast<LONG>(reinterpret_cast<LONG_PTR>(NewWndProc))));

	BaseInit::Initialize(context);
	Menu::Initialize();
	HealthPrediction::Initialize();
	Orbwalker::Initialize();
	//Activator::Initialize();

	//Twitch::Initialize();
}