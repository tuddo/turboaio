#include "HealthPrediction.hpp"
#include "Vars.h"
#include "cpplinq.hpp"
#include "EventHandler.hpp"
#include "Pathing.hpp"
#include <experimental/vector>
#include "Color.h"

AutoAttack::AutoAttack(shared_ptr<AIClient> sender, shared_ptr<AIClient> target)
{
	this->AttackStatus = AttackState::Detected;
	this->DetectTime = pSDK->Game->GetGameTicks() - pSDK->Game->GetPing() / 2;
	this->Sender = sender;
	this->Target = target;
	this->SNetworkId = sender->GetNetworkID();
	this->NetworkId = target->GetNetworkID();
	this->MNetworkId = 0;

	this->Damage = sender->GetPhysicalDamage(target, sender->GetAttackDamage().Total);
	this->StartPosition = sender->GetServerPosition();
	this->AnimationDelay = static_cast<int>(sender->GetAttackCastDelay() * 1000.f);
}

bool AutoAttack::CanRemoveAttack() const
{
	return pSDK->Game->GetGameTicks() - this->DetectTime > 5000;
}

float AutoAttack::Distance() const
{
	return this->StartPosition.GetDistance(this->Target->GetPosition()) - this->Sender->GetBoundingRadius() - this->Target->GetBoundingRadius();
}

int AutoAttack::ETA()
{
	return this->LandTime() - pSDK->Game->GetGameTicks();
}

bool AutoAttack::IsValid()
{
	if (Sender != nullptr && Target != nullptr)
	{
		return !this->Sender->IsDead() && this->Sender->IsVisible() && !this->Target->IsDead() && this->Target->IsVisible();
	}

	return false;
}

bool AutoAttack::HasReached()
{
	return false;
}

int AutoAttack::LandTime()
{
	return 0;
}

int AutoAttack::ElapsedTime() const
{
	return pSDK->Game->GetGameTicks() - this->DetectTime;
}

MeleeAttack::MeleeAttack(shared_ptr<AIClient> sender, shared_ptr<AIClient> target, int extraDelay) : AutoAttack(sender, target)
{
	this->ExtraDelay = extraDelay + 100;
	this->MNetworkId = 0;
}

bool MeleeAttack::HasReached()
{
	return this->AttackStatus == AttackState::Completed || !this->IsValid() || this->ETA() < -100;
}

int MeleeAttack::LandTime()
{
	return this->DetectTime + static_cast<int>(this->Sender->GetAttackCastDelay() * 1000.f) + this->ExtraDelay;
}

RangedAttack::RangedAttack(shared_ptr<AIClient> sender, shared_ptr<AIClient> target, void* mc, int extraDelay) : AutoAttack(sender, target)
{
	this->ExtraDelay = extraDelay;
	this->Missile = mc;

	SDKVECTOR missileStartPosition;
	gSdkContext->_SdkGetMissileStartPosition(mc, &missileStartPosition);
	this->StartPosition = missileStartPosition;

	this->MissileSpeed = .0f;
	gSdkContext->_SdkGetMissileSpeed(this->Missile, &this->MissileSpeed);

	unsigned int misNetID;
	gSdkContext->_SdkGetObjectNetworkID(this->Missile, &misNetID);
	this->MNetworkId = misNetID;
}

bool RangedAttack::IsValid()
{
	if (Missile != nullptr && Sender != nullptr && Target != nullptr)
	{
		const bool isMissile = SDKSTATUS_SUCCESS(gSdkContext->_SdkIsObjectSpellMissile(this->Missile));
		return isMissile && !this->Sender->IsDead() && this->Sender->IsVisible() && !this->Target->IsDead() && this->Target->IsVisible();
	}
	return false;
}

bool RangedAttack::HasReached()
{
	return this->Missile == nullptr || !this->IsValid() || this->AttackStatus == AttackState::Completed || this->ETA() <
		-200;
}

int RangedAttack::LandTime()
{
	return pSDK->Game->GetGameTicks() + this->TimeToLand();
}

int RangedAttack::TimeToLand()
{
	if (!this->IsValid()) return 0;

	SDKVECTOR missilePosition;
	gSdkContext->_SdkGetObjectPosition(this->Missile, &missilePosition);

	SDKVECTOR targetPosition;
	gSdkContext->_SdkGetMissileTarget(this->Missile, &targetPosition, nullptr, nullptr);

	return static_cast<int>(Vector3(missilePosition).GetDistance(targetPosition) / MissileSpeed * 1000);
}

Vector3 RangedAttack::EstimatedPosition()
{
	float missileSpeed;
	gSdkContext->_SdkGetMissileSpeed(this->Missile, &missileSpeed);
	if (missileSpeed == 0)
		missileSpeed = FLT_MAX;

	SDKVECTOR targetPosition;
	void* targetObject;
	gSdkContext->_SdkGetMissileTarget(this->Missile, &targetPosition, &targetObject, nullptr);

	if (targetPosition.x == 0 && targetPosition.y == 0 && targetPosition.z == 0)
	{
		if (targetObject)
			gSdkContext->_SdkGetAIServerPosition(targetObject, &targetPosition);
	}

	const auto dist = missileSpeed / 1000 * (pSDK->Game->GetGameTicks() - this->DetectTime);
	return this->StartPosition.Extend(Vector3(targetPosition), dist);
}

map<int, forward_list<shared_ptr<AutoAttack>>> Attacks::InboundAttacks{};

void Attacks::AddMeleeAttack(const shared_ptr<AIClient>& sender, const shared_ptr<AIClient>& target, int extraDelay)
{
	const int k = target->GetNetworkID();
	if (InboundAttacks.find(k) == InboundAttacks.end())
	{
		InboundAttacks[k] = forward_list<shared_ptr<AutoAttack>>();
	}
	InboundAttacks[k].push_front(make_shared<MeleeAttack>(sender, target, extraDelay));
}

shared_ptr<AutoAttack> LastCreated;
void Attacks::AddRangedAttack(const shared_ptr<AIClient>& sender, const shared_ptr<AIClient>& target, void* mc, int extraDelay)
{
	const int k = target->GetNetworkID();
	if (InboundAttacks.find(k) == InboundAttacks.end())
	{
		InboundAttacks[k] = forward_list<shared_ptr<AutoAttack>>();
	}
	
	shared_ptr<AutoAttack> kek = make_shared<RangedAttack>(sender, target, mc, extraDelay);
	LastCreated = kek;
	InboundAttacks[k].push_front(kek);
}

forward_list<shared_ptr<AutoAttack>>& Attacks::GetInboundAttacks(int unitId)
{
	return InboundAttacks[unitId];
}

float HealthPrediction::GetAngle(Vector2 v1, Vector2 v2)
{
	return static_cast<float>(acos(
		(v1.X * v2.X + v1.Y * v2.Y) / (sqrt(v1.X * v1.X + v1.Y * v1.Y)
			* sqrt(v2.X * v2.X + v2.Y * v2.Y))));
}

void HealthPrediction::OnProcessAutoAttack(void* AI, void* TargetObject, bool StartAttack, bool StopAttack,
	void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);
	UNREFERENCED_PARAMETER(StopAttack);

	if (!AI)
		return;
	if (!TargetObject)
		return;

	if (StartAttack)
	{
		const auto Sender = pSDK->EntityManager->GetAIByObject(AI);
		if (Sender == nullptr || Sender->IsDead() || !Sender->IsVisible())
		{
			return;
		}
		const auto Target = pSDK->EntityManager->GetAIByObject(TargetObject);
		if (Target == nullptr)
		{
			return;
		}

		if (Sender->GetPosition().GetDistance(pSDK->Player->GetPosition()) > 4000.f || !Sender->IsMelee() || Sender->IsHero())
		{
			return;
		}

		if (Target->IsMinion())
		{
			Attacks::AddMeleeAttack(Sender, Target, 0);
		}
	}
}

void HealthPrediction::OnObjectCreate(void* Object, unsigned NetworkID, void* UserData)
{
	UNREFERENCED_PARAMETER(NetworkID);
	UNREFERENCED_PARAMETER(UserData);

	if (!Object)
		return;

	if (SDKSTATUS_SUCCESS(gSdkContext->_SdkIsObjectSpellMissile(Object)))
	{
		void* missileCaster;
		void* missileTarget;

		gSdkContext->_SdkGetMissileCaster(Object, &missileCaster, nullptr);
		gSdkContext->_SdkGetMissileTarget(Object, nullptr, &missileTarget, nullptr);

		if (!missileCaster || !missileTarget)
			return;

		const auto Sender = pSDK->EntityManager->GetAIByObject(missileCaster);
		const auto Target = pSDK->EntityManager->GetAIByObject(missileTarget);
		if (Sender == nullptr || Target == nullptr)
		{
			return;
		}
		if (Sender->IsDead() || !Sender->IsVisible() || Target->IsDead() || !Target->IsVisible() || Sender->GetNetworkID() == pSDK->Player->GetNetworkID() || Sender->GetPosition().GetDistance(pSDK->Player->GetPosition()) > 4000.f)
		{
			return;
		}

		Attacks::AddRangedAttack(Sender, Target, Object, 0);
		//gSdkContext->_SdkConsoleWrite("HasReached => %d | IsValid => %d | Detect Diff => %d | CanBeRemoved => %d | ETA => %d ||||| Sender->IsMelee => %d | Sender->MissileSpeed => %.02f", LastCreated->HasReached(), LastCreated->IsValid(), pSDK->Game->GetGameTicks() - LastCreated->DetectTime, LastCreated->CanRemoveAttack(), LastCreated->ETA(), LastCreated->Sender->IsMelee(), LastCreated->Sender->GetBasicAttack().MissileSpeed);
	}
}

void HealthPrediction::OnObjectDelete(void* Object, unsigned NetworkID, void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	if (!Object)
		return;

	Attacks::InboundAttacks.erase(NetworkID);

	if (SDKSTATUS_SUCCESS(gSdkContext->_SdkIsObjectSpellMissile(Object)))
	{
		void* missileCaster;
		void* missileTarget;

		gSdkContext->_SdkGetMissileCaster(Object, &missileCaster, nullptr);
		gSdkContext->_SdkGetMissileTarget(Object, nullptr, &missileTarget, nullptr);

		if (!missileCaster || !missileTarget)
			return;

		const auto Sender = pSDK->EntityManager->GetAIByObject(missileCaster);
		if (Sender == nullptr || Sender->IsMelee() || Sender->IsHero())
			return;
		const auto Target = pSDK->EntityManager->GetAIByObject(missileTarget);
		if (Target == nullptr)
			return;

		for (auto& attack : Attacks::InboundAttacks[Target->GetNetworkID()])
		{
			if (attack->MNetworkId == static_cast<int>(NetworkID))
				attack->AttackStatus = AttackState::Completed;
		}
	}
}

int g_LastClean;
void HealthPrediction::OnUpdate()
{
	const int gameTicks = pSDK->Game->GetGameTicks();

	////// TEST HPRED INFO
	/*for (auto& entity : pSDK->EntityManager->GetLaneMinions(4000.f, false, true))
	{
		if (!entity->IsValid(4000.f))
			continue;

		char buff[30];

		sprintf(buff, "%.02f", entity->GetHealth().Current - pSDK->HealthPrediction->GetHealthPrediction(entity, 0));
		SDKVECTOR worldCoords = entity->GetPosition().ToSDK();
		gSdkContext->_SdkDrawText(&worldCoords, nullptr, buff, "Arial", &Color::Red, 10, 10, 10, false);
	}*/
	////// TEST HPRED INFO

	if (gameTicks - g_LastClean <= 1000)
		return;

	for (auto& attacks : Attacks::InboundAttacks)
	{
		for (auto& attack : attacks.second)
		{
			if (!attack->Sender || !attack->Target || attack->Sender->IsDead() || !attack->Sender->IsVisible() || attack->Target->IsDead() || !attack->Target->IsVisible())
			{
				attack->AttackStatus = AttackState::Completed;
			}
		}

		attacks.second.remove_if([](shared_ptr<AutoAttack> attack)
		{
			return attack->CanRemoveAttack();
		});
	}

	g_LastClean = gameTicks;
}

void HealthPrediction::OnAICastAttack(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	if (!AI)
		return;
	if (!SpellCast || !SpellCast->TargetObject)
		return;

	const auto Sender = pSDK->EntityManager->GetAIByObject(AI);
	const auto Target = pSDK->EntityManager->GetAIByObject(SpellCast->TargetObject);
	if (Sender != nullptr && Target != nullptr && !Sender->IsDead() && Sender->IsVisible() && !Target->IsDead() && Target->IsVisible() && Sender->IsMelee())
	{
		auto& ob = Attacks::GetInboundAttacks(Target->GetNetworkID());
		if (!ob.empty())
		{
			for (auto& attack : ob)
			{
				if (attack->Sender->GetNetworkID() == Sender->GetNetworkID())
					attack->AttackStatus = AttackState::Completed;
			}
		}
	}
}

void HealthPrediction::Initialize()
{
	pSDK->EventHandler->RegisterEvent(eEventType::AIAttack, OnProcessAutoAttack);
	pSDK->EventHandler->RegisterEvent(eEventType::ObjectCreate, OnObjectCreate);
	pSDK->EventHandler->RegisterEvent(eEventType::ObjectDelete, OnObjectDelete);
	pSDK->EventHandler->RegisterEvent(eEventType::Tick, OnUpdate);
	pSDK->EventHandler->RegisterEvent(eEventType::AICastAttack, OnAICastAttack);
}

int HealthPrediction::TimeForAutoToReachTarget(shared_ptr<AIClient> Target)
{
	const auto dist = pSDK->Player->GetServerPosition().GetDistance(Target->GetServerPosition());
	const auto attackTravelTime = dist / pSDK->Player->GetBasicAttack().MissileSpeed * 1000.f;
	const auto totalTime = static_cast<int>(pSDK->Player->GetAttackCastDelay() * 1000.f + attackTravelTime + pSDK->Game->GetPing() / 2.f);
	return totalTime;
}

float HealthPrediction::CalculateMissileTravelTime(shared_ptr<AIClient> Target, Vector2 SourcePosition,
	float MissileDelay, float MissileSpeed)
{
	if (MissileSpeed <= 0 || MissileSpeed >= FLT_MAX)
	{
		return .0f;
	}

	auto navData = Target->GetNavData();
	auto minionPosition = Target->GetPosition();
	if (!Target->IsMoving() || navData.NumberOfWaypoints == 2)
	{
		return SourcePosition.GetDistance(minionPosition.To2D()) / MissileSpeed;
	}

	Vector3 pathEndPosition = navData.Waypoints[navData.NumberOfWaypoints - 1];
	auto direction = (pathEndPosition - minionPosition).Normalized();
	auto totalPathLength = pathEndPosition.GetDistance(minionPosition);
	auto minionMoveSpeed = Target->GetMovementSpeed();
	auto delayDistance = MissileDelay * minionMoveSpeed;

	if (delayDistance <= totalPathLength)
	{
		auto positionAfterDelay = minionPosition + direction * delayDistance;
		auto angle = GetAngle((positionAfterDelay.To2D() - SourcePosition).Normalized(), direction.To2D());
		auto c = sin(minionMoveSpeed / MissileSpeed * sin(angle));

		if (M_PI - angle - asin(c) >= 0)
		{
			auto playerDistance = static_cast<float>(c / sin(M_PI - angle - asin(c)) * positionAfterDelay.To2D().GetDistance(SourcePosition));
			auto predictedPos = positionAfterDelay + direction * playerDistance;

			minionPosition = playerDistance + delayDistance <= totalPathLength ? predictedPos : pathEndPosition;
		}
		else
		{
			minionPosition = pathEndPosition;
		}
	}
	else
	{
		minionPosition = pathEndPosition;
	}

	return SourcePosition.GetDistance(minionPosition.To2D()) / MissileSpeed;
}

float HealthPrediction::GetHealthPredictionLaneClear(std::shared_ptr<AIClient> AI, int Time)
{
	float predictedDamage = .0f;
	const auto rTime = Time;

	for (auto& attack : Attacks::InboundAttacks[AI->GetNetworkID()])
	{
		if (attack->Target->GetNetworkID() != AI->GetNetworkID() || static_cast<int>(pSDK->Game->GetGameTicks()) - attack->DetectTime > rTime)
			continue;

		predictedDamage += static_cast<float>(attack->Damage);
	}

	const SDK_HEALTH health = AI->GetHealth();
	return health.Current + health.AllShield + health.PhysicalShield - predictedDamage;
}

float HealthPrediction::GetPrediction(std::shared_ptr<AIClient> AI, int Time)
{
	float predictedDamage = .0f;
	for (auto& attack : Attacks::InboundAttacks[AI->GetNetworkID()])
	{
		if (attack->HasReached())
		{
			continue;
		}
		if (!attack->IsValid())
		{
			continue;
		}

		const int gameTicks = pSDK->Game->GetGameTicks();
		if (attack->Target->GetNetworkID() != AI->GetNetworkID())
		{
			continue;
		}
		if (gameTicks - attack->DetectTime > 3000)
		{
			continue;
		}

		const auto mlt = gameTicks + Time;
		const auto alt = gameTicks + attack->ETA();

		if (mlt - alt > 100)
		{
			predictedDamage += static_cast<float>(attack->Damage);
		}
	}

	const SDK_HEALTH health = AI->GetHealth();
	return health.Current + health.AllShield + health.PhysicalShield - predictedDamage;
}

float HealthPrediction::GetHealthPrediction(std::shared_ptr<AIClient> AI, int Time)
{
	const auto rtime = Time == 0 ? TimeForAutoToReachTarget(AI) : Time;
	return GetPrediction(AI, rtime);
}
