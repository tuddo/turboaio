#pragma once

#include <Windows.h>
#include "../sdkapi.h"
#include "Math.hpp"
#include "Vars.h"

struct Renderer
{
	static HWND GameHWND;
	
	static Vector3 ScreenToWorld(Vector2 ScreenPosition)
	{
		SDKPOINT SDKScreenPosition{ ScreenPosition.X, ScreenPosition.Y };
		SDKVECTOR SDKWorldPosition;
		gSdkContext->_SdkScreenToWorld(&SDKScreenPosition, &SDKWorldPosition);

		return SDKWorldPosition;
	}

	static Vector2 WorldToScreen(Vector3 WorldPosition)
	{
		SDKVECTOR SDKWorldPosition{ WorldPosition.ToSDK() };
		SDKPOINT SDKScreenPosition;
		gSdkContext->_SdkWorldToScreen(&SDKWorldPosition, &SDKScreenPosition);

		return { SDKScreenPosition.x, SDKScreenPosition.y };
	}

	static Vector2 WorldToMinimap(Vector3 WorldPosition)
	{
		SDKVECTOR SDKWorldPosition{ WorldPosition.ToSDK() };
		SDKPOINT SDKScreenPosition;
		gSdkContext->_SdkWorldToMinimap(&SDKWorldPosition, &SDKScreenPosition);

		return { SDKScreenPosition.x, SDKScreenPosition.y };
	}

	static pair<int, int> GetResolution()
	{
		RECT Rect;
		HWND GameHWND;
		gSdkContext->_SdkUiGetWindow(&GameHWND);
		GetClientRect(GameHWND, &Rect);

		return make_pair<int, int>(Rect.right - Rect.left, Rect.bottom - Rect.top);
	}

	static Vector2 CursorPos()
	{
		SDKPOINT SDKCursorPos;
		gSdkContext->_SdkGetMouseScreenPosition(&SDKCursorPos);

		return { SDKCursorPos.x, SDKCursorPos.y };
	}

	static Vector3 WorldCursorPos()
	{
		const Vector2 ScreenPosition = CursorPos();
		return ScreenToWorld(ScreenPosition);
	}
};