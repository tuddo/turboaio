#pragma once

#include <Windows.h>
#include "../sdkapi.h"
#include <tuple>
#include <cfloat>
#include <vector>

#define _USE_MATH_DEFINES
#include <math.h>

#pragma region Vector2

struct Vector2
{
	float X, Y;

	Vector2 operator-(const Vector2& vec) const { return{ abs(X - vec.X), abs(Y - vec.Y) }; }
	Vector2 operator+(const Vector2& vec) const { return{ abs(X + vec.X), abs(Y + vec.Y) }; }
	Vector2 operator+(const float& vec) const { return{ abs(X + vec), abs(Y + vec) }; }
	Vector2 operator*(const Vector2& vec) const { return{ abs(X * vec.X), abs(Y * vec.Y) }; }
	Vector2 operator*(const float& vec) const { return{ abs(X * vec), abs(Y * vec) }; }
	Vector2 operator/(const Vector2& vec) const { return{ abs(X / vec.X), abs(Y / vec.Y) }; }
	Vector2 operator/(const float& vec) const { return{ abs(X / vec), abs(Y / vec) }; }
	bool operator==(const Vector2& vec) const { return X == vec.X && Y == vec.Y; }

	float Length() const { return sqrtf(X * X + Y * Y); }
	float GetDistance(Vector2 vector) const
	{
		const float xDiff = vector.X - X;
		const float yDiff = vector.Y - Y;
		return sqrtf(xDiff * xDiff + yDiff * yDiff);
	}
	float GetDistanceSquared(Vector2 vector) const
	{
		const float xDiff = vector.X - X;
		const float yDiff = vector.Y - Y;
		return xDiff * xDiff + yDiff * yDiff;
	}
	Vector2 Normalized() const
	{
		const float length = Length();
		const float inv = 1.0f / length;
		return{ X * inv, Y * inv };
	}
	Vector2 Perpendicular() const { return { -Y, X }; }
	std::tuple<bool, Vector2> Intersection(Vector2 lineSegment1End, Vector2 lineSegment2Start, Vector2 lineSegment2End) const
	{
		const double deltaACy = Y - lineSegment2Start.Y;
		const double deltaDCx = lineSegment2End.X - lineSegment2Start.X;
		const double deltaACx = X - lineSegment2Start.X;
		const double deltaDCy = lineSegment2End.Y - lineSegment2Start.Y;
		const double deltaBAx = lineSegment1End.X - X;
		const double deltaBAy = lineSegment1End.Y - Y;

		const double denominator = deltaBAx * deltaDCy - deltaBAy * deltaDCx;
		const double numerator = deltaACy * deltaDCx - deltaACx * deltaDCy;

		if (abs(denominator) < FLT_EPSILON)
		{
			if (abs(numerator) < FLT_EPSILON)
			{
				if (X >= lineSegment2Start.X && X <= lineSegment2End.X)
				{
					return std::make_tuple<bool, Vector2>(true, Vector2(X, Y));
				}
				if (lineSegment2Start.X >= X && lineSegment2Start.X <= lineSegment1End.X)
				{
					return std::make_tuple<bool, Vector2>(true, Vector2(lineSegment2Start.X, lineSegment2Start.Y));
				}
				return std::make_tuple<bool, Vector2>(false, Vector2());
			}
			return std::make_tuple<bool, Vector2>(false, Vector2());
		}

		const double r = numerator / denominator;
		if (r < 0 || r > 1)
		{
			return std::make_tuple<bool, Vector2>(false, Vector2());
		}

		const double s = (deltaACy * deltaBAx - deltaACx * deltaBAy) / denominator;
		if (s < 0 || s > 1)
		{
			return std::make_tuple<bool, Vector2>(false, Vector2());
		}

		return std::make_tuple<bool, Vector2>(true, Vector2(static_cast<float>(X + r * deltaBAx), static_cast<float>(Y + r * deltaBAy)));
	}
	float Dot(Vector2 vec) const { return X * vec.X + Y * vec.Y; }
	float Polar() const
	{
		if (abs(X - 0) <= static_cast<float>(1e-9))
		{
			return Y > 0 ? 90.f : Y < 0 ? 270.f : 0;
		}

		auto theta = static_cast<float>(atan(Y / X) * (180 / M_PI));
		if (X < 0)
		{
			theta += 180.f;
		}
		if (theta < 0)
		{
			theta += 360.f;
		}
		return theta;
	}
	float AngleBetween(Vector2 vector2) const
	{
		float theta = Polar() - vector2.Polar();
		if (theta < 0)
		{
			theta = theta + 360;
		}
		if (theta > 180)
		{
			theta = 360 - theta;
		}
		return theta;
	}
	Vector2 Perpendicular()
	{
		return { -Y, X };
	}
	Vector2 Perpendicular2() const
	{
		return { Y, -X };
	}
	Vector2 Perpendicular(int offset = 0) const
	{
		return offset == 0 ? Vector2(-Y, X) : Vector2(Y, -X);
	}
	std::vector<Vector2> CircleCircleIntersection(Vector2 center2, float radius1, float radius2)
	{
		float d = GetDistance(center2);

		if (d > radius1 + radius2 || d <= abs(radius1 - radius2))
		{
			return {};
		}

		Vector2& center1 = *this;

		const float a = (radius1 * radius1 - radius2 * radius2 + d * d) / (2 * d);
		const float h = sqrtf(radius1 * radius1 - a * a);
		Vector2 direction = (center2 - center1).Normalized();
		const Vector2 pa = center1 + direction * a;
		const Vector2 s1 = pa + direction.Perpendicular(0) * h;
		const Vector2 s2 = pa - direction.Perpendicular(0) * h;
		return { s1, s2 };
	}
	Vector2 Closest(std::vector<Vector2> array) const
	{
		Vector2 result{ .0f, .0f };
		float distance = FLT_MAX;

		for (const auto vector : array)
		{
			const float temporaryDistance = GetDistance(vector);
			if (distance < temporaryDistance)
			{
				distance = temporaryDistance;
				result = vector;
			}
		}

		return result;
	}
	float CrossProduct(Vector2 other) const
	{
		return other.Y * X - other.X * Y;
	}
	Vector2 Extend(Vector2 toVector2, float distance)
	{
		Vector2& vector2 = *this;
		return vector2 + (toVector2 - vector2).Normalized() * distance;
	}
	bool IsOrthagonal(Vector2 toVector2) const
	{
		return abs(X * toVector2.X + Y * toVector2.Y) < FLT_EPSILON;
	}
	bool IsUnderRectangle(float x, float y, float width, float height) const
	{
		return X > x && X < x + width && Y > y && Y < y + height;
	}
	bool IsValid() const
	{
		return X != .0f && Y != .0f;
	}
	Vector2 Rotated(float angle) const
	{
		const auto cosAngle = cos(angle);
		const auto sinAngle = sin(angle);

		return { X * cosAngle - Y * sinAngle, Y * cosAngle + X * sinAngle };
	}
	bool IsZero() const
	{
		return X == .0f && Y == .0f;
	}

	SDKVECTOR ToSDK() const { return { X ,Y , 0 }; }

	Vector2() : X(0), Y(0)
	{
	}
	Vector2(SDKVECTOR vector)
	{
		X = vector.x;
		Y = vector.y;
	}
	Vector2(float x, float y)
	{
		X = x;
		Y = y;
	}
};

#pragma endregion

#pragma region Vector3

struct Vector3
{
	float X, Y, Z;

	Vector3 operator-(const Vector3& vec) const { return{ X - vec.X, Y - vec.Y, Z - vec.Z }; }
	Vector3 operator+(const Vector3& vec) const { return{ X + vec.X, Y + vec.Y, Z + vec.Z }; }
	Vector3 operator+(const float &vec) const { return{ X + vec, Y + vec, Z + vec }; }
	Vector3 operator*(const Vector3& vec) const { return{ X * vec.X, Y * vec.Y, Z * vec.Z }; }
	Vector3 operator*(const float& vec) const { return{ X * vec, Y * vec, Z * vec }; }
	Vector3 operator/(const Vector3& vec) const { return{ X / vec.X, Y / vec.Y, Z / vec.Z }; }
	Vector3 operator/(const float& vec) const { return{ X / vec, Y / vec, Z / vec }; }
	bool operator==(const Vector3& vec) const { return X == vec.X && Y == vec.Y && Z == vec.Z; }

	float Length() const { return sqrtf(X * X + Y * Y + Z * Z); }
	float GetDistance(Vector3 vector) const
	{
		const float xDiff = vector.X - X;
		const float yDiff = vector.Y - Y;
		const float zDiff = vector.Z - Z;
		return sqrt(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);
	}
	float GetDistanceSquared(Vector3 vector) const
	{
		const float xDiff = vector.X - X;
		const float yDiff = vector.Y - Y;
		const float zDiff = vector.Z - Z;
		return xDiff * xDiff + yDiff * yDiff + zDiff * zDiff;
	}
	Vector3 Normalized() const
	{
		const float length = Length();
		const float inv = 1.0f / length;
		return{ X * inv, Y * inv, Z * inv };
	}
	Vector3 Extend(Vector3 toVector3, float distance)
	{
		Vector3& vector3 = *this;
		return vector3 + (toVector3 - vector3).Normalized() * distance;
	}
	Vector2 To2D() const { return { X, Z }; }
	float Dot(Vector3 vec) const { return X * vec.X + Y * vec.Y + Z * vec.Z; }
	bool IsZero() const
	{
		return X == .0f && Y == .0f && Z == .0f;
	}

	SDKVECTOR ToSDK() const { return { X, Y, Z }; }

	Vector3(): X(0), Y(0), Z(0)
	{
	}
	Vector3(Vector2 vector)
	{
		X = vector.X;
		Y = 0;
		Z = vector.Y;
	}
	Vector3(SDKVECTOR vector)
	{
		X = vector.x;
		Y = vector.y;
		Z = vector.z;
	}
	Vector3(float x, float y, float z)
	{
		X = x;
		Y = y;
		Z = z;
	}
};

#pragma endregion

#pragma region Polygon

	enum class PolygonPointState : int
	{
		Border = -1,
		Outside,
		Inside,
	};
	struct Polygon
	{
		std::vector<Vector2> Points;

		void Add(Vector2 point)
		{
			Points.push_back(point);
		}
		void Add(Vector3 point)
		{
			Points.push_back(point.To2D());
		}
		void Add(Polygon polygon)
		{
			for (auto point : polygon.Points)
			{
				Points.push_back(point);
			}
		}
		bool IsOutside(Vector2 point)
		{
			return PointInPolygon(point) == PolygonPointState::Outside;
		}
		bool IsInside(Vector2 point)
		{
			return PointInPolygon(point) == PolygonPointState::Inside;
		}
		bool IsOnBorder(Vector2 point)
		{
			return PointInPolygon(point) == PolygonPointState::Border;
		}
		PolygonPointState PointInPolygon(Vector2 point)
		{
			PolygonPointState result{};
			const size_t cnt = Points.size();
			if (cnt < 3)
				return PolygonPointState::Outside;

			Vector2 ip = Points[0];
			for (size_t i = 1; i <= cnt; ++i)
			{
				const Vector2 ipNext = i == cnt ? Points[0] : Points[i];
				if (ipNext.Y == point.Y)
				{
					if (ipNext.X == point.X || ip.Y == point.Y && ipNext.X > point.X == ip.X < point.X)
					{
						return PolygonPointState::Border;
					}
				}

				if (ip.Y < point.Y != ipNext.Y < point.Y)
				{
					if (ip.X >= point.X)
					{
						if (ipNext.X > point.X)
						{
							result = static_cast<PolygonPointState>(1 - static_cast<int>(result));
						}
						else
						{
							const float d = (ip.X - point.X) * (ipNext.Y - point.Y) - (ipNext.X - point.X) * (ip.Y - point.Y);
							if (abs(d) < FLT_EPSILON)
							{
								return PolygonPointState::Border;
							}

							if (d > 0 == ipNext.Y > ip.Y)
							{
								result = static_cast<PolygonPointState>(1 - static_cast<int>(result));
							}
						}
					}
					else
					{
						if (ipNext.X > point.X)
						{
							const float d = (ip.X - point.X) * (ipNext.Y - point.Y) - (ipNext.X - point.X) * (ip.Y - point.Y);
							if (abs(d) < FLT_EPSILON)
							{
								return PolygonPointState::Border;
							}

							if (d > 0 == ipNext.Y > ip.Y)
							{
								result = static_cast<PolygonPointState>(1 - static_cast<int>(result));
							}
						}
					}
				}

				ip = ipNext;
			}

			return result;
		}
	};

#pragma endregion

#pragma region Arc

	struct Arc : Polygon
	{
	private:
		const int _quality;
	public:
		Vector2 StartPos;
		Vector2 EndPos;
		float Angle;
		float Radius;

		Arc(Vector2 start, Vector2 direction, float angle, float radius, int quality = 20) : _quality(quality)
		{
			StartPos = start;
			EndPos = (direction - start).Normalized();
			Angle = angle;
			Radius = radius;
			UpdatePolygon();
		}

		void UpdatePolygon(int offset = 0)
		{
			Points.clear();
			const auto outRadius = static_cast<float>((Radius + offset) / cos(2 * M_PI / _quality));
			auto side1 = EndPos.Rotated(-Angle * .5f);
			for (int i = 0; i <= _quality; i++)
			{
				const auto cDirection = side1.Rotated(i * Angle / _quality).Normalized();
				Points.emplace_back(StartPos.X + outRadius * cDirection.X, StartPos.Y + outRadius * cDirection.Y);
			}
		}
	};

#pragma endregion

#pragma region Circle

	struct Circle : Polygon
	{
	private:
		const int _quality;
	public:
		Vector2 Center;
		float Radius;

		Circle(Vector2 center, float radius, int quality = 20) : _quality(quality)
		{
			Center = center;
			Radius = radius;
			UpdatePolygon();
		}

		void UpdatePolygon(int offset = 0, float overrideWidth = -1)
		{
			Points.clear();
			const auto outRadius = overrideWidth > 0 ? overrideWidth : (offset + Radius) / static_cast<float>(cos(2 * M_PI / _quality));
			for (int i = 1; i <= _quality; i++)
			{
				const auto angle = static_cast<float>(i * 2 * M_PI / _quality);
				Points.emplace_back(Center.X + outRadius * cosf(angle), Center.Y + outRadius * sinf(angle));
			}
		}
	};

#pragma endregion

#pragma region Line

	struct Line : Polygon
	{
		Vector2 LineStart;
		Vector2 LineEnd;


		Line(Vector2 start, Vector2 end, float length = -1)
		{
			LineStart = start;
			LineEnd = end;
			if (length > 0)
			{
				SetLength(length);
			}
			UpdatePolygon();
		}

		float GetLength() const
		{
			return LineStart.GetDistance(LineEnd);
		}

		void SetLength(float length)
		{
			LineEnd = (LineEnd - LineStart).Normalized() * length + LineStart;
		}

		void UpdatePolygon()
		{
			Points.clear();
			Points.push_back(LineStart);
			Points.push_back(LineEnd);
		}
	};

#pragma endregion

#pragma region Rectangle

	struct Rect : Polygon
	{
		Vector2 Start;
		Vector2 End;
		float Width;

		Rect(Vector2 start, Vector2 end, float width)
		{
			Start = start;
			End = end;
			Width = width;
			UpdatePolygon();
		}

		Vector2 GetDirection() const
		{
			return (End - Start).Normalized();
		}

		Vector2 GetPerpendicular() const
		{
			return GetDirection().Perpendicular();
		}

		void UpdatePolygon(int offset = 0, float overrideWidth = -1)
		{
			const auto floatedOffset = static_cast<float>(offset);

			Points.clear();
			Points.push_back(Start + GetPerpendicular() * (overrideWidth > 0 ? overrideWidth : Width + offset) - GetDirection() * floatedOffset);
			Points.push_back(Start - GetPerpendicular() * (overrideWidth > 0 ? overrideWidth : Width + offset) - GetDirection() * floatedOffset);
			Points.push_back(End - GetPerpendicular() * (overrideWidth > 0 ? overrideWidth : Width + offset) + GetDirection() * floatedOffset);
			Points.push_back(End + GetPerpendicular() * (overrideWidth > 0 ? overrideWidth : Width + offset) + GetDirection() * floatedOffset);
		}
	};

#pragma endregion

#pragma region Geometry

struct Geometry
{
	static bool Close(float a, float b, float eps);
	static float RadianToDegree(double angle);
	static float Polar(Vector2 v1);
	static float AngleBetween(Vector2 p1, Vector2 p2);
	static std::tuple<float, Vector2> VectorMovementCollision(Vector2 startPoint1, Vector2 endPoint1, float v1, Vector2 startPoint2, float v2, float delay = .0f);
};

inline bool Geometry::Close(float a, float b, float eps)
{
	if (abs(eps) < FLT_EPSILON)
	{
		eps = static_cast<float>(1e-9);
	}
	return abs(a - b) <= eps;
}

inline float Geometry::RadianToDegree(double angle)
{
	return static_cast<float>(angle * (180.f / M_PI));
}

inline float Geometry::Polar(Vector2 v1)
{
	if (Close(v1.X, 0, 0))
	{
		if (v1.Y > 0)
		{
			return 90.f;
		}
		return v1.Y < 0 ? 270.f : 0;
	}

	float theta = RadianToDegree(atan(static_cast<double>(v1.Y / v1.X)));
	if (v1.X < 0)
	{
		theta = theta + 180;
	}
	if (theta < 0)
	{
		theta = theta + 360;
	}
	return theta;
}

inline float Geometry::AngleBetween(Vector2 p1, Vector2 p2)
{
	float theta = p1.Polar() - p2.Polar();
	if (theta < 0)
	{
		theta = theta + 360;
	}
	if (theta > 180)
	{
		theta = 360 - theta;
	}
	return theta;
}

inline std::tuple<float, Vector2> Geometry::VectorMovementCollision(Vector2 startPoint1, Vector2 endPoint1, float v1, Vector2 startPoint2, float v2, float delay)
{
	float sP1x = startPoint1.X,
		sP1y = startPoint1.Y,
		eP1x = endPoint1.X,
		eP1y = endPoint1.Y,
		sP2x = startPoint2.X,
		sP2y = startPoint2.Y;

	float d = eP1x - sP1x;
	float e = eP1y - sP1y;
	float dist = sqrt(d * d + e * e);
	float t1 = NAN;
	float S = abs(dist) > FLT_EPSILON ? v1 * d / dist : .0f;
	float K = abs(dist) > FLT_EPSILON ? v1 * e / dist : .0f;

	float r = sP2x - sP1x;
	float j = sP2y - sP1y;
	float c = r * r + j * j;

	if (dist > .0f)
	{
		if (abs(v1 - FLT_MAX) < FLT_EPSILON)
		{
			float t = dist / v1;
			t1 = v2 * t >= .0f ? t : NAN;
		}
		else if (abs(v2 - FLT_MAX) < FLT_EPSILON)
		{
			t1 = .0f;
		}
		else
		{
			float a = S * S + K * K - v2 * v2;
			float b = -r * S - j * K;

			if (abs(a) < FLT_EPSILON)
			{
				if (abs(b) < FLT_EPSILON)
				{
					t1 = abs(c) < FLT_EPSILON ? .0f : NAN;
				}
				else
				{
					float t = -c / (2 * b);
					t1 = (v2 * t >= .0f) ? t : NAN;
				}
			}
			else
			{
				float sqr = b * b - a * c;
				if (sqr >= .0f)
				{
					float nom = sqrt(sqr);
					float t = (-nom - b) / a;
					t1 = v2 * t >= .0f ? t : NAN;
					t = (nom - b) / a;
					float t2 = v2 * t >= .0f ? t : NAN;

					if (t2 != NAN && t1 != NAN)
					{
						if (t1 >= delay && t2 >= delay)
						{
							t1 = min(t1, t2);
						}
						else
						{
							t1 = t2;
						}
					}
				}
			}
		}
	}
	else if (abs(dist) < FLT_EPSILON)
	{
		t1 = .0f;
	}

	return std::make_tuple(t1, t1 != NAN ? Vector2(sP1x + S * t1, sP1y + K * t1) : Vector2());
}

#pragma endregion

#pragma region Physics

struct Physics
{
	static Vector2 PositionAfter(std::vector<Vector2> self, int t, int s, int delay = 0);
};

inline Vector2 Physics::PositionAfter(std::vector<Vector2> self, int t, int s, int delay)
{
	float distance = static_cast<float>(max(0, t - delay) * s / 1000);
	for (size_t i = 0; i <= self.size() - 2; i++)
	{
		const Vector2 from = self[i];
		Vector2 to = self[i + 1];
		float d = to.GetDistance(from);
		if (d > distance)
		{
			return from + (to - from).Normalized() * distance;
		}
		distance -= d;
	}
	return self[self.size() - 1];
}

#pragma endregion
