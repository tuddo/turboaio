#include "Menu.hpp"
#include <Windows.h>
#include "ImGui/imgui.h"
#include "ImGui/imgui_impl_dx9.h"
#include "EventHandler.hpp"
#include "Vars.h"
#include "TargetSelector.hpp"

// ImGui Properties
bool Menu::DrawWindow = true;

int Menu::BaseWindupTime = 33;

//// SDK
std::vector<std::function<void()>> Menu::CustomMenus{};

const char* TargetSelectorOptions[] = { "AutoPriority", "MostAD", "MostAP", "Closest", "NearMouse", "LessAttack" };
bool firstExec = false;
void __cdecl OnOverlayScene(void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	if (!Menu::DrawWindow)
		Menu::DrawWindow = true;

	ImGui_ImplDX9_NewFrame();

	#pragma region SDK Menu

	for (const auto& menu : Menu::CustomMenus)
		menu();

	#pragma endregion

	ImGui::EndFrame();
	ImGui::Render();
}

void Menu::UseStyle()
{
	ImGuiStyle& style = ImGui::GetStyle();
	style.WindowBorderSize = 1.f;
	style.ChildBorderSize = 1.f;
	style.FrameBorderSize = 1.f;
	style.PopupBorderSize = 1.f;
	style.ScrollbarSize = 10.f;
	style.WindowRounding = .0f;
	style.ScrollbarRounding = .0f;
	style.WindowTitleAlign.x = .5f;

	ImVec4* colors = style.Colors;
	colors[ImGuiCol_Text] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
	colors[ImGuiCol_TextDisabled] = ImVec4(0.50f, 0.50f, 0.50f, 1.00f);
	colors[ImGuiCol_WindowBg] = ImVec4(0.06f, 0.06f, 0.06f, 0.94f);
	colors[ImGuiCol_ChildBg] = ImVec4(1.00f, 1.00f, 1.00f, 0.00f);
	colors[ImGuiCol_PopupBg] = ImVec4(0.08f, 0.08f, 0.08f, 0.94f);
	colors[ImGuiCol_Border] = ImVec4(0.43f, 0.43f, 0.50f, 0.50f);
	colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
	colors[ImGuiCol_FrameBg] = ImVec4(0.16f, 0.29f, 0.48f, 0.54f);
	colors[ImGuiCol_FrameBgHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.40f);
	colors[ImGuiCol_FrameBgActive] = ImVec4(0.26f, 0.59f, 0.98f, 0.67f);
	colors[ImGuiCol_TitleBg] = ImVec4(1.00f, 0.00f, 0.00f, 1.00f);
	colors[ImGuiCol_TitleBgActive] = ImVec4(1.00f, 0.00f, 0.00f, 1.00f);
	colors[ImGuiCol_TitleBgCollapsed] = ImVec4(1.00f, 0.00f, 0.00f, 1.00f);
	colors[ImGuiCol_MenuBarBg] = ImVec4(0.14f, 0.14f, 0.14f, 1.00f);
	colors[ImGuiCol_ScrollbarBg] = ImVec4(0.02f, 0.02f, 0.02f, 0.53f);
	colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.31f, 0.31f, 0.31f, 1.00f);
	colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.41f, 0.41f, 0.41f, 1.00f);
	colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.51f, 0.51f, 0.51f, 1.00f);
	colors[ImGuiCol_CheckMark] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	colors[ImGuiCol_SliderGrab] = ImVec4(0.24f, 0.52f, 0.88f, 1.00f);
	colors[ImGuiCol_SliderGrabActive] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	colors[ImGuiCol_Button] = ImVec4(0.26f, 0.59f, 0.98f, 0.40f);
	colors[ImGuiCol_ButtonHovered] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	colors[ImGuiCol_ButtonActive] = ImVec4(0.06f, 0.53f, 0.98f, 1.00f);
	colors[ImGuiCol_Header] = ImVec4(0.26f, 0.59f, 0.98f, 0.31f);
	colors[ImGuiCol_HeaderHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.80f);
	colors[ImGuiCol_HeaderActive] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	colors[ImGuiCol_Separator] = ImVec4(0.43f, 0.43f, 0.50f, 0.50f);
	colors[ImGuiCol_SeparatorHovered] = ImVec4(0.10f, 0.40f, 0.75f, 0.78f);
	colors[ImGuiCol_SeparatorActive] = ImVec4(0.10f, 0.40f, 0.75f, 1.00f);
	colors[ImGuiCol_ResizeGrip] = ImVec4(0.26f, 0.59f, 0.98f, 0.25f);
	colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.67f);
	colors[ImGuiCol_ResizeGripActive] = ImVec4(0.26f, 0.59f, 0.98f, 0.95f);
	colors[ImGuiCol_CloseButton] = ImVec4(0.41f, 0.41f, 0.41f, 0.50f);
	colors[ImGuiCol_CloseButtonHovered] = ImVec4(0.98f, 0.39f, 0.36f, 1.00f);
	colors[ImGuiCol_CloseButtonActive] = ImVec4(0.98f, 0.39f, 0.36f, 1.00f);
	colors[ImGuiCol_PlotLines] = ImVec4(0.61f, 0.61f, 0.61f, 1.00f);
	colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
	colors[ImGuiCol_PlotHistogram] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
	colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
	colors[ImGuiCol_TextSelectedBg] = ImVec4(0.26f, 0.59f, 0.98f, 0.35f);
	colors[ImGuiCol_ModalWindowDarkening] = ImVec4(0.80f, 0.80f, 0.80f, 0.35f);
	colors[ImGuiCol_DragDropTarget] = ImVec4(1.00f, 1.00f, 0.00f, 0.90f);
}

std::string SelectedKey{};
map<std::string, MENUKEY_INFO> RunningMenuKeys{};
void __cdecl SDKMenu_OnKeyPress(bool KeyDown, unsigned int VirtualKey, unsigned short Repeated, unsigned char ScanCode, bool IsExtended, bool IsAltDown, bool PreviousState, bool TransitionState, void * UserData)
{
	UNREFERENCED_PARAMETER(UserData);
	UNREFERENCED_PARAMETER(IsAltDown);
	UNREFERENCED_PARAMETER(IsExtended);
	UNREFERENCED_PARAMETER(ScanCode);
	UNREFERENCED_PARAMETER(Repeated);
	UNREFERENCED_PARAMETER(KeyDown);

	if (VirtualKey == VK_SPACE || VirtualKey == VK_ESCAPE || VirtualKey >= 65 && VirtualKey <= 90)
	{
		if (!SelectedKey.empty())
		{
			MENUKEY_INFO& hotkey = RunningMenuKeys[SelectedKey];

			if (VirtualKey == VK_ESCAPE)
			{
				hotkey.CurrentKey = hotkey.DefaultKey;
				gSdkContext->_SdkSetSettingNumber(SelectedKey.c_str(), hotkey.DefaultKey);
				SelectedKey.clear();
				return;
			}

			if (PreviousState == 0)
			{
				hotkey.CurrentKey = static_cast<eKeyCode>(VirtualKey);
				gSdkContext->_SdkSetSettingNumber(SelectedKey.c_str(), VirtualKey);
				SelectedKey.clear();
			}
			return;
		}

		if (VirtualKey != VK_ESCAPE)
		{
			for (auto& menuKey : RunningMenuKeys)
			{
				if (VirtualKey == static_cast<unsigned int>(menuKey.second.CurrentKey))
				{
					if (menuKey.second.Type == MENUKEY_TYPE_HOTKEY)
					{
						if (PreviousState == 0 && TransitionState == 0)
							menuKey.second.Status = true;
						else if (PreviousState == 1 && TransitionState == 1)
							menuKey.second.Status = false;
					}
					else if (menuKey.second.Type == MENUKEY_TYPE_TOGGLE)
					{
						if (PreviousState == 0 && TransitionState == 0)
							menuKey.second.Status = !menuKey.second.Status;
					}
				}
			}
		}
	}
}

void Menu::Initialize()
{
	//Config::LoadSettings();
	UseStyle();

	pSDK->Menu->CreateMenu("Blockchain : Base", []()
	{
		pSDK->Menu->Tree("Orbwalker", "BBase.Orbwalker.Tree", []()
		{
			pSDK->Menu->Text("Base Wind: %d", Menu::BaseWindupTime);
			pSDK->Menu->Text("Total Wind: %d", Menu::BaseWindupTime + pSDK->Menu->GetInt("BBase.Wind.Up") - pSDK->Menu->GetInt("BBase.Wind.Down"));
			pSDK->Menu->SliderInt("Extra Wind-Up", "BBase.Wind.Up", 0, 0, 200);
			pSDK->Menu->SliderInt("Extra Wind-Down", "BBase.Wind.Down", 0, 0, 200);
			pSDK->Menu->Checkbox("Disable Auto-Attacks (AA)", "BBase.Orbwalker.DisableAA", false);

			pSDK->Menu->Tree("Drawings", "BBase.Orbwalker.Drawings.Tree", []()
			{
				pSDK->Menu->Checkbox("Draw Forced Target", "BBase.Orbwalker.Draw.Forced", true);
				pSDK->Menu->Checkbox("Draw Killable Minion", "BBase.Orbwalker.Draw.Killable", false);
				pSDK->Menu->Checkbox("Draw Enemies Attack Range", "BBase.Orbwalker.Draw.GlobalRange", false);
				pSDK->Menu->Checkbox("Draw Local Attack Range & Hold Radius", "BBase.Orbwalker.Draw.LocalRange", true);
			});

			pSDK->Menu->Tree("Keys", "BBase.Orbwalker.Keys.Tree", []()
			{
				pSDK->Menu->Hotkey("Lane Clear", "BBase.Orbwalker.LaneClear", eKeyCode::KeyV);
				pSDK->Menu->Hotkey("Last Hit", "BBase.Orbwalker.LastHit", eKeyCode::KeyX);
				pSDK->Menu->Hotkey("Combo", "BBase.Orbwalker.Combo", eKeyCode::KeySpace);
				pSDK->Menu->Hotkey("Harass", "BBase.Orbwalker.Harass", eKeyCode::KeyC);
				pSDK->Menu->Hotkey("Flee", "BBase.Orbwalker.Flee", eKeyCode::KeyZ);
			});

			pSDK->Menu->Tree("Lane Clear", "BBase.Orbwalker.LaneClear.Tree", []()
			{
				pSDK->Menu->Checkbox("Attack Heroes", "BBase.Orbwalker.AttackHeroes", false);
			});

			pSDK->Menu->Tree("Jungle Clear", "BBase.Orbwalker.JungleClear.Tree", []()
			{
				pSDK->Menu->Checkbox("Prioritize Small Minions", "BBase.Orbwalker.PrioritizeSmall", true);
			});
		});

		pSDK->Menu->Tree("Target Selector", "BBase.TargetSelector.Tree", []()
		{
			pSDK->Menu->List("Mode", "Base.TargetSelector.Mode", TargetSelectorOptions, 6, 0);

			int heroCounter = 0;
			for (auto& hero : pSDK->EntityManager->GetHeroes(100000.f, false, true))
			{
				ImGui::Text("%s", hero->GetAIName().c_str());
				ImGui::SameLine(200.f);

				std::string labelStr = "##" + std::to_string(1337 + heroCounter);
				ImGui::SliderInt(labelStr.c_str(), &TargetSelector::ManualPriorities[hero], 1, 5);
				heroCounter++;
			}
		});
	});

	pSDK->EventHandler->RegisterEvent(eEventType::KeyPress, SDKMenu_OnKeyPress);
	pSDK->EventHandler->RegisterEvent(eEventType::OverlayScene, OnOverlayScene);
}

// Turbo
std::map<std::string, bool> KeyEverExecuted{};
void Menu::CreateMenu(std::string menuLabel, std::function<void()> menuFunction)
{
	const std::function<void()> execFunc = [menuLabel, menuFunction]()
	{
		if (!KeyEverExecuted[menuLabel])
		{
			ImGui::SetNextWindowPosCenter();
			KeyEverExecuted[menuLabel] = true;
		}

		ImGui::Begin(menuLabel.c_str(), nullptr, ImGuiWindowFlags_AlwaysAutoResize);
		menuFunction();
		ImGui::End();
	};
	Menu::CustomMenus.push_back(execFunc);

	ImGui_ImplDX9_NewFrame();

	execFunc();

	ImGui::EndFrame();
	ImGui::Render();
}

void Menu::Tree(std::string treeLabel, std::string cfgExpanded, std::function<void()> treeFunction)
{
	if (!KeyEverExecuted[cfgExpanded])
	{
		treeFunction();
		KeyEverExecuted[cfgExpanded] = true;
	}

	bool treeSetting;
	gSdkContext->_SdkGetSettingBool(cfgExpanded.c_str(), &treeSetting, false);
	if (treeSetting)
		ImGui::SetNextTreeNodeOpen(true);

	const bool treeStatus = ImGui::TreeNode(treeLabel.c_str());
	if (treeStatus)
	{
		treeFunction();
		ImGui::TreePop();
		if (!treeSetting)
			gSdkContext->_SdkSetSettingBool(cfgExpanded.c_str(), true);
	}
	else
	{
		if (treeSetting)
			gSdkContext->_SdkSetSettingBool(cfgExpanded.c_str(), false);
	}
}

void Menu::Text(std::string label, ...)
{
	va_list list;
	va_start(list, label);
	ImGui::TextV(label.c_str(), list);
	va_end(list);
}

std::map<std::string, bool> CheckboxStatus;
void Menu::Checkbox(std::string label, std::string cfgChecked, bool defaultValue)
{
	if (!KeyEverExecuted[cfgChecked])
	{
		bool checkboxSetting;
		gSdkContext->_SdkGetSettingBool(cfgChecked.c_str(), &checkboxSetting, defaultValue);
		CheckboxStatus[cfgChecked] = checkboxSetting;
		KeyEverExecuted[cfgChecked] = true;
	}

	if (ImGui::Checkbox(label.c_str(), &CheckboxStatus[cfgChecked]))
		gSdkContext->_SdkSetSettingBool(cfgChecked.c_str(), CheckboxStatus[cfgChecked]);
}

ImVec4 ImGui_Red{ 1.f, 0.f, 0.f, 1.f };
ImVec4 ImGui_Green{ 0.f, 1.f, 0.f, 1.f };

void Menu::Hotkey(std::string label, std::string cfgKey, eKeyCode defaultKey)
{
	if (!KeyEverExecuted[cfgKey])
	{
		int keyInt;
		gSdkContext->_SdkGetSettingNumber(cfgKey.c_str(), &keyInt, defaultKey);

		MENUKEY_INFO hotkeyInfo{};
		hotkeyInfo.Type = MENUKEY_TYPE_HOTKEY;
		hotkeyInfo.ConfigKey = cfgKey;
		hotkeyInfo.DefaultKey = defaultKey;
		hotkeyInfo.CurrentKey = static_cast<eKeyCode>(keyInt);
		hotkeyInfo.Status = false;
		RunningMenuKeys[cfgKey] = hotkeyInfo;
		KeyEverExecuted[cfgKey] = true;
	}

	MENUKEY_INFO& menukeyInfo = RunningMenuKeys[cfgKey];
	const int currentKey = menukeyInfo.CurrentKey;

	bool buttonClicked = false;

	Text(label);
	ImGui::SameLine(200.f);
	if (currentKey != -1)
	{
		const char key = static_cast<char>(currentKey);

		char keyBuff[16];
		if (currentKey != eKeyCode::KeySpace)
			sprintf(keyBuff, "%c", key);
		else
			sprintf(keyBuff, "Space");

		if (SelectedKey == cfgKey)
			buttonClicked = ImGui::Button("Press any key...");
		else
			buttonClicked = ImGui::Button(keyBuff);
	}
	else
		buttonClicked = ImGui::Button("UNDEFINED");

	ImGui::SameLine();
	menukeyInfo.Status ? ImGui::TextColored(ImGui_Green, "ON") : ImGui::TextColored(ImGui_Red, "OFF");

	if (buttonClicked)
	{
		if (SelectedKey.empty())
			SelectedKey = cfgKey;
	}
}

void Menu::Toggle(std::string label, std::string cfgKey, eKeyCode defaultKey, bool defaultStatus)
{
	if (!KeyEverExecuted[cfgKey])
	{
		int keyInt;
		gSdkContext->_SdkGetSettingNumber(cfgKey.c_str(), &keyInt, defaultKey);

		MENUKEY_INFO hotkeyInfo{};
		hotkeyInfo.Type = MENUKEY_TYPE_TOGGLE;
		hotkeyInfo.ConfigKey = cfgKey;
		hotkeyInfo.DefaultKey = defaultKey;
		hotkeyInfo.CurrentKey = static_cast<eKeyCode>(keyInt);
		hotkeyInfo.Status = defaultStatus;
		RunningMenuKeys[cfgKey] = hotkeyInfo;
		KeyEverExecuted[cfgKey] = true;
	}

	MENUKEY_INFO& menukeyInfo = RunningMenuKeys[cfgKey];
	const int currentKey = menukeyInfo.CurrentKey;

	bool buttonClicked = false;

	Text(label);
	ImGui::SameLine(200.f);
	if (currentKey != -1)
	{
		const char key = static_cast<char>(currentKey);

		char keyBuff[16];
		if (currentKey != eKeyCode::KeySpace)
			sprintf(keyBuff, "%c", key);
		else
			sprintf(keyBuff, "Space");

		if (SelectedKey == cfgKey)
			buttonClicked = ImGui::Button("Press any key...");
		else
			buttonClicked = ImGui::Button(keyBuff);
	}
	else
		buttonClicked = ImGui::Button("UNDEFINED");

	ImGui::SameLine();
	menukeyInfo.Status ? ImGui::TextColored(ImGui_Green, "ON") : ImGui::TextColored(ImGui_Red, "OFF");

	if (buttonClicked)
	{
		if (SelectedKey.empty())
			SelectedKey = cfgKey;
	}
}

bool Menu::GetMenuKeyStatus(std::string cfgKey) { return RunningMenuKeys[cfgKey].Status; }

int TotalCommonComponents = 0;
map<std::string, int> ComponentID{};
map<std::string, int> CacheInt{};
void Menu::SliderInt(std::string label, std::string cfgKey, int defaultValue, int min, int max)
{
	if (!KeyEverExecuted[cfgKey])
	{
		ComponentID[cfgKey] = ++TotalCommonComponents;
		KeyEverExecuted[cfgKey] = true;
		gSdkContext->_SdkGetSettingNumber(cfgKey.c_str(), &CacheInt[cfgKey], defaultValue);
	}

	Text(label);
	ImGui::SameLine(200.f);

	char buffer[8];
	sprintf(buffer, "##%d", ComponentID[cfgKey]);
	if (ImGui::SliderInt(buffer, &CacheInt[cfgKey], min, max))
		gSdkContext->_SdkSetSettingNumber(cfgKey.c_str(), CacheInt[cfgKey]);
}

map<std::string, float> CacheFloat{};
void Menu::SliderFloat(std::string label, std::string cfgKey, float defaultValue, float min, float max, float power)
{
	if (!KeyEverExecuted[cfgKey])
	{
		ComponentID[cfgKey] = ++TotalCommonComponents;
		KeyEverExecuted[cfgKey] = true;
		gSdkContext->_SdkGetSettingFloat(cfgKey.c_str(), &CacheFloat[cfgKey], defaultValue);
	}

	Text(label);
	ImGui::SameLine(200.f);

	char buffer[8];
	sprintf(buffer, "##%d", ComponentID[cfgKey]);
	if (ImGui::SliderFloat(buffer, &CacheFloat[cfgKey], min, max, "%.02f", power))
		gSdkContext->_SdkSetSettingFloat(cfgKey.c_str(), CacheFloat[cfgKey]);
}

int Menu::GetInt(std::string cfgKey) { return CacheInt[cfgKey]; }

float Menu::GetFloat(std::string cfgKey) { return CacheFloat[cfgKey]; }

bool Menu::GetBool(std::string cfgKey) { return CheckboxStatus[cfgKey]; }

std::map<std::string, int> ListIndex{};
void Menu::List(std::string label, std::string cfgKey, const char* items[], int itemsCount, int defaultIndex)
{
	if (!KeyEverExecuted[cfgKey])
	{
		ComponentID[cfgKey] = ++TotalCommonComponents;

		int listValue;
		gSdkContext->_SdkGetSettingNumber(cfgKey.c_str(), &listValue, defaultIndex);
		ListIndex[cfgKey] = listValue;
		KeyEverExecuted[cfgKey] = true;
	}

	Text(label);
	ImGui::SameLine(200.f);

	char buffer[8];
	sprintf(buffer, "##%d", ComponentID[cfgKey]);
	if (ImGui::Combo(buffer, &ListIndex[cfgKey], items, itemsCount))
		gSdkContext->_SdkSetSettingNumber(cfgKey.c_str(), ListIndex[cfgKey]);
}

void Menu::SameLine() { ImGui::SameLine(); }