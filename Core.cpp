// I'm too lazy too categorize them
#include <Windows.h>
#include "../sdkapi.h"

// Gud libraries
#include "Initializer.hpp"
#include <winuser.h>

PSDK_CONTEXT SDK_CONTEXT_GLOBAL;

HINSTANCE gInstDLL;

BOOL WINAPI DllMain(_In_ HINSTANCE hinstDLL, _In_ const DWORD fdwReason, _In_ const LPVOID lpvReserved)
{
	UNREFERENCED_PARAMETER(hinstDLL);
	gInstDLL = hinstDLL;

	if (fdwReason == DLL_PROCESS_ATTACH)
	{
		SDK_EXTRACT_CONTEXT(lpvReserved);
		if (!SDK_CONTEXT_GLOBAL)
			return FALSE;

		if (!SDKSTATUS_SUCCESS(SdkNotifyLoadedModule("BlockchainBase", SDK_VERSION)))
			return FALSE;

		_g_SdkContext->_SdkRegisterOnLoad([](void *UserData) -> void
		{
			UNREFERENCED_PARAMETER(UserData);

			Initializer::Initialize(_g_SdkContext, gInstDLL);
		}, nullptr);
	}
	else if (fdwReason == DLL_PROCESS_DETACH)
		SetWindowLongPtr(Initializer::GameHWND, GWL_WNDPROC, static_cast<LONG>(reinterpret_cast<LONG_PTR>(Initializer::OldWND)));
	
	return TRUE;
}