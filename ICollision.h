#pragma once

#include <Windows.h>
#include "../sdkapi.h"
#include "Math.hpp"

struct ICollision
{
	virtual bool GetWallCollision(Vector2& startPos, Vector2& endPos, float width) = 0;
};