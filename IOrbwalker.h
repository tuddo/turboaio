#pragma once

#include <Windows.h>
#include "../sdkapi.h"
#include <string>
#include "Math.hpp"

enum class OrbwalkerMode : int {
	None,
	LaneClear,
	LastHit,
	Combo,
	Harras,
	Flee
};

struct IOrbwalker
{
	virtual OrbwalkerMode GetOrbwalkerMode() = 0;
	virtual bool CanMove() = 0;
	virtual bool CanAttack() = 0;
	virtual bool IsAutoAttackReset(std::string spellName) = 0;
	virtual void MoveTo(Vector3 position, bool pet = false) = 0;
	virtual void MoveToMouse(bool pet = false) = 0;
	virtual bool GetOrbwalkerAttackStatus() = 0;
	virtual void ToggleOrbwalkerAttack() = 0;
	virtual bool GetOrbwalkerStatus() = 0;
	virtual void ToggleOrbwalker() = 0;
};