#include "Orbwalker.hpp"
#include <vector>
#include "Vars.h"
#include "Menu.hpp"
#include "Utils.hpp"
#include "TargetSelector.hpp"
#include "EventHandler.hpp"
#include "Initializer.hpp"
#include "Pathing.hpp"

int Orbwalker::SpecialType = 0;

int Orbwalker::MissileLaunched = 0;
int Orbwalker::LastAutoAttackTick = 0;
int Orbwalker::LastAutoAttackCommandTick = 0;
int Orbwalker::LastMovementOrderTick = 0;
std::shared_ptr<AIClient> Orbwalker::LastTarget = nullptr;
int Orbwalker::BlockOrdersUntilTick = 0;

bool Orbwalker::OrbwalkerStatus = true;
bool Orbwalker::FullOrbwalkerStatus = true;
std::vector<std::string> Orbwalker::AttackResets = {
	"PowerFist", "CamilleQ", "CamilleQ2", "VorpalSpikes", "DariusNoxianTacticsONH",
	"Masochism", "EliseSpiderW", "FioraE", "GarenQ", "NetherBlade",
	"IllaoiW", "JaxEmpowerTwo", "JayceHyperCharge", "LeonaShieldOfDaybreak", "LucianE",
	"Meditate", "MordekaiserMaceOfSpades", "NasusQ", "NautilusPiercingGaze", "Takedown",
	"RengarQ", "RenektonPreExecute", "RivenTriCleave", "ShyvanaDoubleAttack", "SivirW",
	"TalonQ", "TrundleTrollSmash", "VayneTumble", "ViE", "VolibearQ",
	"MonkeyKingDoubleAttack", "XinZhaoQ", "YorickQ"
};
std::vector<std::string> Orbwalker::NoCancelChamps = { "Kalista" };

#pragma region Methods

void Orbwalker::Attack(std::shared_ptr<AIClient>& target)
{
	const unsigned int _gameTicks = pSDK->Game->GetGameTicks();

	if (BlockOrdersUntilTick - static_cast<int>(_gameTicks) > 0)
	{
		return;
	}

	for (auto& handler : EventHandler::PreAttackHandlers)
	{
		handler(target);
	}

	// Issue the actual attack order
	if (std::find(NoCancelChamps.begin(), NoCancelChamps.end(), pSDK->Player->GetAIName()) == NoCancelChamps.end())
	{
		MissileLaunched = INT_MAX;
	}

	pSDK->Player->Attack(target);
	LastAutoAttackCommandTick = _gameTicks;
	LastTarget = target;

	BlockOrdersUntilTick = _gameTicks + 70 + min(60, pSDK->Game->GetPing());
}

void Orbwalker::Move(Vector3 position)
{
	const unsigned int _gameTicks = pSDK->Game->GetGameTicks();

	if (BlockOrdersUntilTick - static_cast<int>(_gameTicks) > 0)
	{
		return;
	}

	const float _boundingRadius = pSDK->Player->GetBoundingRadius();
	const float _boundingRadiusSqr = _boundingRadius * _boundingRadius;
	if (pSDK->Player->GetPosition().GetDistanceSquared(position) < _boundingRadiusSqr)
	{
		if (Pathing::GetPath(pSDK->Player.get()).size() > 1)
		{
			gSdkContext->_SdkStopLocalPlayer(false);
			LastMovementOrderTick = _gameTicks - 70;
		}

		return;
	}

	auto _serverPosition = pSDK->Player->GetServerPosition();
	/*if (_serverPosition.GetDistanceSquared(position) < _boundingRadiusSqr)
	{
		position = _serverPosition.Extend(position, _boundingRadius + (rand() % (51 - 0 + 1) + 51));
	}*/

	const float _maximumDistance = 1500.f;
	if (_serverPosition.GetDistanceSquared(position) > _maximumDistance * _maximumDistance)
	{
		position = _serverPosition.Extend(position, _maximumDistance);
	}

	pSDK->Orbwalker->MoveTo(position);
	LastMovementOrderTick = _gameTicks;
}

int Orbwalker::GetTotalWind()
{
	return Menu::BaseWindupTime + pSDK->Menu->GetInt("BBase.Wind.Up") - pSDK->Menu->GetInt("BBase.Wind.Down");
}

void Orbwalker::Orbwalk()
{
	if (!FullOrbwalkerStatus)
		return;

	if (pSDK->Orbwalker->CanAttack() && OrbwalkerStatus && !pSDK->Menu->GetBool("BBase.Orbwalker.DisableAA"))
	{
		auto _bestTarget = pSDK->TargetSelector->GetBestTarget(pSDK->Player->GetRealAttackRange(), true, true);
		if (_bestTarget != nullptr)
		{
			if (_bestTarget->IsValid(pSDK->Player->GetRealAttackRange()))
			{
				Attack(_bestTarget);
			}
		}
	}

	if (pSDK->Orbwalker->CanMove())
	{
		const auto _worldCursor = pSDK->Game->GetWorldCursorPosition();
		if (!_worldCursor.IsZero())
			Move(_worldCursor);
	}
}

#pragma endregion

#pragma region Events

void __cdecl Orbwalker::OnUpdate()
{
	//UNREFERENCED_PARAMETER(UserData);

	if (pSDK->Orbwalker->GetOrbwalkerMode() == OrbwalkerMode::None)
		return;

	bool chatActive;
	gSdkContext->_SdkIsChatActive(&chatActive);

	if (GetForegroundWindow() == Initializer::GameHWND && !chatActive && !pSDK->Player->IsDead())
		Orbwalker::Orbwalk();
}

void __cdecl Orbwalker::OnAIAttack(void* AI, void* TargetObject, bool StartAttack, bool StopAttack, void* UserData)
{
	UNREFERENCED_PARAMETER(TargetObject);
	UNREFERENCED_PARAMETER(StartAttack);
	UNREFERENCED_PARAMETER(StopAttack);
	UNREFERENCED_PARAMETER(UserData);

	if (AI != pSDK->Player->Object)
		return;
	if (!TargetObject)
		return;

	if (StartAttack)
	{
		if (AI == pSDK->Player->Object)
		{
			Orbwalker::LastAutoAttackTick = pSDK->Game->GetGameTicks() - pSDK->Game->GetPing() / 2;
			Orbwalker::MissileLaunched = INT_MAX;
			Orbwalker::LastMovementOrderTick = 0;

			auto target = pSDK->EntityManager->GetAIByObject(TargetObject);

			if (target != nullptr)
			{
				if (target->Object != nullptr)
				{
					if (!target->IsValid(pSDK->Player->GetRealAttackRange()))
					{
						return;
					}

					if (Orbwalker::LastTarget != nullptr)
					{
						if (target->Object != Orbwalker::LastTarget->Object)
						{
							Orbwalker::LastTarget = target;
						}
					}
				}
			}
		}
	}
	else if (StopAttack)
	{
		Orbwalker::LastAutoAttackTick = 0;
	}
}

void __cdecl Orbwalker::OnAIProcessSpell(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData)
{
	UNREFERENCED_PARAMETER(AI);
	UNREFERENCED_PARAMETER(SpellCast);
	UNREFERENCED_PARAMETER(UserData);

	if (AI != pSDK->Player->Object)
		return;
	if (SpellCast->IsAutoAttack)
		return;

	if (pSDK->Orbwalker->IsAutoAttackReset({ SpellCast->Spell.ScriptName }))
		Orbwalker::LastAutoAttackTick = 0;
}

void __cdecl Orbwalker::OnAICastAttack(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	if (AI != pSDK->Player->Object)
		return;
	if (pSDK->Player->IsMelee())
		return;

	if (SpellCast->IsAutoAttack)
	{
		for (auto& handler : EventHandler::PostAttackHandlers)
			handler(pSDK->EntityManager->GetAIByObject(SpellCast->TargetObject));

		const unsigned int netPing = pSDK->Game->GetPing();
		Orbwalker::MissileLaunched = pSDK->Game->GetGameTicks();
		if (netPing <= 30)
			Orbwalker::MissileLaunched += netPing;
	}
	else
	{
		for (auto& handler : EventHandler::PreCastHandlers)
			handler(pSDK->EntityManager->GetAIByObject(SpellCast->TargetObject), SpellCast->EndPosition, SpellCast->Spell);
	}
}

void __cdecl Orbwalker::OnObjectDelete(void* Object, unsigned int NetworkID, void* UserData)
{
	UNREFERENCED_PARAMETER(NetworkID);
	UNREFERENCED_PARAMETER(UserData);

	if (SDKSTATUS_SUCCESS(gSdkContext->_SdkIsObjectSpellMissile(Object)))
	{
		void* caster;
		gSdkContext->_SdkGetMissileCaster(Object, &caster, nullptr);
		if (pSDK->Player->Object != caster)
			return;

		bool hasFinished;
		gSdkContext->_SdkHasMissileCompleted(Object, &hasFinished);

		if (!hasFinished)
			Orbwalker::MissileLaunched = pSDK->Game->GetGameTicks();
	}
}

#pragma endregion

void Orbwalker::Initialize()
{
	const auto localName = pSDK->Player->GetAIName();
	if (localName == "Graves") SpecialType = 1;
	else if (localName == "Jhin") SpecialType = 2;
	else if (localName == "Rengar") SpecialType = 3;
	else if (localName == "Kalista") SpecialType = 4;

	pSDK->EventHandler->RegisterEvent(eEventType::Tick, OnUpdate);
	pSDK->EventHandler->RegisterEvent(eEventType::AIAttack, OnAIAttack);
	pSDK->EventHandler->RegisterEvent(eEventType::AIProcessSpell, OnAIProcessSpell);
	pSDK->EventHandler->RegisterEvent(eEventType::AICastAttack, OnAICastAttack);
	pSDK->EventHandler->RegisterEvent(eEventType::ObjectDelete, OnObjectDelete);
}

// Turbo
OrbwalkerMode Orbwalker::GetOrbwalkerMode()
{
	if (pSDK->Menu->GetMenuKeyStatus("BBase.Orbwalker.LaneClear")) return OrbwalkerMode::LaneClear;
	if (pSDK->Menu->GetMenuKeyStatus("BBase.Orbwalker.LastHit")) return OrbwalkerMode::LastHit;
	if (pSDK->Menu->GetMenuKeyStatus("BBase.Orbwalker.Combo")) return OrbwalkerMode::Combo;
	if (pSDK->Menu->GetMenuKeyStatus("BBase.Orbwalker.Harass")) return OrbwalkerMode::Harras;
	if (pSDK->Menu->GetMenuKeyStatus("BBase.Orbwalker.Flee")) return OrbwalkerMode::Flee;

	return OrbwalkerMode::None;
}

bool Orbwalker::CanMove()
{
	int _localExtraWindup = 0;
	if (SpecialType == 3 && pSDK->Player->HasBuff("rengarqbase") || pSDK->Player->HasBuff("rengarqemp"))
	{
		_localExtraWindup = 200;
	}

	const int _gameTicks = pSDK->Game->GetGameTicks();

	if (_gameTicks >= MissileLaunched)
		return true;

	bool _canMove;
	gSdkContext->_SdkCanAIMove(pSDK->Player->Object, &_canMove);

	return std::find(NoCancelChamps.begin(), NoCancelChamps.end(), pSDK->Player->GetAIName()) != NoCancelChamps.end() || static_cast<int>(_gameTicks + pSDK->Game->GetPing() / 2) >= LastAutoAttackTick + static_cast<int>(pSDK->Player->GetAttackCastDelay() * 1000.f) + GetTotalWind() + _localExtraWindup && _canMove;
}

bool Orbwalker::CanAttack()
{
	float _extraAttackDelay = .0f;
	if (SpecialType == 1)
	{
		if (pSDK->Player->HasBuff("GravesBasicAttackAmmo1"))
			return false;

		const float _attackDelay = pSDK->Player->GetAttackDelay() * 1000.f;
		_extraAttackDelay = (_attackDelay * 1.0740296828f) - 716.2381256175f - _attackDelay;
	}
	else if (SpecialType == 2 && pSDK->Player->HasBuff("JhinPassiveReload"))
	{
		return false;
	}

	if (pSDK->Player->HasBuffType(BUFF_TYPE_POLYMORPH))
	{
		return false;
	}
	if (pSDK->Player->HasBuffType(BUFF_TYPE_BLIND) && SpecialType != 4)
	{
		return false;
	}

	bool _canAttack;
	gSdkContext->_SdkCanAIAttack(pSDK->Player->Object, &_canAttack);

	return static_cast<int>(pSDK->Game->GetGameTicks() + pSDK->Game->GetPing() / 2 + 25) >= LastAutoAttackTick + static_cast<int>(pSDK->Player->GetAttackDelay() * 1000.f) + GetTotalWind() && _canAttack;
}

bool Orbwalker::IsAutoAttackReset(std::string spellName)
{
	return std::find(AttackResets.begin(), AttackResets.end(), spellName) != AttackResets.end();
}

void Orbwalker::MoveTo(Vector3 position, bool pet)
{
	auto sdkPosition{ position.ToSDK() };
	gSdkContext->_SdkMoveLocalPlayer(&sdkPosition, pet);
}

void Orbwalker::MoveToMouse(bool pet)
{
	SDKPOINT sdkPoint = Utils::GetDirectXCursorPos();
	SDKVECTOR sdkVector = SDKVECTOR();
	gSdkContext->_SdkScreenToWorld(&sdkPoint, &sdkVector);

	if (pSDK->Player->GetDistanceSquared(sdkVector) > 100 * 100)
		gSdkContext->_SdkMoveLocalPlayer(&sdkVector, pet);
}

bool Orbwalker::GetOrbwalkerAttackStatus()
{
	return OrbwalkerStatus;
}

void Orbwalker::ToggleOrbwalkerAttack()
{
	OrbwalkerStatus = !OrbwalkerStatus;
}

bool Orbwalker::GetOrbwalkerStatus()
{
	return FullOrbwalkerStatus;
}

void Orbwalker::ToggleOrbwalker()
{
	FullOrbwalkerStatus = !FullOrbwalkerStatus;
}