#pragma once

#include <Windows.h>
#include "../sdkapi.h"

class Utils
{
public:
	static float GetDistance(SDKVECTOR startVec, SDKVECTOR endVec);
	static SDKPOINT GetDirectXCursorPos();
	static float GetProjectileSpeed(SDK_SPELL spell);
};