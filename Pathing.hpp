#pragma once

#include <vector>
#include "Math.hpp"
#include "IEntityManager.h"

class Pathing
{
public:
	// Path getting & sorting
	static std::vector<Vector3> GetPath(AIClient* unit);
	static std::vector<Vector3> CutPath(std::vector<Vector3> path, float distance);

	// Path operations
	static float GetPathLength(std::vector<Vector3> path);

	// Unit operations
	static Vector3 CalculateVelocity(Vector3 start, Vector3 end, float speed);
};
