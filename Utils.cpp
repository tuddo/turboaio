#include "Utils.hpp"
#include <cmath>
#include "Vars.h"

float Utils::GetDistance(SDKVECTOR startVec, SDKVECTOR endVec) { return sqrtf(powf(fabs(endVec.x - startVec.x), 2) + pow(fabs(endVec.y - startVec.y), 2) + pow(fabs(endVec.z- startVec.z), 2)); }

SDKPOINT Utils::GetDirectXCursorPos()
{
	SDKPOINT sdkPoint;
	gSdkContext->_SdkGetMouseScreenPosition(&sdkPoint);

	if (sdkPoint.x < 0 || sdkPoint.y < 0)
	{
		sdkPoint.x = 0;
		sdkPoint.y = 0;
	}

	return sdkPoint;
}

float Utils::GetProjectileSpeed(SDK_SPELL spell) { return spell.MissileSpeed; }