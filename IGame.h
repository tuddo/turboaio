#pragma once

#include <Windows.h>
#include "../sdkapi.h"
#include "Math.hpp"
#include <string>

struct IGame
{
	// Other things
	virtual unsigned int GetPing() = 0;
	virtual float GetGameTime() = 0;
	virtual unsigned int GetGameTicks() = 0;
	virtual float GetDistance(Vector3 vector1, Vector3 vector2) = 0;
	virtual float GetDistanceSquared(Vector3 vector1, Vector3 vector2) = 0;
	virtual Vector3 GetWorldCursorPosition() = 0;
	virtual int CountEnemiesAtPosition(Vector3 position, float range) = 0;
	virtual bool IsChatOpened() = 0;
	virtual bool IsOverlayOpened() = 0;
	virtual void SendChat(std::string message) = 0;
	virtual void ShowEmote(unsigned char emoteID) = 0;
	virtual bool IsHeroInGame(std::string heroName) = 0;
};