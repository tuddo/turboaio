#pragma once

#include "IEntityManager.h"

struct IHealthPrediction
{
	// Health Prediction
	virtual float GetHealthPrediction(std::shared_ptr<AIClient> AI, int Time) = 0;
};