#pragma once

#include <Windows.h>
#include "../sdkapi.h"

class Initializer
{
public:
	static HWND GameHWND;
	static WNDPROC OldWND;

	static void Initialize(PSDK_CONTEXT context, HINSTANCE hinstDLL);
};