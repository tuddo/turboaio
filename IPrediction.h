#pragma once

#include "Math.hpp"
#include <vector>
#include "IEntityManager.h"

// Hitchances
enum class SkillshotHitchance : int
{
	Collision,
	OutOfRange,
	Impossible,
	Low,
	Medium,
	High,
	VeryHigh,
	Dashing,
	Immobile,
	Uninitialized
};

// Skillshot types
enum class SkillshotType : int
{
	Line,
	Circle,
	Cone
};

// Skillshot collisions
enum class SkillshotCollision : int
{
	Minion,
	Enemy,
	Windwall,
	Wall,
	Ally
};

struct PREDICTION_INPUT
{
	bool AOE = false;
	bool Collision = false;
	std::vector<SkillshotCollision> CollisionObjects = { SkillshotCollision::Minion, SkillshotCollision::Windwall };
	float Delay = 0.f;
	float Radius = 1.f;
	float Range = FLT_MAX;
	float Speed = FLT_MAX;
	SkillshotType Type = SkillshotType::Line;
	AIClient* Unit;
	bool UseBoundingRadius = true;
	Vector3 From;
	Vector3 RangeCheckFrom;

	float RealRadius() const { return UseBoundingRadius ? Radius + Unit->GetBoundingRadius() : Radius; };
};

struct PREDICTION_OUTPUT
{
	PREDICTION_INPUT Input;
	int AOETargetsHitCount;
	std::vector<AIClient*> AOETargetsHit{};
	std::vector<AIClient*> CollisionObjects{};
	SkillshotHitchance Hitchance = SkillshotHitchance::Impossible;
	Vector3 CastPosition;
	Vector3 UnitPosition;
};

struct IPrediction
{
	// Prediction
	virtual PREDICTION_OUTPUT GetPrediction(PREDICTION_INPUT input, bool ft, bool checkCollision) = 0;
};