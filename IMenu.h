#pragma once

#include <map>
#include <functional>

enum eKeyCode : int
{
	KeyA = 65,
	KeyB = 66,
	KeyC = 67,
	KeyD = 68,
	KeyE = 69,
	KeyF = 70,
	KeyG = 71,
	KeyH = 72,
	KeyI = 73,
	KeyJ = 74,
	KeyK = 75,
	KeyL = 76,
	KeyM = 77,
	KeyN = 78,
	KeyO = 79,
	KeyP = 80,
	KeyQ = 81,
	KeyR = 82,
	KeyS = 83,
	KeyT = 84,
	KeyU = 85,
	KeyV = 86,
	KeyW = 87,
	KeyX = 88,
	KeyY = 89,
	KeyZ = 90,
	KeySpace = 32
};

#define MENUKEY_TYPE_HOTKEY 0
#define MENUKEY_TYPE_TOGGLE 1

struct MENUKEY_INFO
{
	int Type;
	std::string ConfigKey;
	eKeyCode DefaultKey;
	eKeyCode CurrentKey;
	bool Status;
};

struct IMenu
{
	virtual void CreateMenu(std::string menuLabel, std::function<void()> menuFunction) = 0;
	virtual void Tree(std::string treeLabel, std::string cfgExpanded, std::function<void()> treeFunction) = 0;
	virtual void Text(std::string label, ...) = 0;
	virtual void Checkbox(std::string label, std::string cfgChecked, bool defaultValue = false) = 0;
	virtual void Hotkey(std::string label, std::string cfgKey, eKeyCode defaultKey) = 0;
	virtual void Toggle(std::string label, std::string cfgKey, eKeyCode defaultKey, bool defaultStatus) = 0;
	virtual bool GetMenuKeyStatus(std::string cfgKey) = 0;
	virtual void SliderInt(std::string label, std::string cfgKey, int defaultValue, int min, int max) = 0;
	virtual void SliderFloat(std::string label, std::string cfgKey, float defaultValue, float min, float max, float power = 1.0f) = 0;
	virtual int GetInt(std::string cfgKey) = 0;
	virtual float GetFloat(std::string cfgKey) = 0;
	virtual bool GetBool(std::string cfgKey) = 0;
	virtual void List(std::string label, std::string cfgKey, const char* items[], int itemsCount, int defaultIndex = 0) = 0;
	virtual void SameLine() = 0;
};