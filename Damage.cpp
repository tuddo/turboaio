﻿#include "Damage.hpp"
#include "ItemDB.hpp"
#include "Vars.h"
#include "TargetSelector.hpp"

typedef sDamageInfo(*STATICPASSIVE)();
typedef sDamageInfo(*DYNAMICPASSIVE)(std::shared_ptr<AIClient> target);
std::vector<STATICPASSIVE> StaticPassives;
std::vector<DYNAMICPASSIVE> DynamicPassives;

sDamageInfo Damage::GetStaticItemsDamage(std::shared_ptr<AIClient> source)
{
	sDamageInfo damage{ .0f, .0f, .0f };

	bool hasShardItem = false;

	const sBuffInfo statikkInfo = source->GetBuff("itemstatikshankcharge");
	if (statikkInfo.Count == 100 && statikkInfo.Stacks == 1)
	{
		float tempDamage = .0f;

		for (auto item : source->GetItems())
		{
			if (hasShardItem)
				break;

			if (item.ItemID == eItemID::RapidFirecannon && !hasShardItem)
			{
				float damageArr[] = { 60.f, 60.f, 60.f, 60.f, 60.f, 67.f, 73.f, 79.f, 85.f, 91.f, 97.f, 104.f, 110.f, 116.f, 122.f, 128.f, 134.f, 140.f };
				tempDamage = damageArr[source->GetLevel() - 1];
				hasShardItem = true;
			}
			else if (item.ItemID == eItemID::StatikkShiv && !hasShardItem)
			{
				float damageArr[] = { 60.f, 60.f, 60.f, 60.f, 60.f, 67.f, 73.f, 79.f, 85.f, 91.f, 97.f, 104.f, 110.f, 116.f, 122.f, 128.f, 134.f, 140.f };
				tempDamage = damageArr[source->GetLevel() - 1];
				hasShardItem = true;
			}
			else if (item.ItemID == eItemID::KircheisShard && !hasShardItem)
			{
				tempDamage = 50.f;
				hasShardItem = true;
			}
		}

		damage.MagicalDamage += tempDamage;
	}

	if (pSDK->Player->HasItem(eItemID::RecurveBow))
		damage.PhysicalDamage += 15.f;
	if (pSDK->Player->HasItem(eItemID::Sheen))
	{
		if (source->GetBuff("sheen").Stacks == 1)
		{
			damage.PhysicalDamage += source->GetAttackDamage().Base;
		}
	}
	if (pSDK->Player->HasItem(eItemID::LichBane))
	{
		if (source->GetBuff("lichbane").Stacks == 1)
		{
			damage.MagicalDamage += 0.75f * source->GetAttackDamage().Base + 0.5f * source->GetAbilityPower();
		}
	}
	else if (pSDK->Player->HasItem(eItemID::TrinityForce))
	{
		if (source->GetBuff("TrinityForce").Stacks == 1)
		{
			damage.PhysicalDamage += 2.f * source->GetAttackDamage().Base;
		}
	}
	if (pSDK->Player->HasItem(eItemID::GuinsoosRageblade))
		damage.MagicalDamage += 15.f;
	if (pSDK->Player->HasItem(eItemID::NashorsTooth))
		damage.MagicalDamage += 15.f + 0.15f * source->GetAbilityPower();
	if (pSDK->Player->HasItem(eItemID::WitsEnd))
		damage.MagicalDamage += 42.f;

	return damage;
}

sDamageInfo Damage::GetDynamicItemsDamage(std::shared_ptr<AIClient> source, std::shared_ptr<AIClient> target)
{
	sDamageInfo damage{ .0f, .0f, .0f };

	for (auto item : source->GetItems())
	{
		// Add botrk bonus (current health based) damage to the targets.
		if (item.ItemID == eItemID::BladeoftheRuinedKing)
		{
			const float percentHealth = 0.08f * target->GetHealth().Current;
			if (percentHealth <= 15.f)
				damage.PhysicalDamage += 15.f;
			else
			{
				if (target->IsMinion())
					damage.PhysicalDamage += min(60.f, percentHealth);
				else
					damage.PhysicalDamage += percentHealth;
			}
		}

		// Add bloodrazor bonus (max health based) damage to targets.
		else if (item.ItemID == eItemID::EnchantmentBloodrazor)
		{
			const float percentHealth = 0.04f * target->GetHealth().Max;
			if (target->IsMinion())
				damage.PhysicalDamage += min(75.f, percentHealth);
			else
				damage.PhysicalDamage += percentHealth;
		}

		// Add the dedicated damage to minions
		if (target->IsMinion())
		{
			// If minion is a lane minion
			if (target->IsLaneMinion())
			{
				// Add 5 physical damage to the calculation in case the source AI has a starter item.
				if (item.ItemID == eItemID::DoransRing)
					damage.PhysicalDamage += 5.f;
				else if (item.ItemID == eItemID::DoransShield)
					damage.PhysicalDamage += 5.f;
			}
			// If minion is part of a jungle camp
			else if (target->IsMonster())
			{
				// Add 35 physical damage to the calculation if the source AI is jungler with Hunter's Machete
				if (item.ItemID == eItemID::HuntersMachete)
					damage.PhysicalDamage += 35.f;
				// Add 40 physical damage to the calculation if the source AI is jungler with Skirmisher's Sabre or Stalker's Blade
				else if (item.ItemID == eItemID::SkirmishersSabre || item.ItemID == eItemID::StalkersBlade)
					damage.PhysicalDamage += 40.f;
			}
		}
	}

	return damage;
}

void Damage::InitStaticHeroDamage()
{
	auto heroName{ pSDK->Player->GetAIName() };

	if (heroName.find("Akali") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("akalipweapon").Stacks == 1)
			{
				float damageArr[] = { 39.f, 42.f, 45.f, 48.f, 51.f, 54.f, 57.f, 60.f, 69.f, 78.f, 87.f, 96.f, 105.f, 120.f, 135.f, 150.f, 165.f, 180.f };

				const float baseDamage = damageArr[pSDK->Player->GetLevel() - 1];
				const float bonusADDamage = .9f * pSDK->Player->GetAttackDamage().Bonus;
				const float bonusAPDamage = .7f * pSDK->Player->GetAbilityPower();

				damage.MagicalDamage = baseDamage + bonusADDamage + bonusAPDamage;
			}

			return damage;
		});
	}
	else if (heroName.find("Alistar") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("alistareattack").Stacks == 1)
				damage.MagicalDamage = 20.f + 15.f * pSDK->Player->GetLevel();

			return damage;
		});
	}
	else if (heroName.find("Bard") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("bardpspiritammocount").Count > 0)
				damage.MagicalDamage = 40.f + 15.f * roundf(pSDK->Player->GetBuff("bardpdisplaychimecount").Count / 5.f) + .3f * pSDK->Player->GetAbilityPower();

			return damage;
		});
	}
	else if (heroName.find("Blitzcrank") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			damage.PhysicalDamage = pSDK->Player->GetAttackDamage().Total;

			return damage;
		});
	}
	else if (heroName.find("Camille") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("CamilleQ").Stacks == 1)
			{
				float physicalDamage[] = { 0.2f, 0.25f, 0.3f, 0.35f, 0.4f };

				damage.PhysicalDamage = physicalDamage[pSDK->Player->GetSpell(0).Level - 1] * pSDK->Player->GetAttackDamage().Total;
			}
			else if (pSDK->Player->GetBuff("CamilleQ2").Stacks == 1)
			{
				float physicalDamage[] = { 0.4f, 0.5f, 0.6f, 0.7f, 0.8f };

				const int spellLevel = pSDK->Player->GetSpell(0).Level;
				const float mixedDamage = physicalDamage[spellLevel - 1] * pSDK->Player->GetAttackDamage().Total;

				damage.TrueDamage = min(1.f, 0.36f + 0.04f * pSDK->Player->GetLevel()) * mixedDamage;
				damage.PhysicalDamage = mixedDamage - damage.TrueDamage;
			}

			return damage;
		});
	}
	else if (heroName.find("Corki") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			const float attackDamage = pSDK->Player->GetAttackDamage().Total;

			damage.PhysicalDamage = -attackDamage;

			damage.PhysicalDamage += .2f * attackDamage;
			damage.MagicalDamage = .8f * attackDamage;

			return damage;
		});
	}
	else if (heroName.find("Darius") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("DariusNoxianTacticsONH").Stacks == 1)
			{
				float bonusDamage[] = { .5f, .55f, .6f, .65f, .7f };

				damage.PhysicalDamage = bonusDamage[pSDK->Player->GetSpell(1).Level - 1];
			}

			return damage;
		});
	}
	else if (heroName.find("Diana") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("dianapassivemarker").Stacks == 2)
			{
				float baseDamage[] = { 20.f, 25.f, 30.f, 35.f, 40.f, 50.f, 60.f, 70.f, 80.f, 90.f, 105.f, 120.f, 135.f, 155.f, 175.f, 200.f, 225.f, 250.f };

				damage.MagicalDamage = baseDamage[pSDK->Player->GetLevel() - 1] + 0.8f * pSDK->Player->GetAbilityPower();
			}

			return damage;
		});
	}
	else if (heroName.find("DrMundo") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("Masochism").Stacks == 1)
			{
				damage.PhysicalDamage = (0.025f + 0.005f * pSDK->Player->GetSpell(2).Level) * pSDK->Player->GetHealth().Max;
			}

			return damage;
		});
	}
	else if (heroName.find("Draven") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("DravenSpinning").Stacks == 1)
			{
				float basePercent[] = { 35.f, 40.f, 45.f, 50.f, 55.f };
				float bonusPercent[] = { .65f, .75f, .85f, .95f, 1.05f };

				const int arrayIndex = pSDK->Player->GetSpell(0).Level - 1;

				damage.PhysicalDamage = basePercent[arrayIndex] + bonusPercent[arrayIndex] * pSDK->Player->GetAttackDamage().Bonus;
			}

			return damage;
		});
	}
	else if (heroName.find("Elise") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("EliseR").Stacks == 1)
				damage.MagicalDamage = 10.f * pSDK->Player->GetSpell(0).Level + 0.3f * pSDK->Player->GetAbilityPower();

			return damage;
		});
	}
	else if (heroName.find("Fizz") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("FizzW").Stacks == 1)
				damage.MagicalDamage = 50.f + 20.f * pSDK->Player->GetSpell(1).Level + 0.5f * pSDK->Player->GetAbilityPower();

			return damage;
		});
	}
	else if (heroName.find("Galio") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("galiopassivebuff").Stacks == 1)
			{
				const float baseDamage = 8.f + 4.f * pSDK->Player->GetLevel();
				float bonusMagicResist;

				gSdkContext->_SdkGetAIBonusMagicResist(pSDK->Player->Object, &bonusMagicResist);

				damage.MagicalDamage = baseDamage + pSDK->Player->GetAttackDamage().Total + 0.5f * pSDK->Player->GetAbilityPower() + .4f * bonusMagicResist;
			}

			return damage;
		});
	}
	else if (heroName.find("Garen") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("garenq").Stacks == 1)
				damage.PhysicalDamage = 30.f + 30.f * (pSDK->Player->GetSpell(0).Level - 1) + 0.4f * pSDK->Player->GetAttackDamage().Total;

			return damage;
		});
	}
	else if (heroName.find("Hecarim") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("hecarimrampspeed").Stacks == 1)
				damage.PhysicalDamage += 20.f + 70.f * pSDK->Player->GetSpell(2).Level + pSDK->Player->GetAttackDamage().Total;

			return damage;
		});
	}
	else if (heroName.find("Irelia") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("ireliapassivestacksmax").Stacks > 0)
				damage.MagicalDamage = 12.f + 3.f * pSDK->Player->GetLevel() + (.25f * pSDK->Player->GetAttackDamage().Bonus);

			return damage;
		});
	}
	else if (heroName.find("Jax") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("JaxEmpowerTwo").Stacks > 0)
				damage.MagicalDamage = 40.f + (35.f * pSDK->Player->GetSpell(1).Level - 1) + .60f * pSDK->Player->GetAbilityPower();

			return damage;
		});
	}
	else if (heroName.find("Jinx") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("JinxQ").Stacks == 1)
				damage.PhysicalDamage = 0.10f * pSDK->Player->GetAttackDamage().Total;

			return damage;
		});
	}
	else if (heroName.find("Kaisa") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			const int level = pSDK->Player->GetLevel();
			const int plasmaStacks = pSDK->Player->GetBuff("kaisapassivemarker").Stacks;

			const float baseDamage = level < 3 ? 4.f : level < 6 ? 5.f : level < 9 ? 6.f : level < 11 ? 7.f : level < 14 ? 8.f : level < 17 ? 9.f : 10.f;
			const float baseDamagePerPlasma = level < 4 ? 1.f : level < 8 ? 2.f : level < 12 ? 3.f : level < 16 ? 4.f : 5.f;

			const float apDamagePercent = plasmaStacks < 1 ? 0.10f : plasmaStacks < 2 ? .125f : plasmaStacks < 3 ? .15f : plasmaStacks < 4 ? 0.175f : 0.20f;

			damage.MagicalDamage = baseDamage + baseDamagePerPlasma + apDamagePercent * pSDK->Player->GetAbilityPower();

			return damage;
		});
	}
	else if (heroName.find("Kalista") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			const float attackDamage = pSDK->Player->GetAttackDamage().Total;
			damage.PhysicalDamage -= .1f * attackDamage;

			return damage;
		});
	}
	else if (heroName.find("Kassadin") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			const int wLevel = pSDK->Player->GetSpell(1).Level;
			const float localAP = pSDK->Player->GetAbilityPower();
			if (wLevel > 0)
			{
				if (pSDK->Player->GetBuff("NetherBlade").Stacks == 1)
					damage.MagicalDamage = 40.f + 25.f * (wLevel - 1) + 0.8f * localAP;
				else
					damage.MagicalDamage = 20.f + 0.1f * localAP;
			}

			return damage;
		});
	}
	else if (heroName.find("Kennen") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("kennendoublestrikelive").Stacks == 1)
				damage.MagicalDamage += 10.f + 5.f * (pSDK->Player->GetSpell(1).Level - 1);

			return damage;
		});
	}
	else if (heroName.find("KhaZix") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("KhazixPDamage").Stacks == 1)
				damage.MagicalDamage = 6.f + 8.f * pSDK->Player->GetLevel() + 0.4f * pSDK->Player->GetAttackDamage().Bonus;

			return damage;
		});
	}
	else if (heroName.find("Teemo") != std::string::npos)
	{
		StaticPassives.push_back([]() -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			const auto spellInfo = pSDK->Player->GetSpell(2);
			if (spellInfo.Learned)
			{
				damage.MagicalDamage += spellInfo.Level * 10.f + 0.3f * pSDK->Player->GetAbilityPower();
			}

			return damage;
		});
	}
}

void Damage::InitDynamicHeroDamage()
{
	auto heroName{ pSDK->Player->GetAIName() };

	if (heroName.find("Aatrox") != std::string::npos)
	{
		DynamicPassives.push_back([](std::shared_ptr<AIClient> target) -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("aatroxpassiveready").Stacks == 1)
			{
				float bonusArr[] = { .08f, .0847f, .0894f, .0941f, .0988f, .1035f, .1082f, .1129f, .1176f, .1224f, .1271f, .1318f, .1365f, .1412f, .1459f, .1506f, .1553f, .16f };
				const float bonusPercent = bonusArr[pSDK->Player->GetLevel() - 1];

				if (target->IsMinion())
					damage.PhysicalDamage = min(400, bonusPercent * target->GetHealth().Max);
				else
					damage.PhysicalDamage = bonusPercent * target->GetHealth().Max;
			}

			return damage;
		});
	}
	else if (heroName.find("Ashe") != std::string::npos)
	{
		DynamicPassives.push_back([](std::shared_ptr<AIClient> target) -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (target->GetBuff("ashepassiveslow").Stacks == 1)
				damage.PhysicalDamage = (.1f + pSDK->Player->GetCriticalStrike()) * pSDK->Player->GetAttackDamage().Total;;

			return damage;
		});
	}
	else if (heroName.find("Braum") != std::string::npos)
	{
		DynamicPassives.push_back([](std::shared_ptr<AIClient> target) -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (target->GetBuff("BraumMark").Stacks == 3)
				damage.MagicalDamage = 16.f + 10.f * pSDK->Player->GetLevel();

			return damage;
		});
	}
	else if (heroName.find("Caitlyn") != std::string::npos)
	{
		DynamicPassives.push_back([](std::shared_ptr<AIClient> target) -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("caitlynheadshot").Stacks == 1)
			{
				if (!target->IsHero())
					damage.PhysicalDamage = (1.f + 1.25f * pSDK->Player->GetCriticalStrike()) * pSDK->Player->GetAttackDamage().Total;
				else
				{
					const int level = pSDK->Player->GetLevel();
					float basePercent = .0f;

					if (level < 7)
						basePercent = .5f;
					else if (level >= 7 && level < 13)
						basePercent = .75f;
					else if (level >= 13)
						basePercent = 1.f;

					damage.PhysicalDamage = (basePercent + 1.25f * pSDK->Player->GetCriticalStrike()) * pSDK->Player->GetAttackDamage().Total;
				}
			}

			return damage;
		});
	}
	else if (heroName.find("ChoGath") != std::string::npos)
	{
		DynamicPassives.push_back([](std::shared_ptr<AIClient> target) -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("VorpalSpikes").Count > 0)
			{
				float baseMagic[] = { 0.2f, .3f, .4f, .5f, .6f };

				const float scaleMagic = baseMagic[pSDK->Player->GetSpell(2).Level - 1] + 0.3f * pSDK->Player->GetAbilityPower();
				const float targetBasedMagic = (0.03f + 0.005f * pSDK->Player->GetBuff("Feast").Stacks) * target->GetHealth().Max;

				damage.MagicalDamage = scaleMagic + targetBasedMagic;
			}

			return damage;
		});
	}
	else if (heroName.find("Ekko") != std::string::npos)
	{
		DynamicPassives.push_back([](std::shared_ptr<AIClient> target) -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			const float abilityPower = pSDK->Player->GetAbilityPower();

			if (target->GetBuff("ekkostacks").Stacks == 2)
			{
				float baseDamage[] = { 30.f, 40.f, 50.f, 60.f, 70.f, 80.f, 85.f, 90.f, 95.f, 100.f, 105.f, 110.f, 115.f, 120.f, 125.f, 130.f, 135.f, 140.f };

				if (target->IsMonster())
					damage.MagicalDamage = min(600.f, baseDamage[pSDK->Player->GetLevel() - 1] * 2 + 1.6f * abilityPower);
				else
					damage.MagicalDamage = baseDamage[pSDK->Player->GetLevel() - 1] + 0.8f * abilityPower;
			}

			const auto targetHealth = target->GetHealth();
			if (targetHealth.Current < 0.3f * targetHealth.Max)
			{
				float tempDamage = (0.03f + 0.03f * static_cast<int>(abilityPower / 100.f)) * (targetHealth.Max - targetHealth.Current);

				if (target->IsMinion())
				{
					if (tempDamage < 15.f)
						tempDamage = 15.f;
					else if (tempDamage > 150.f)
						tempDamage = 150.f;
				}

				damage.MagicalDamage += tempDamage;
			}

			if (target->GetBuff("ekkoeattackbuff").Stacks == 1)
			{
				float baseDamage[] = { 40.f, 65.f, 90.f, 115.f, 140.f };

				damage.MagicalDamage += baseDamage[pSDK->Player->GetSpell(2).Level - 1] + 0.4f * abilityPower;
			}

			return damage;
		});
	}
	else if (heroName.find("Evelynn") != std::string::npos)
	{
		DynamicPassives.push_back([](std::shared_ptr<AIClient> target) -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (target->GetBuff("evelynnqdebuff").Stacks > 0)
				damage.MagicalDamage += 10.f * pSDK->Player->GetSpell(0).Level + 0.25f * pSDK->Player->GetAbilityPower();

			return damage;
		});
	}
	else if (heroName.find("Ezreal") != std::string::npos)
	{
		DynamicPassives.push_back([](std::shared_ptr<AIClient> target) -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			const int damageIndex = pSDK->Player->GetSpell(1).Level - 1;

			if (target->GetBuff("ezrealwattach").Stacks == 1)
			{
				float baseDamage[] = { 80.f, 135.f, 190.f, 245.f, 300.f };

				damage.MagicalDamage = baseDamage[damageIndex] + 0.6f * pSDK->Player->GetAttackDamage().Bonus + (0.70f +
					0.05f * damageIndex) * pSDK->Player->GetAbilityPower();
			}

			return damage;
		});
	}
	else if (heroName.find("Gnar") != std::string::npos)
	{
		DynamicPassives.push_back([](std::shared_ptr<AIClient> target) -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (target->GetBuff("gnarwproc").Stacks == 2)
			{
				const int spellWIndex = pSDK->Player->GetSpell(1).Level - 1;

				const float baseDamage = 10.f + 10.f * spellWIndex;
				const float baseHealthBasedPercent = .06f + .02f * spellWIndex;
				const float baseAPScale = pSDK->Player->GetAbilityPower();

				if (target->IsMonster())
					damage.MagicalDamage = min(baseDamage + baseHealthBasedPercent * target->GetHealth().Max + baseAPScale, 100.f + 50.f * spellWIndex + baseAPScale);
				else
					damage.MagicalDamage = baseDamage + baseHealthBasedPercent * target->GetHealth().Max + baseAPScale;
			}

			return damage;
		});
	}
	else if (heroName.find("Vayne") != std::string::npos)
	{
		DynamicPassives.push_back([](std::shared_ptr<AIClient> target) -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (target->GetBuff("VayneSilveredDebuff").Stacks == 2)
			{
				float minimumDamage[] = { 50.f, 65.f, 80.f, 95.f, 110.f };
				float bonusPercent[] = { .04f, .065f, .09f, .115f, .14f };

				SDK_SPELL spell;
				gSdkContext->_SdkGetAISpell(target->Object, 1, &spell);
				
				if (target->IsMinion())
				{
					if (spell.Level > 0)
					{
						const float minimum = minimumDamage[spell.Level - 1];
						const float bonus = bonusPercent[spell.Level - 1] * target->GetHealth().Max;

						damage.TrueDamage = min(200.f, minimum + bonus);
					}
				}
				else
				{
					if (spell.Level > 0)
					{
						const float minimum = minimumDamage[spell.Level - 1];
						const float bonus = bonusPercent[spell.Level - 1] * target->GetHealth().Max;

						damage.TrueDamage = minimum + bonus;
					}
				}
			}

			return damage;
		});
	}
	else if (heroName.find("Illaoi") != std::string::npos)
	{
		DynamicPassives.push_back([](std::shared_ptr<AIClient> target) -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("IllaoiW").Stacks > 0)
			{
				float baseDamage[] = { 0.03f, 0.035f, 0.04f, 0.045f, 0.05f };
				const float bonusPercent = .02f * static_cast<int>(pSDK->Player->GetAttackDamage().Total / 100.f);

				if (target->IsHero())
					damage.PhysicalDamage = (baseDamage[pSDK->Player->GetSpell(1).Level - 1] + bonusPercent) * target->GetHealth().Max;
				else
					damage.PhysicalDamage = min(300.f, (baseDamage[pSDK->Player->GetSpell(1).Level - 1] + bonusPercent) * target->GetHealth().Max);
			}

			return damage;
		});
	}
	else if (heroName.find("Jarvan") != std::string::npos)
	{
		DynamicPassives.push_back([](std::shared_ptr<AIClient> target) -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (target->GetBuff("jarvanivmartialcadencecheck").Stacks <= 0)
			{
				float hpDamage = 0.08f * target->GetHealth().Current;

				if (target->IsHero())
					damage.PhysicalDamage = min(20.f, hpDamage);
				else
				{
					if (hpDamage > 400.f)
						hpDamage = 400.f;

					damage.PhysicalDamage = max(20.f, hpDamage);
				}
			}

			return damage;
		});
	}
	else if (heroName.find("Kayle") != std::string::npos)
	{
		DynamicPassives.push_back([](std::shared_ptr<AIClient> target) -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			const int eLevel = pSDK->Player->GetSpell(2).Level;
			if (eLevel > 0)
			{
				if (pSDK->Player->GetBuff("KayleE").Stacks == 1)
				{
					const auto targetHealth = target->GetHealth();
					damage.MagicalDamage = (0.10f + 0.025f * (eLevel - 1)) * targetHealth.Max - targetHealth.Current;
				}
				else
					damage.MagicalDamage = 10.f + 5.f * (eLevel - 1) + 0.1f * pSDK->Player->GetAttackDamage().Total + 0.2f * pSDK->Player->GetAbilityPower();
			}
			if (pSDK->Player->GetBuff("kaylepassiverank1").Stacks == 1)
			{
				if (pSDK->Player->GetBuff("kayleenrage").Stacks == 1)
				{
					if (!target->IsTurret())
					{
						damage.MagicalDamage += 10.f;
						if (eLevel >= 2)
							damage.MagicalDamage += 5.f * eLevel - 1;
					}
				}
			}

			return damage;
		});
	}
	else if (heroName.find("KogMaw") != std::string::npos)
	{
		DynamicPassives.push_back([](std::shared_ptr<AIClient> target) -> sDamageInfo
		{
			sDamageInfo damage{ .0f, .0f, .0f };

			if (pSDK->Player->GetBuff("KogMawBioArcaneBarrage").Stacks == 1)
				damage.MagicalDamage = (0.03f + 0.0075f * (pSDK->Player->GetSpell(1).Level - 1) + 0.01f * static_cast<int>(target->GetAbilityPower() / 100.f)) * target->GetHealth().Max;

			return damage;
		});
	}
}

sDamageInfo Damage::GetStaticHeroDamage()
{
	sDamageInfo damage{ .0f, .0f, .0f };

	for (auto passive : StaticPassives)
	{
		const sDamageInfo tempDamage = passive();

		damage.PhysicalDamage += tempDamage.PhysicalDamage;
		damage.MagicalDamage += tempDamage.MagicalDamage;
		damage.TrueDamage += tempDamage.TrueDamage;
	}

	return damage;
}

sDamageInfo Damage::GetDynamicHeroDamage(std::shared_ptr<AIClient> target)
{
	sDamageInfo damage{ .0f, .0f, .0f };

	for (auto passive : DynamicPassives)
	{
		const sDamageInfo tempDamage = passive(target);

		damage.PhysicalDamage += tempDamage.PhysicalDamage;
		damage.MagicalDamage += tempDamage.MagicalDamage;
		damage.TrueDamage += tempDamage.TrueDamage;
	}

	return damage;
}

float Damage::GetDamageReductionModifier(AIClient* source, std::shared_ptr<AIClient> target, float amount, eDamageType damageType)
{
	if (source->IsHero())
	{
		if (source->GetBuff("SummonerExhaust").Stacks == 1)
			amount *= 0.6f;
	}

	if (target->IsHero())
	{
		auto targetHero = dynamic_pointer_cast<AIHeroClient>(target);
		const auto garenBuff = targetHero->GetBuff("GarenW");

		if (damageType == Physical)
		{
			if (targetHero->GetBuff("Tantrum").Stacks == 1)
			{
				float bonusArmor;
				gSdkContext->_SdkGetAIBonusArmor(targetHero->Object, &bonusArmor);

				amount -= 2.f + 2.f * (targetHero->GetSpell(2).Level - 1) + 0.03f * bonusArmor + 0.03f * targetHero->GetMagicResist();
			}
			else if (targetHero->GetBuff("ireliawdefense").Stacks == 1)
			{
				amount *= 1.f - (.5f + .07f * static_cast<int>(targetHero->GetAbilityPower() / 100));
			}
		}
		else if (damageType == Magical)
		{
			if (targetHero->GetBuff("voidstone").Stacks == 1)
			{
				amount *= 0.85f;
			}
		}

		if (targetHero->GetBuff("FerociousHowl").Stacks == 1)
		{
			float damageProcent[] = { .55f, .65f, .75f };
			amount *= 1.f - damageProcent[targetHero->GetSpell(3).Level - 1];
		}
		else if (targetHero->GetBuff("AnnieE").Stacks == 1)
		{
			amount *= 1.f - (.16f + .06f * (targetHero->GetSpell(2).Level - 1));
		}
		else if (targetHero->GetBuff("galiowbuff").Stacks == 1)
		{
			float bonusMR;
			gSdkContext->_SdkGetAIBonusMagicResist(targetHero->Object, &bonusMR);

			if (damageType == Physical)
			{
				const float baseProcent = 0.1f + 0.025f * (targetHero->GetSpell(1).Level - 1);
				const float bonusAPProcent = .025f * static_cast<int>(targetHero->GetAbilityPower() / 100.f);
				const float bonusMRProcent = .04f * static_cast<int>(bonusMR / 100.f);

				amount *= 1.f - (baseProcent + bonusAPProcent + bonusMRProcent);
			}
			else if (damageType == Magical)
			{
				const float baseProcent = 0.2f + 0.05f * (targetHero->GetSpell(1).Level - 1);
				const float bonusAPProcent = .05f * static_cast<int>(targetHero->GetAbilityPower() / 100.f);
				const float bonusMRProcent = .08f * static_cast<int>(bonusMR / 100.f);

				amount *= 1.f - (baseProcent + bonusAPProcent + bonusMRProcent);
			}
		}
		else if (garenBuff.Stacks == 1)
		{
			if (pSDK->Game->GetGameTime() - garenBuff.StartTime < 0.75f)
				amount *= .4f;
			else
				amount *= .7f;
		}
		else if (targetHero->GetBuff("gragaswself").Stacks == 1)
		{
			const float baseProcent = 0.1f + 0.02f * (targetHero->GetSpell(1).Level - 1);
			const float bonusAPProcent = 0.04f * static_cast<int>(targetHero->GetAbilityPower() / 100.f);

			amount *= 1.f - (baseProcent + bonusAPProcent);
		}
		else if (targetHero->GetBuff("Meditate").Stacks == 1)
		{
			if (source->IsTurret())
			{
				amount *= 1.f - (.6f + .02f * (targetHero->GetSpell(2).Level - 1)) / 2;
			}
			else
			{
				amount *= 1.f - (.6f + .02f * (targetHero->GetSpell(2).Level - 1));
			}
		}
		else if (targetHero->GetBuff("WarwickE").Stacks == 1)
		{
			amount *= 1.f - (.35f + .05f * (targetHero->GetSpell(2).Level - 1));
		}
	}

	return amount;
}

float Damage::GetPhysicalDamage(AIClient* source, std::shared_ptr<AIClient> target, float damage)
{
	lsPenetrationProperty armorPenetration = source->GetArmorPenetration();
	float bonusArmorPenetration;
	gSdkContext->_SdkGetAIBonusArmorPenetration(source->Object, &bonusArmorPenetration);

	if (source->IsMinion())
	{
		armorPenetration.Flat = .0f;
		armorPenetration.Percent = 1.f;
		bonusArmorPenetration = 1.f;
	}
	if (source->IsTurret())
	{
		armorPenetration.Flat = .0f;
		armorPenetration.Percent = 1.f;
		bonusArmorPenetration = 1.f;

		if (target->IsMinion())
		{
			damage *= 1.25f;
			if (target->GetAIName().find("MinionSiege") != string::npos)
				damage *= 0.7f;

			return damage;
		}
	}

	float lethality;
	gSdkContext->_SdkGetAILethality(source->Object, &lethality);

	const float armor = target->GetArmor();
	float bonusArmor;
	gSdkContext->_SdkGetAIBonusArmor(target->Object, &bonusArmor);

	const float flatArmorPen = lethality * (0.6f + 0.4f * pSDK->Player->GetLevel() / 18.f);
	float value;
	if (armor < 0)
		value = 2 - 100 / (100 - armor);
	else if (armor * armorPenetration.Percent - bonusArmor * (1 - bonusArmorPenetration) - flatArmorPen < 0)
		value = 1;
	else
		value = 100 / (100 + armor * armorPenetration.Percent - bonusArmor * (1 - bonusArmorPenetration) - flatArmorPen);

	return GetDamageReductionModifier(source, target, value * damage, Physical);
}
float Damage::GetMagicalDamage(AIClient* source, std::shared_ptr<AIClient> target, float damage)
{
	const float magicResist = target->GetMagicResist();
	const lsPenetrationProperty magicPenetration = source->GetMagicPenetration();

	float value;
	if (magicResist < 0)
		value = 2 - 100 / (100 - magicResist);
	else if (magicResist * magicPenetration.Percent - magicPenetration.Flat < 0)
		value = 1;
	else
		value = 100 / (100 + (magicResist * magicPenetration.Percent) - magicPenetration.Flat);

	return GetDamageReductionModifier(source, target, value * damage, Magical);
}

sDamageInfo Damage::GetAutoAttackDamage(std::shared_ptr<AIClient> target)
{
	sDamageInfo tempDamage{ pSDK->Player->GetAttackDamage().Total, .0f, .0f };

	#pragma region Bonus Damage

	// Add the static damage from items to the calculation process.
	const sDamageInfo staticItems = GetStaticItemsDamage(pSDK->Player);
	tempDamage.PhysicalDamage += staticItems.PhysicalDamage;
	tempDamage.MagicalDamage += staticItems.MagicalDamage;
	tempDamage.TrueDamage += staticItems.TrueDamage;

	// Add the dynamic damage from items to the calculation process.
	const sDamageInfo dynamicItems = GetDynamicItemsDamage(pSDK->Player, target);
	tempDamage.PhysicalDamage += dynamicItems.PhysicalDamage;
	tempDamage.MagicalDamage += dynamicItems.MagicalDamage;
	tempDamage.TrueDamage += dynamicItems.TrueDamage;

	// Add the dynamic static from heroes to the calculation process.
	const sDamageInfo staticHeroes = GetStaticHeroDamage();
	tempDamage.PhysicalDamage += staticHeroes.PhysicalDamage;
	tempDamage.MagicalDamage += staticHeroes.MagicalDamage;
	tempDamage.TrueDamage += staticHeroes.TrueDamage;

	// Add the dynamic damage from heroes to the calculation process.
	const sDamageInfo dynamicHeroes = GetDynamicHeroDamage(target);
	tempDamage.PhysicalDamage += dynamicHeroes.PhysicalDamage;
	tempDamage.MagicalDamage += dynamicHeroes.MagicalDamage;
	tempDamage.TrueDamage += dynamicHeroes.TrueDamage;

	#pragma endregion

	sDamageInfo damage{};
	damage.PhysicalDamage = GetPhysicalDamage(pSDK->Player.get(), target, tempDamage.PhysicalDamage);
	damage.MagicalDamage = GetMagicalDamage(pSDK->Player.get(), target, tempDamage.MagicalDamage);
	damage.TrueDamage = tempDamage.TrueDamage;

	//gSdkContext->_SdkConsoleClear();
	//gSdkContext->_SdkConsoleWrite("Physical => %.02f | Magical => %.02f | True => %.02f | Total => %.02f", damage.PhysicalDamage, damage.MagicalDamage, damage.TrueDamage, damage.PhysicalDamage + damage.MagicalDamage + damage.TrueDamage);

	return damage;
}

void __cdecl Damage_Scene(void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	gSdkContext->_SdkConsoleClear();
	gSdkContext->_SdkConsoleWrite("== LOCAL BUFFS ==");
	for (const auto& buff : pSDK->Player->GetBuffs())
	{
		gSdkContext->_SdkConsoleWrite("Name => %s | Count => %d | Stacks => %d | STime => %.02f | ETime => %.02f", buff.Name.c_str(), buff.Count, buff.Stacks, buff.StartTime, buff.EndTime);
	}
	if (TargetSelector::ManualForceTarget != nullptr)
	{
		gSdkContext->_SdkConsoleWrite("== TARGET BUFFS ==");
		for (const auto& buff : TargetSelector::ManualForceTarget->GetBuffs())
		{
			gSdkContext->_SdkConsoleWrite("Name => %s | Count => %d | Stacks => %d | STime => %.02f | ETime => %.02f", buff.Name.c_str(), buff.Count, buff.Stacks, buff.StartTime, buff.EndTime);
		}
	}
}

void Damage::Initialize()
{
	InitStaticHeroDamage();
	InitDynamicHeroDamage();

	//pSDK->EventHandler->RegisterEvent(eEventType::GameScene, Damage_Scene);
}