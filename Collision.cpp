#include "Collision.hpp"
#include "Vars.h"
#include "Math.hpp"
#include "Color.h"

Collision::WALL_INFO Collision::Wall{};

bool __cdecl Collision_ObjectCreate(void* Object, unsigned int NetworkID, void* UserData)
{
	UNREFERENCED_PARAMETER(NetworkID);
	UNREFERENCED_PARAMETER(UserData);

	const char* objName;
	gSdkContext->_SdkGetObjectName(Object, &objName);
	if (string(objName).find("W_windwall_enemy") != string::npos)
	{
		const auto object = new GameObject();
		object->Object = Object;
		Collision::Wall.WallObject = object;
	}
	return true;
}

bool __cdecl Collision_ObjectDelete(void* Object, unsigned int NetworkID, void* UserData)
{
	UNREFERENCED_PARAMETER(NetworkID);
	UNREFERENCED_PARAMETER(UserData);

	const char* objName;
	gSdkContext->_SdkGetObjectName(Object, &objName);
	if (string(objName).find("windwall") != string::npos)
	{
		delete Collision::Wall.WallObject;
	}
	return true;
}

void __cdecl Collision_OnProcessSpell(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	if (AI == nullptr)
		return;

	auto objAI = pSDK->EntityManager->GetAIByObject(AI);
	if (objAI == nullptr)
		return;
	if (!objAI->IsHero())
		return;

	if (objAI->IsValid(4000) && !strcmp(SpellCast->Spell.ScriptName, "YasuoW"))
	{
		Collision::Wall.WallLevel = SpellCast->Spell.Level;
		Collision::Wall.WallCast = pSDK->Game->GetGameTicks();
		Collision::Wall.WallCastPosition = SpellCast->EndPosition;
	}
}

bool Collision::GetWallCollision(Vector2& startPos, Vector2& endPos, float width)
{
	float distance{ startPos.GetDistance(endPos) };
	Vector2 direction{ (endPos - startPos).Normalized() };
	float step{ distance / 10.f };
	size_t max_steps{ static_cast<size_t>(ceil(distance / step)) };

	for (size_t i = 1; i <= max_steps; i++)
	{
		Vector2 currentCheck{ startPos + (endPos - startPos).Normalized() * (i * step) };
		Vector2 currentCheckPerp{ currentCheck.Perpendicular().Normalized() };

		SDKVECTOR currentCheckSDK{ currentCheck.ToSDK() };

		bool isWall;
		gSdkContext->_SdkIsLocationWall(&currentCheckSDK, &isWall);
		if (isWall)
		{
			SDKVECTOR startPosSDK{ pSDK->Player->GetPosition().ToSDK() };
			SDKVECTOR endPosSDK{ pSDK->Game->GetWorldCursorPosition().ToSDK() };

			//gSdkContext->_SdkDrawLine(&startPosSDK, &endPosSDK, 10.f, &Color::Red, 0);
			return true;
		}
	}

	return false;
}

void collisionTest(void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	Vector2 playerPos = pSDK->Player->GetPosition().To2D();
	Vector2 cursorPos = pSDK->Game->GetWorldCursorPosition().To2D();

	SDKVECTOR playerPosSDK{ pSDK->Player->GetPosition().ToSDK() };
	SDKVECTOR cursorPosSDK{ pSDK->Game->GetWorldCursorPosition().ToSDK() };

	const bool isCollision = pSDK->Collision->GetWallCollision(playerPos, cursorPos, 10.f);
	/*if (isCollision)
		gSdkContext->_SdkDrawLine(&playerPosSDK, &cursorPosSDK, 10.f, &Color::Red, 0);
	else
		gSdkContext->_SdkDrawLine(&playerPosSDK, &cursorPosSDK, 10.f, &Color::Green, 0);*/
}

void Collision::Initialize()
{
	//pSDK->EventHandler->RegisterEvent(eEventType::GameScene, collisionTest);
	pSDK->EventHandler->RegisterEvent(eEventType::ObjectCreate, Collision_ObjectCreate);
	pSDK->EventHandler->RegisterEvent(eEventType::ObjectDelete, Collision_ObjectDelete);
	pSDK->EventHandler->RegisterEvent(eEventType::AIProcessSpell, Collision_OnProcessSpell);
}
