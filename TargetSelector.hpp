#pragma once

#include "IEntityManager.h"
#include "ITargetSelector.h"
#include <map>

enum class eTargetingMode : int
{
	AutoPriority,
	MostAD,
	MostAP,
	Closest,
	NearMouse,
	LessAttack
};

struct TargetSelector : ITargetSelector
{
	static std::shared_ptr<AIClient> ForceTarget;
	static std::shared_ptr<AIHeroClient> ManualForceTarget;
	static std::map<std::shared_ptr<AIClient>, int> ManualPriorities;
	static std::shared_ptr<AIClient> LaneClearMinion;

	static bool ShouldWait();
	static std::shared_ptr<AIHeroClient> GetBestHeroTarget(float range, bool forOrbwalker = false);
	static void Initialize();
	static void EnemyHeroesCreated();

	static float GetAttackRangeToTarget(const std::shared_ptr<AIClient>& target);
	static bool InAutoAttackRange(const std::shared_ptr<AIClient>& target);

	std::shared_ptr<AIClient> GetManualTarget() override;
	std::shared_ptr<AIClient> GetForcedTarget() override;
	void SetForcedTarget(std::shared_ptr<AIClient> target) override;
	std::shared_ptr<AIClient> GetBestTarget(float range, bool considerManualTarget = true, bool forOrbwalker = false) override;
	int GetPriority(std::shared_ptr<AIHeroClient> target) override;
	float GetPriorityMod(std::shared_ptr<AIHeroClient> target) override;
};