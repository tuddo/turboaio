#pragma once

#include <Windows.h>
#include "../sdkapi.h"

struct Color
{
	static SDKCOLOR Black;
	static SDKCOLOR White;
	static SDKCOLOR Red;
	static SDKCOLOR Green;
	static SDKCOLOR Blue;
	static SDKCOLOR Gray;
	static SDKCOLOR DarkGray;
	static SDKCOLOR Orange;
	static SDKCOLOR Purple;
	static SDKCOLOR Yellow;
	static SDKCOLOR Magenta;
};

__declspec(selectany) SDKCOLOR Color::Black = { 0, 0, 0, 255 };
__declspec(selectany) SDKCOLOR Color::White = { 255, 255, 255, 0 };
__declspec(selectany) SDKCOLOR Color::Red = { 0, 0, 255, 255 };
__declspec(selectany) SDKCOLOR Color::Green = { 0, 255, 0, 255 };
__declspec(selectany) SDKCOLOR Color::Blue = { 255, 0, 0, 255 };
__declspec(selectany) SDKCOLOR Color::Gray = { 169, 169, 169, 255 };
__declspec(selectany) SDKCOLOR Color::DarkGray = { 128, 128, 128, 255 };
__declspec(selectany) SDKCOLOR Color::Orange = { 255, 165, 0, 255 };
__declspec(selectany) SDKCOLOR Color::Purple = { 128, 0, 128, 255 };
__declspec(selectany) SDKCOLOR Color::Yellow = { 255, 255, 0, 255 };
__declspec(selectany) SDKCOLOR Color::Magenta = { 255, 0, 255, 255 };