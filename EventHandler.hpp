#pragma once

#include <vector>
#include <Windows.h>
#include "../sdkapi.h"
#include "IEventHandler.h"

struct EventHandler : IEventHandler
{
	static std::vector<ONKEYPRESSCALLBACK> KeyPressHandlers;
	static std::vector<RENDERSCENECALLBACK> GameSceneHandlers;
	static std::vector<LIBRARYCALLBACK> LibraryCallHandlers;
	static std::vector<ONAIATTACKCALLBACK> AIAttackHandlers;
	static std::vector<ONAICASTATTACKCALLBACK> AICastAttackHandlers;
	static std::vector<ONAIMOVECALLBACK> AIMoveHandlers;
	static std::vector<ONAICASTATTACKCALLBACK> AIProcessSpellHandlers;
	static std::vector<ONLOADCALLBACK> LoadHandlers;
	static std::vector<GAMEOBJECTSCALLBACK> ObjectCreateHandlers;
	static std::vector<GAMEOBJECTSCALLBACK> ObjectDeleteHandlers;
	static std::vector<GAMEOBJECTSCALLBACK> ObjectUpdateHandlers;
	static std::vector<ONUNITRECALLCALLBACK> AIRecallHandlers;
	static std::vector<RENDERSCENECALLBACK> OverlaySceneHandlers;
	static std::vector<ONPLAYERSHOPCALLBACK> LocalPlayerShopHandlers;
	static std::vector<ONAIBUFFCREATEDELETECALLBACK> AIBuffUpdateHandlers;
	static std::vector<ONPLAYERPREATTACK> PreAttackHandlers;
	static std::vector<ONPLAYERPOSTATTACK> PostAttackHandlers;
	static std::vector<ONPLAYERPRECAST> PreCastHandlers;
	static std::vector<ONTICK> TickHandlers;

	static void __cdecl OnKeyPress(bool KeyDown, unsigned int VirtualKey, unsigned short Repeated, unsigned char ScanCode, bool IsExtended, bool IsAltDown, bool PreviousState, bool TransitionState, void* UserData);
	static void __cdecl OnGameScene(void* UserData);
	static bool __cdecl OnLibraryCall(const char* ImportName, unsigned int Version, void* Data, size_t* SizeOfData, void* UserData);
	static void __cdecl OnAIAttack(void* AI, void* TargetObject, bool StartAttack, bool StopAttack, void* UserData);
	static void __cdecl OnAICastAttack(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData);
	static void __cdecl OnAIMove(void* AI, bool Move, bool Stop, void* UserData);
	static void __cdecl OnAIProcessSpell(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData);
	static void __cdecl OnLoad(void* UserData);
	static bool __cdecl OnObjectCreate(void* Object, unsigned int NetworkID, void* UserData);
	static bool __cdecl OnObjectDelete(void* Object, unsigned int NetworkID, void* UserData);
	static bool __cdecl OnObjectUpdate(void* Object, unsigned int NetworkID, void* UserData);
	static void __cdecl OnAIRecall(void* Unit, const char* Name, const char* Type, void* UserData);
	static void __cdecl OnOverlayScene(void* UserData);
	static void __cdecl OnLocalPlayerShop(int Event, int Slot, int Extra, void* UserData);
	static void __cdecl OnAIBuffUpdate(void* AI, bool Created, unsigned char Type, float StartTime, float EndTime, const char* Name, void* CasterObject, unsigned int CasterID, int Stacks, bool HasCount, int Count, PSDK_SPELL Spell, void* UserData);

	static void __cdecl OnTickHandler(void *UserData);

	static void Initialize();

	void RegisterEvent(eEventType eventType, void* function) override;
};