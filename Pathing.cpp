#include "Pathing.hpp"
#include "ObjectManager.hpp"

std::vector<Vector3> Pathing::GetPath(AIClient* unit)
{
	std::vector<Vector3> tempVec;
	const sNavData navData = unit->GetNavData();

	if (navData.NumberOfWaypoints)
	{
		for (size_t i = 0; i < navData.NumberOfWaypoints; i++)
		{
			Vector3 waypoint = navData.Waypoints[i];

			tempVec.push_back(waypoint);
		}
	}

	return tempVec;
}

std::vector<Vector3> Pathing::CutPath(std::vector<Vector3> path, float distance)
{
	std::vector<Vector3> tempVec;

	if (distance < 0)
	{
		path[0] = path[0] + (path[1] - path[0]).Normalized() * distance;
		return path;
	}

	const size_t pathSize = path.size();
	float Distance = distance;
	for (size_t i = 0; i < pathSize - 1; i++)
	{
		const float dist = path[i].GetDistance(path[i + 1]);
		if (dist > Distance)
		{
			tempVec.push_back(path[i] + (path[i + 1] - path[i]).Normalized() * Distance);
			for (size_t j = i + 1; j < pathSize; j++)
			{
				tempVec.push_back(path[j]);
			}

			break;
		}
		Distance -= dist;
	}

	return !tempVec.empty() ? tempVec : std::vector<Vector3>{ path.back() };
}

float Pathing::GetPathLength(std::vector<Vector3> path)
{
	const size_t pathSize = path.size();
	
	float length = .0f;

	if (pathSize < 1)
		return 0.f;

	for (size_t i = 0; i < pathSize - 1; i++)
		length += path[i].GetDistance(path[i + 1]);

	return length;
}

Vector3 Pathing::CalculateVelocity(Vector3 start, Vector3 end, float speed)
{
	Vector3 vector = end - start;
	const float distance = sqrt(vector.Dot(vector));

	return vector / distance * speed;
}