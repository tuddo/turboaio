#pragma once

#include "IOrbwalker.h"
#include "GameObjects.h"

struct Orbwalker : IOrbwalker
{
	static int SpecialType;

	static int MissileLaunched;
	static int LastAutoAttackTick;
	static int LastAutoAttackCommandTick;
	static int LastMovementOrderTick;
	static std::shared_ptr<AIClient> LastTarget;
	static int BlockOrdersUntilTick;

	static bool OrbwalkerStatus;
	static bool FullOrbwalkerStatus;
	static std::vector<std::string> AttackResets;
	static std::vector<std::string> NoCancelChamps;

	static void Attack(std::shared_ptr<AIClient>& target);
	static void Move(Vector3 position);
	static int GetTotalWind();

	static void Orbwalk();
	static void __cdecl OnUpdate();
	static void __cdecl OnAIAttack(void* AI, void* TargetObject, bool StartAttack, bool StopAttack, void* UserData);
	static void __cdecl OnAIProcessSpell(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData);
	static void __cdecl OnAICastAttack(void* AI, PSDK_SPELL_CAST SpellCast, void* UserData);
	static void __cdecl OnObjectDelete(void* Object, unsigned int NetworkID, void* UserData);

	static void Initialize();

	// Override from Orbwalker interface
	OrbwalkerMode GetOrbwalkerMode() override;
	bool CanMove() override;
	bool CanAttack() override;
	bool IsAutoAttackReset(std::string spellName) override;
	void MoveTo(Vector3 position, bool pet) override;
	void MoveToMouse(bool pet) override;
	bool GetOrbwalkerAttackStatus() override;
	void ToggleOrbwalkerAttack() override;
	bool GetOrbwalkerStatus() override;
	void ToggleOrbwalker() override;
};
