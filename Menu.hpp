#pragma once

#include <vector>
#include <functional>
#include "IMenu.h"

struct Menu : IMenu
{
	static bool DrawWindow;
	static int BaseWindupTime;
	static std::vector<std::function<void()>> CustomMenus;

	static void UseStyle();
	static void Initialize();

	void CreateMenu(std::string menuLabel, std::function<void()> menuFunction) override;
	void Tree(std::string treeLabel, std::string cfgExpanded, std::function<void()> treeFunction) override;
	void Text(std::string label, ...) override;
	void Checkbox(std::string label, std::string cfgChecked, bool defaultValue) override;
	void Hotkey(std::string label, std::string cfgKey, eKeyCode defaultKey) override;
	void Toggle(std::string label, std::string cfgKey, eKeyCode defaultKey, bool defaultStatus) override;
	bool GetMenuKeyStatus(std::string cfgKey) override;
	void SliderInt(std::string label, std::string cfgKey, int defaultValue, int min, int max) override;
	void SliderFloat(std::string label, std::string cfgKey, float defaultValue, float min, float max,
		float power) override;
	int GetInt(std::string cfgKey) override;
	float GetFloat(std::string cfgKey) override;
	bool GetBool(std::string cfgKey) override;
	void List(std::string label, std::string cfgKey, const char* items[], int itemsCount, int defaultIndex = 0) override;
	void SameLine() override;
};