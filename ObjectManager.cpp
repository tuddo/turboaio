#include <vector>
#include "ObjectManager.hpp"
#include "Vars.h"
#include "EventHandler.hpp"
#include "TargetSelector.hpp"

std::vector<SDK_ITEM> ObjectManager::LocalItems{};
std::vector<sBuffInfo> ObjectManager::LocalBuffsCache;

std::map<unsigned int, std::shared_ptr<AIHeroClient>> ObjectManager::AllyHeroes;
std::map<unsigned int, std::shared_ptr<AIClient>> ObjectManager::AllyMinions;
std::map<unsigned int, std::shared_ptr<AIClient>> ObjectManager::AllyInhibitors;
std::map<unsigned int, std::shared_ptr<AITurretClient>> ObjectManager::AllyTurrets;
std::map<unsigned int, std::shared_ptr<GameObject>> ObjectManager::Ally;

std::map<unsigned int, std::shared_ptr<AIHeroClient>> ObjectManager::EnemyHeroes;
std::map<unsigned int, std::shared_ptr<AIClient>> ObjectManager::EnemyMinions;
std::map<unsigned int, std::shared_ptr<AIClient>> ObjectManager::EnemyInhibitors;
std::map<unsigned int, std::shared_ptr<AITurretClient>> ObjectManager::EnemyTurrets;
std::map<unsigned int, std::shared_ptr<GameObject>> ObjectManager::Enemy;

std::map<unsigned int, std::shared_ptr<AIHeroClient>> ObjectManager::Heroes;
std::map<unsigned int, std::shared_ptr<AIClient>> ObjectManager::Inhibitors;
std::map<unsigned int, std::shared_ptr<AIClient>> ObjectManager::Nexus;
std::map<unsigned int, std::shared_ptr<AIClient>> ObjectManager::Jungle;
std::map<unsigned int, std::shared_ptr<AIClient>> ObjectManager::Minions;
std::map<unsigned int, std::shared_ptr<AITurretClient>> ObjectManager::Turrets;

bool __cdecl ObjectManager_ObjectCreate(void* Object, unsigned int NetworkID, void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	if (!Object)
		return false;
	if (!NetworkID)
		return false;

	int objectType;
	gSdkContext->_SdkGetObjectTypeID(Object, &objectType);
	if (objectType == ObjectType::typeAIHeroClient)
	{
		shared_ptr<AIHeroClient> hero = make_shared<AIHeroClient>();
		hero->Object = Object;
		const int heroTeam = hero->GetTeamID();
		const int localTeam = pSDK->Player->TeamID;

		if (heroTeam == localTeam)
		{
			auto pair = make_pair(NetworkID, std::move(hero));

			ObjectManager::AllyHeroes.insert(pair);
			ObjectManager::Heroes.insert(pair);
			ObjectManager::Ally.insert(pair);
		}
		else if (heroTeam != localTeam && heroTeam != TEAM_TYPE_NEUTRAL)
		{
			auto pair = make_pair(NetworkID, std::move(hero));

			ObjectManager::EnemyHeroes.insert(pair);
			ObjectManager::Heroes.insert(pair);
			ObjectManager::Enemy.insert(pair);
			if (ObjectManager::EnemyHeroes.size() == 5)
				TargetSelector::EnemyHeroesCreated();
		}
	}
	else if (objectType == ObjectType::typeAIMinion)
	{
		shared_ptr<AIClient> minion = make_shared<AIClient>();
		minion->Object = Object;
		const int minionTeam = minion->GetTeamID();
		const int localTeam = pSDK->Player->TeamID;

		if (minionTeam == localTeam)
		{
			auto pair = make_pair(NetworkID, std::move(minion));

			ObjectManager::AllyMinions.insert(pair);
			ObjectManager::Minions.insert(pair);
			ObjectManager::Ally.insert(pair);
		}
		else if (minionTeam != localTeam && minionTeam != TEAM_TYPE_NEUTRAL)
		{
			auto pair = make_pair(NetworkID, std::move(minion));

			ObjectManager::EnemyMinions.insert(pair);
			ObjectManager::Minions.insert(pair);
			ObjectManager::Enemy.insert(pair);
		}
		else if (minionTeam == TEAM_TYPE_NEUTRAL)
		{
			string minionAIName = minion->GetAIName();
			if (minionAIName.find("Sru_") != string::npos || minionAIName.find("SRU_") != string::npos)
				ObjectManager::Jungle.insert({ NetworkID, std::move(minion) });
			else
				ObjectManager::Minions.insert({ NetworkID, std::move(minion) });
		}
	}
	else if (objectType == ObjectType::typeAITurret)
	{
		shared_ptr<AITurretClient> turret = make_shared<AITurretClient>();
		turret->Object = Object;
		const int turretTeam = turret->GetTeamID();
		const int localTeam = pSDK->Player->TeamID;

		if (turretTeam == localTeam)
		{
			auto pair = make_pair(NetworkID, std::move(turret));

			ObjectManager::AllyTurrets.insert(pair);
			ObjectManager::Turrets.insert(pair);
			ObjectManager::Ally.insert(pair);
		}
		else if (turretTeam != localTeam && turretTeam != TEAM_TYPE_NEUTRAL)
		{
			auto pair = make_pair(NetworkID, std::move(turret));

			ObjectManager::EnemyTurrets.insert(pair);
			ObjectManager::Turrets.insert(pair);
			ObjectManager::Enemy.insert(pair);
		}
	}
	else if (objectType == ObjectType::typeObjBarracksDampener)
	{
		shared_ptr<AIClient> inhibitor = make_shared<AIClient>();
		inhibitor->Object = Object;
		const int inhibitorTeam = inhibitor->GetTeamID();
		const int localTeam = pSDK->Player->TeamID;

		if (inhibitorTeam == localTeam)
		{
			auto pair = make_pair(NetworkID, std::move(inhibitor));

			ObjectManager::AllyInhibitors.insert(pair);
			ObjectManager::Inhibitors.insert(pair);
			ObjectManager::Ally.insert(pair);
		}
		else if (inhibitorTeam != localTeam && inhibitorTeam != TEAM_TYPE_NEUTRAL)
		{
			auto pair = make_pair(NetworkID, std::move(inhibitor));

			ObjectManager::EnemyInhibitors.insert(pair);
			ObjectManager::Inhibitors.insert(pair);
			ObjectManager::Enemy.insert(pair);
		}
	}
	else if (objectType == ObjectType::typeObjHQ)
	{
		shared_ptr<AIClient> nexus = make_shared<AIClient>();
		nexus->Object = Object;
		const int nexusTeam = nexus->GetTeamID();
		const int localTeam = pSDK->Player->TeamID;

		auto pair = make_pair(NetworkID, std::move(nexus));

		ObjectManager::Nexus.insert(pair);
		if (nexusTeam == localTeam)
			ObjectManager::Ally.insert(pair);
		else if (nexusTeam != localTeam && nexusTeam != TEAM_TYPE_NEUTRAL)
			ObjectManager::Enemy.insert(pair);
	}

	return true;
}

bool __cdecl ObjectManager_ObjectDelete(void* Object, unsigned int NetworkID, void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);
	
	if (!Object)
		return true;
	if (!NetworkID)
		return true;

	delete ObjectManager::Ally[NetworkID].get();
	ObjectManager::Ally.erase(NetworkID);
	delete ObjectManager::Enemy[NetworkID].get();
	ObjectManager::Enemy.erase(NetworkID);

	int objectType;
	gSdkContext->_SdkGetObjectTypeID(Object, &objectType);
	if (objectType == ObjectType::typeAIHeroClient)
	{
		delete ObjectManager::AllyHeroes[NetworkID].get();
		ObjectManager::AllyHeroes.erase(NetworkID);
		delete ObjectManager::EnemyHeroes[NetworkID].get();
		ObjectManager::EnemyHeroes.erase(NetworkID);
		delete ObjectManager::Heroes[NetworkID].get();
		ObjectManager::Heroes.erase(NetworkID);
	}
	else if (objectType == ObjectType::typeAIMinion)
	{
		delete ObjectManager::AllyMinions[NetworkID].get();
		ObjectManager::AllyMinions.erase(NetworkID);
		delete ObjectManager::EnemyMinions[NetworkID].get();
		ObjectManager::EnemyMinions.erase(NetworkID);
		delete ObjectManager::Minions[NetworkID].get();
		ObjectManager::Minions.erase(NetworkID);
		delete ObjectManager::Jungle[NetworkID].get();
		ObjectManager::Jungle.erase(NetworkID);
	}
	else if (objectType == ObjectType::typeAITurret)
	{
		delete ObjectManager::AllyTurrets[NetworkID].get();
		ObjectManager::AllyTurrets.erase(NetworkID);
		delete ObjectManager::EnemyTurrets[NetworkID].get();
		ObjectManager::EnemyTurrets.erase(NetworkID);
		delete ObjectManager::Turrets[NetworkID].get();
		ObjectManager::Turrets.erase(NetworkID);
	}
	else if (objectType == ObjectType::typeObjBarracksDampener)
	{
		delete ObjectManager::AllyInhibitors[NetworkID].get();
		ObjectManager::AllyInhibitors.erase(NetworkID);
		delete ObjectManager::EnemyInhibitors[NetworkID].get();
		ObjectManager::EnemyInhibitors.erase(NetworkID);
		delete ObjectManager::Inhibitors[NetworkID].get();
	}
	else if (objectType == ObjectType::typeObjHQ)
	{
		delete ObjectManager::Nexus[NetworkID].get();
		ObjectManager::Nexus.erase(NetworkID);
	}

	return true;
}

void ObjectManager_UpdateLocalPlayer()
{
	void* LocalPlayer;
	if (SDKSTATUS_SUCCESS(gSdkContext->_SdkGetLocalPlayer(&LocalPlayer)))
	{
		if (pSDK->Player->Object != LocalPlayer)
		{
			pSDK->Player->Object = LocalPlayer;
			pSDK->Player->TeamID = pSDK->Player->GetTeamID();

			gSdkContext->_SdkConsoleWrite("[BlockchainBase] Local Player address fixed!");
		}
	}
}

vector<shared_ptr<AIClient>> ObjectManager::GetMinions(bool ally, bool enemy, float range)
{
	vector<shared_ptr<AIClient>> tempVec;

	if (ally && !enemy)
	{
		for (const auto& minion : AllyMinions)
		{
			if (pSDK->Player->GetDistanceSquared(minion.second->GetPosition()) > range * range)
				continue;

			tempVec.push_back(minion.second);
		}
	}
	else if (!ally && enemy)
	{
		for (const auto& minion : EnemyMinions)
		{
			if (pSDK->Player->GetDistanceSquared(minion.second->GetPosition()) > range * range)
				continue;

			tempVec.push_back(minion.second);
		}
	}
	else if (ally && enemy)
	{
		for (const auto& minion : Minions)
		{
			if (pSDK->Player->GetDistanceSquared(minion.second->GetPosition()) > range * range)
				continue;

			tempVec.push_back(minion.second);
		}
	}

	return tempVec;
}

vector<shared_ptr<AIClient>> ObjectManager::GetLaneMinions(bool ally, bool enemy, float range)
{
	vector<shared_ptr<AIClient>> tempVec;

	if (ally && !enemy)
	{
		for (const auto& minion : AllyMinions)
		{
			if (pSDK->Player->GetDistanceSquared(minion.second->GetPosition()) > range * range)
				continue;

			bool isLaneMinion;
			gSdkContext->_SdkIsMinionLaneMinion(minion.second->Object, &isLaneMinion);

			if (isLaneMinion)
				tempVec.push_back(minion.second);
		}
	}
	else if (!ally && enemy)
	{
		for (const auto& minion : EnemyMinions)
		{
			if (pSDK->Player->GetDistanceSquared(minion.second->GetPosition()) > range * range)
				continue;

			bool isLaneMinion;
			gSdkContext->_SdkIsMinionLaneMinion(minion.second->Object, &isLaneMinion);

			if (isLaneMinion)
				tempVec.push_back(minion.second);
		}
	}
	else if (ally && enemy)
	{
		for (const auto& minion : Minions)
		{
			if (pSDK->Player->GetDistanceSquared(minion.second->GetPosition()) > range * range)
				continue;

			bool isLaneMinion;
			gSdkContext->_SdkIsMinionLaneMinion(minion.second->Object, &isLaneMinion);

			if (isLaneMinion)
				tempVec.push_back(minion.second);
		}
	}

	return tempVec;
}

vector<shared_ptr<AIClient>> ObjectManager::GetJungle(float range)
{
	vector<shared_ptr<AIClient>> tempVec;

	for (const auto& jungle : Jungle)
	{
		if (pSDK->Player->GetDistanceSquared(jungle.second->GetPosition()) > range * range)
			continue;

		tempVec.push_back(jungle.second);
	}

	return tempVec;
}

vector<shared_ptr<AIHeroClient>> ObjectManager::GetHeroes(bool ally, bool enemy, float range)
{
	vector<shared_ptr<AIHeroClient>> tempVec;

	if (ally && !enemy)
	{
		for (const auto& hero : AllyHeroes)
		{
			if (pSDK->Player->GetDistanceSquared(hero.second->GetPosition()) > range * range)
				continue;

			tempVec.push_back(hero.second);
		}
	}
	else if (!ally && enemy)
	{
		for (const auto& hero : EnemyHeroes)
		{
			if (pSDK->Player->GetDistanceSquared(hero.second->GetPosition()) > range * range)
				continue;

			tempVec.push_back(hero.second);
		}
	}
	else if (ally && enemy)
	{
		for (const auto& hero : Heroes)
		{
			if (pSDK->Player->GetDistanceSquared(hero.second->GetPosition()) > range * range)
				continue;

			tempVec.push_back(hero.second);
		}
	}

	return tempVec;
}

vector<shared_ptr<AITurretClient>> ObjectManager::GetTurrets(bool ally, bool enemy, float range)
{
	vector<shared_ptr<AITurretClient>> tempVec;

	if (ally && !enemy)
	{
		for (const auto& turret : AllyTurrets)
		{
			if (pSDK->Player->GetDistanceSquared(turret.second->GetPosition()) > range * range)
				continue;

			tempVec.push_back(turret.second);
		}
	}
	else if (!ally && enemy)
	{
		for (const auto& turret : EnemyTurrets)
		{
			if (pSDK->Player->GetDistanceSquared(turret.second->GetPosition()) > range * range)
				continue;

			tempVec.push_back(turret.second);
		}
	}
	else if (ally && enemy)
	{
		for (const auto& turret : Turrets)
		{
			if (pSDK->Player->GetDistanceSquared(turret.second->GetPosition()) > range * range)
				continue;

			tempVec.push_back(turret.second);
		}
	}

	return tempVec;
}

std::vector<std::shared_ptr<AIClient>> ObjectManager::GetInhibitors(bool ally, bool enemy, float range)
{
	vector<shared_ptr<AIClient>> tempVec;

	if (ally && !enemy)
	{
		for (const auto& inhibitor : AllyInhibitors)
		{
			if (pSDK->Player->GetDistanceSquared(inhibitor.second->GetPosition()) > range * range)
				continue;

			tempVec.push_back(inhibitor.second);
		}
	}
	else if (!ally && enemy)
	{
		for (const auto& inhibitor : EnemyInhibitors)
		{
			if (pSDK->Player->GetDistanceSquared(inhibitor.second->GetPosition()) > range * range)
				continue;

			tempVec.push_back(inhibitor.second);
		}
	}
	else if (ally && enemy)
	{
		for (const auto& inhibitor : Inhibitors)
		{
			if (pSDK->Player->GetDistanceSquared(inhibitor.second->GetPosition()) > range * range)
				continue;

			tempVec.push_back(inhibitor.second);
		}
	}

	return tempVec;
}

std::vector<std::shared_ptr<AIClient>> ObjectManager::GetNexus(bool ally, bool enemy, float range)
{
	vector<shared_ptr<AIClient>> tempVec;

	if (ally && !enemy)
	{
		for (const auto& nexus : Nexus)
		{
			if (nexus.second->GetTeamID() == pSDK->Player->TeamID)
			{
				if (pSDK->Player->GetDistanceSquared(nexus.second->GetPosition()) > range * range)
					continue;

				tempVec.push_back(nexus.second);
			}
		}
	}
	else if (!ally && enemy)
	{
		for (const auto& nexus : Nexus)
		{
			const int teamID = nexus.second->GetTeamID();
			if (teamID != pSDK->Player->TeamID && teamID != TEAM_TYPE_NEUTRAL)
			{
				if (pSDK->Player->GetDistanceSquared(nexus.second->GetPosition()) > range * range)
					continue;

				tempVec.push_back(nexus.second);
			}
		}
	}
	else if (ally && enemy)
	{
		for (const auto& nexus : Nexus)
		{
			if (nexus.second->GetTeamID() != TEAM_TYPE_NEUTRAL)
			{
				if (pSDK->Player->GetDistanceSquared(nexus.second->GetPosition()) > range * range)
					continue;

				tempVec.push_back(nexus.second);
			}
		}
	}

	return tempVec;
}

void ObjectManager::Initialize()
{
	void* localPlayer;
	int teamID;
	gSdkContext->_SdkGetLocalPlayer(&localPlayer);
	gSdkContext->_SdkGetObjectTeamID(localPlayer, &teamID);
	pSDK->Player->Object = localPlayer;
	pSDK->Player->TeamID = teamID;

	gSdkContext->_SdkEnumGameObjects([](void* Object, unsigned int NetworkID, void* UserData) -> bool
	{
		UNREFERENCED_PARAMETER(UserData);

		if (!Object)
			return false;
		if (!NetworkID)
			return false;

		int objectType;
		gSdkContext->_SdkGetObjectTypeID(Object, &objectType);
		if (objectType == ObjectType::typeAIHeroClient)
		{
			shared_ptr<AIHeroClient> hero = make_shared<AIHeroClient>();
			hero->Object = Object;
			const int heroTeam = hero->GetTeamID();
			const int localTeam = pSDK->Player->TeamID;

			if (heroTeam == localTeam)
			{
				auto pair = make_pair(NetworkID, std::move(hero));

				ObjectManager::AllyHeroes.insert(pair);
				ObjectManager::Heroes.insert(pair);
				ObjectManager::Ally.insert(pair);
			}
			else if (heroTeam != localTeam && heroTeam != TEAM_TYPE_NEUTRAL)
			{
				auto pair = make_pair(NetworkID, std::move(hero));

				ObjectManager::EnemyHeroes.insert(pair);
				ObjectManager::Heroes.insert(pair);
				ObjectManager::Enemy.insert(pair);
				if (ObjectManager::EnemyHeroes.size() == 5)
					TargetSelector::EnemyHeroesCreated();
			}
		}
		else if (objectType == ObjectType::typeAIMinion)
		{
			shared_ptr<AIClient> minion = make_shared<AIClient>();
			minion->Object = Object;
			const int minionTeam = minion->GetTeamID();
			const int localTeam = pSDK->Player->TeamID;

			if (minionTeam == localTeam)
			{
				auto pair = make_pair(NetworkID, std::move(minion));

				ObjectManager::AllyMinions.insert(pair);
				ObjectManager::Minions.insert(pair);
				ObjectManager::Ally.insert(pair);
			}
			else if (minionTeam != localTeam && minionTeam != TEAM_TYPE_NEUTRAL)
			{
				auto pair = make_pair(NetworkID, std::move(minion));

				ObjectManager::EnemyMinions.insert(pair);
				ObjectManager::Minions.insert(pair);
				ObjectManager::Enemy.insert(pair);
			}
			else if (minionTeam == TEAM_TYPE_NEUTRAL)
			{
				string minionAIName = minion->GetAIName();
				if (minionAIName.find("Sru_") != string::npos || minionAIName.find("SRU_") != string::npos)
					ObjectManager::Jungle.insert({ NetworkID, std::move(minion) });
				else
					ObjectManager::Minions.insert({ NetworkID, std::move(minion) });
			}
		}
		else if (objectType == ObjectType::typeAITurret)
		{
			shared_ptr<AITurretClient> turret = make_shared<AITurretClient>();
			turret->Object = Object;
			const int turretTeam = turret->GetTeamID();
			const int localTeam = pSDK->Player->TeamID;

			if (turretTeam == localTeam)
			{
				auto pair = make_pair(NetworkID, std::move(turret));

				ObjectManager::AllyTurrets.insert(pair);
				ObjectManager::Turrets.insert(pair);
				ObjectManager::Ally.insert(pair);
			}
			else if (turretTeam != localTeam && turretTeam != TEAM_TYPE_NEUTRAL)
			{
				auto pair = make_pair(NetworkID, std::move(turret));

				ObjectManager::EnemyTurrets.insert(pair);
				ObjectManager::Turrets.insert(pair);
				ObjectManager::Enemy.insert(pair);
			}
		}
		else if (objectType == ObjectType::typeObjBarracksDampener)
		{
			shared_ptr<AIClient> inhibitor = make_shared<AIClient>();
			inhibitor->Object = Object;
			const int inhibitorTeam = inhibitor->GetTeamID();
			const int localTeam = pSDK->Player->TeamID;

			if (inhibitorTeam == localTeam)
			{
				auto pair = make_pair(NetworkID, std::move(inhibitor));

				ObjectManager::AllyInhibitors.insert(pair);
				ObjectManager::Inhibitors.insert(pair);
				ObjectManager::Ally.insert(pair);
			}
			else if (inhibitorTeam != localTeam && inhibitorTeam != TEAM_TYPE_NEUTRAL)
			{
				auto pair = make_pair(NetworkID, std::move(inhibitor));

				ObjectManager::EnemyInhibitors.insert(pair);
				ObjectManager::Inhibitors.insert(pair);
				ObjectManager::Enemy.insert(pair);
			}
		}
		else if (objectType == ObjectType::typeObjHQ)
		{
			shared_ptr<AIClient> nexus = make_shared<AIClient>();
			nexus->Object = Object;
			const int nexusTeam = nexus->GetTeamID();
			const int localTeam = pSDK->Player->TeamID;

			auto pair = make_pair(NetworkID, std::move(nexus));

			ObjectManager::Nexus.insert(pair);
			if (nexusTeam == localTeam)
				ObjectManager::Ally.insert(pair);
			else if (nexusTeam != localTeam && nexusTeam != TEAM_TYPE_NEUTRAL)
				ObjectManager::Enemy.insert(pair);
		}

		return true;
	}, nullptr);

	pSDK->EventHandler->RegisterEvent(eEventType::ObjectCreate, ObjectManager_ObjectCreate);
	pSDK->EventHandler->RegisterEvent(eEventType::ObjectDelete, ObjectManager_ObjectDelete);
	pSDK->EventHandler->RegisterEvent(eEventType::Tick, ObjectManager_UpdateLocalPlayer);
}
