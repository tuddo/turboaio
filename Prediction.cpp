#include "Prediction.hpp"
#include "Pathing.hpp"
#include "Vars.h"

PREDICTION_OUTPUT Prediction::GetPrediction(AIHeroClient* unit, float delay)
{
	PREDICTION_INPUT input;
	input.Unit = unit;
	input.Delay = delay;
	input.From = pSDK->Player->GetServerPosition();
	input.RangeCheckFrom = pSDK->Player->GetServerPosition();
	return GetPrediction(input);
}

PREDICTION_OUTPUT Prediction::GetPrediction(AIHeroClient* unit, float delay, float radius)
{
	PREDICTION_INPUT input;
	input.Unit = unit;
	input.Delay = delay;
	input.Radius = radius;
	input.From = pSDK->Player->GetServerPosition();
	input.RangeCheckFrom = pSDK->Player->GetServerPosition();
	return GetPrediction(input);
}

PREDICTION_OUTPUT Prediction::GetPrediction(AIHeroClient* unit, float delay, float radius, float speed)
{
	PREDICTION_INPUT input;
	input.Unit = unit;
	input.Delay = delay;
	input.Radius = radius;
	input.Speed = speed;
	input.From = pSDK->Player->GetServerPosition();
	input.RangeCheckFrom = pSDK->Player->GetServerPosition();
	return GetPrediction(input);
}

PREDICTION_OUTPUT Prediction::GetPrediction(AIHeroClient* unit, float delay, float radius, float speed,
	vector<SkillshotCollision> collisionableObjects)
{
	PREDICTION_INPUT input;
	input.Unit = unit;
	input.Delay = delay;
	input.Radius = radius;
	input.Speed = speed;
	input.CollisionObjects = collisionableObjects;
	input.From = pSDK->Player->GetServerPosition();
	input.RangeCheckFrom = pSDK->Player->GetServerPosition();
	return GetPrediction(input);
}

PREDICTION_OUTPUT Prediction::GetPrediction(PREDICTION_INPUT input)
{
	return pSDK->Prediction->GetPrediction(input, true, true);
}

PREDICTION_OUTPUT Prediction::GetPrediction(PREDICTION_INPUT input, bool ft, bool checkCollision)
{
	UNREFERENCED_PARAMETER(checkCollision);

	input.From = pSDK->Player->GetServerPosition();
	input.RangeCheckFrom = pSDK->Player->GetServerPosition();

	PREDICTION_OUTPUT result{};
	result.Hitchance = SkillshotHitchance::Uninitialized;

	if (!input.Unit->IsValid(FLT_MAX))
		return {};

	if (ft)
	{
		// Adapt the delay to user ping.
		input.Delay += pSDK->Game->GetPing() / 2000.f + .06f;

		/*
		if (input.AOE)
		{
			return AOEPrediction::GetPrediction(input);
		}
		*/
	}

	// Target too far away check.
	float rangeCheck = input.Range * 1.5f;
	if (abs(input.Range - FLT_MAX) > FLT_EPSILON && input.Unit->GetDistanceSquared(input.RangeCheckFrom.ToSDK()) > rangeCheck * rangeCheck)
	{
		PREDICTION_OUTPUT output = PREDICTION_OUTPUT();
		output.Input = input;
		output.Hitchance = SkillshotHitchance::OutOfRange;
		return output;
	}

	if (result.Hitchance == SkillshotHitchance::Uninitialized)
	{
		result = GetPositionOnPath(input, Pathing::GetPath(input.Unit));
	}

	if (abs(input.Range - FLT_MAX) > FLT_EPSILON)
	{
		if (result.Hitchance >= SkillshotHitchance::High && input.RangeCheckFrom.GetDistance(input.Unit->GetPosition()) > input.Range + input.RealRadius() * 3 / 4)
			result.Hitchance = SkillshotHitchance::Medium;

		if (input.RangeCheckFrom.GetDistance(result.UnitPosition) > input.Range + (input.Type == SkillshotType::Circle ? input.RealRadius() : 0))
			result.Hitchance = SkillshotHitchance::OutOfRange;

		if (input.RangeCheckFrom.GetDistance(result.CastPosition) > input.Range)
		{
			if (result.Hitchance != SkillshotHitchance::OutOfRange && result.Hitchance != SkillshotHitchance::Uninitialized)
			{
				const Vector2 normalVec = (result.UnitPosition - input.RangeCheckFrom).To2D().Normalized();
				result.CastPosition = input.RangeCheckFrom + Vector3(normalVec) * input.Range;
			}
			else
			{
				result.Hitchance = SkillshotHitchance::OutOfRange;
			}
		}
	}

	return result;
}

PREDICTION_OUTPUT Prediction::GetStandardPrediction(PREDICTION_INPUT input)
{
	//float speed = input.Unit->GetMovementSpeed();

	return {};
}

PREDICTION_OUTPUT Prediction::GetPositionOnPath(PREDICTION_INPUT input, std::vector<Vector3> path, float speed)
{
	speed = (abs(speed - (-1)) < FLT_EPSILON) ? input.Unit->GetMovementSpeed() : speed;

	const size_t pathSize = path.size();
	if (pathSize <= 1)
	{
		PREDICTION_OUTPUT output{};
		output.Input = input;
		output.UnitPosition = input.Unit->GetServerPosition();
		output.CastPosition = input.Unit->GetServerPosition();
		output.Hitchance = SkillshotHitchance::VeryHigh;
		return output;
	}

	const float pathLength = Pathing::GetPathLength(path);
	if (pathLength >= input.Delay * speed - input.RealRadius() && abs(input.Speed - FLT_MAX) < FLT_EPSILON)
	{
		float tDistance = input.Delay * speed - input.RealRadius();

		for (size_t i = 0; i < pathSize - 1; i++)
		{
			auto a = path[i];
			auto b = path[i + 1];
			auto d = a.GetDistance(b);

			if (d >= tDistance)
			{
				auto direction = (b - a).Normalized();

				auto cp = a + (direction * tDistance);
				auto p = a + (direction * (i == pathSize - 2 ? min(tDistance + input.RealRadius(), d) : tDistance + input.RealRadius()));

				PREDICTION_OUTPUT output{};
				output.Input = input;
				output.CastPosition = cp;
				output.UnitPosition = p;
				return output;
			}

			tDistance -= d;
		}
	}

	if (pathLength >= input.Delay * speed - input.RealRadius() && abs(input.Speed - FLT_MAX) > FLT_EPSILON)
	{
		auto d = input.Delay * speed - input.RealRadius();

		if (input.Type == SkillshotType::Line || input.Type == SkillshotType::Cone)
		{
			if (input.From.GetDistance(input.Unit->GetServerPosition()) < 200)
			{
				d = input.Delay * speed;
			}
		}

		path = Pathing::CutPath(path, d);

		auto tT = .0f;

		for (size_t i = 0; i < pathSize - 1; i++)
		{
			auto a = path[i];
			auto b = path[i + 1];
			auto tB = a.GetDistance(b) / speed;
			auto direction = (b - a).Normalized();
			a = a - direction * (speed * tT);
			auto sol = Geometry::VectorMovementCollision(a.To2D(), b.To2D(), speed, input.From.To2D(), input.Speed, tT);
			auto t = static_cast<float>(std::get<0>(sol));
			auto pos = static_cast<Vector2>(std::get<1>(sol));

			if (pos.X != 0 && pos.Y != 0 && t >= tT && t <= tT + tB)
			{
				if (pos.GetDistance(b.To2D()) < 4.47213595499958f)
					break;

				auto p = pos + direction.To2D() * input.RealRadius();

				PREDICTION_OUTPUT output{};
				output.Input = input;
				output.CastPosition = { pos.X, pos.Y, input.Unit->GetPosition().Z };
				output.CastPosition = { p.X, p.Y, input.Unit->GetPosition().Z };
				output.Hitchance = SkillshotHitchance::High;
				return output;
			}

			tT += tB;
		}
	}

	const auto position = path.back();
	PREDICTION_OUTPUT output{};
	output.Input = input;
	output.CastPosition = position;
	output.UnitPosition = position;
	output.Hitchance = SkillshotHitchance::Medium;
	return output;
}
