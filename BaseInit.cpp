#include <Windows.h>
#include "../sdkapi.h"
#include "Vars.h"
#include "ObjectManager.hpp"
#include "EventHandler.hpp"
#include "ItemDB.hpp"
#include "Damage.hpp"
#include "Collision.hpp"

// Base Components
#include "Menu.hpp"
#include "Orbwalker.hpp"
#include "EntityManager.h"
#include "TargetSelector.hpp"
#include "Prediction.hpp"
#include "HealthPrediction.hpp"
#include "Game.h"
#include "Collision.hpp"
#include "Color.h"

PSDK_CONTEXT gSdkContext;
sSDK* pSDK = new sSDK();

bool __cdecl SDK_OnLibraryRequest(const char* ImportName, unsigned int Version, void* Data, size_t* SizeOfData, void* UserData)
{
	UNREFERENCED_PARAMETER(Version);
	UNREFERENCED_PARAMETER(SizeOfData);
	UNREFERENCED_PARAMETER(UserData);

	if (strcmp(ImportName, "GetSDK") == 0)
		*static_cast<sSDK**>(Data) = pSDK;

	return true;
}

SDKVECTOR Origin{ 1, 1, 1 };
void BaseInit::OnDraw(void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	if (TargetSelector::ManualForceTarget != nullptr)
	{
		if (pSDK->Menu->GetBool("BBase.Orbwalker.Draw.Forced"))
		{
			SDKVECTOR manualTargetPos{ TargetSelector::ManualForceTarget->GetPosition().ToSDK() };
			gSdkContext->_SdkDrawCircle(&manualTargetPos, 120, &Color::White, 0, &Origin);
		}
	}

	if (pSDK->Menu->GetBool("BBase.Orbwalker.Draw.LocalRange"))
	{
		SDKVECTOR sdkPlayerPos{ pSDK->Player->GetPosition().ToSDK() };

		const float attackRange = pSDK->Player->GetAttackRange();
		const float boundingRadius = pSDK->Player->GetBoundingRadius();

		gSdkContext->_SdkDrawCircle(&sdkPlayerPos, attackRange + boundingRadius, &Color::Red, 0, &Origin);
		gSdkContext->_SdkDrawCircle(&sdkPlayerPos, boundingRadius, &Color::White, 0, &Origin);
	}

	if (pSDK->Menu->GetBool("BBase.Orbwalker.Draw.Killable"))
	{
		for (auto& minion : pSDK->EntityManager->GetLaneMinions(2000.f, false, true))
		{
			if (!minion->IsValid(2000.f))
				continue;

			const float _predHealth = pSDK->HealthPrediction->GetHealthPrediction(minion, 0);
			if (_predHealth > 0 && _predHealth < pSDK->Player->GetAutoAttackDamage(minion).GetTotal())
			{
				SDKVECTOR position{ minion->GetPosition().ToSDK() };
				gSdkContext->_SdkDrawCircle(&position, 80, &Color::Green, 0, &Origin);
			}
		}
	}

	const bool drawGlobalRange = pSDK->Menu->GetBool("BBase.Orbwalker.Draw.GlobalRange");
	if (drawGlobalRange)
	{
		for (auto& hero : pSDK->EntityManager->GetHeroes(2000.f, false, true))
		{
			if (!hero->IsValid(2000.f))
				continue;

			const float attackRange = hero->GetAttackRange();
			const float boundingRadius = hero->GetBoundingRadius();

			SDKVECTOR heroPos{ hero->GetPosition().ToSDK() };
			if (drawGlobalRange)
				gSdkContext->_SdkDrawCircle(&heroPos, attackRange + boundingRadius, &Color::Red, 0, &Origin);
		}
	}
}

void BaseInit::Initialize(PSDK_CONTEXT context)
{
	gSdkContext = context;

	#pragma region Load SDK components

	pSDK->Menu = make_shared<Menu>();
	pSDK->Orbwalker = make_shared<Orbwalker>();
	pSDK->EntityManager = make_shared<EntityManager>();
	pSDK->Player = make_shared<AILocalPlayer>();
	pSDK->TargetSelector = make_shared <TargetSelector>();
	pSDK->Prediction = make_shared<Prediction>();
	pSDK->HealthPrediction = make_shared<HealthPrediction>();
	pSDK->Game = make_shared<Game>();
	pSDK->EventHandler = make_shared<EventHandler>();
	pSDK->Collision = make_shared<Collision>();

	EventHandler::Initialize();
	gSdkContext->_SdkConsoleWrite("[BlockchainBase] Event Handler loaded.");

	ObjectManager::Initialize();
	gSdkContext->_SdkConsoleWrite("[BlockchainBase] Object Manager loaded.");

	TargetSelector::Initialize();
	gSdkContext->_SdkConsoleWrite("[BlockchainBase] Target Selector loaded.");

	ItemDB::Initialize();
	
	Damage::Initialize();
	gSdkContext->_SdkConsoleWrite("[BlockchainBase] DamageLib loaded.");
	
	Collision::Initialize();
	gSdkContext->_SdkConsoleWrite("[BlockchainBase] Collision loaded.");

	#pragma endregion

	pSDK->EventHandler->RegisterEvent(eEventType::LibraryCall, SDK_OnLibraryRequest);
	pSDK->EventHandler->RegisterEvent(eEventType::GameScene, OnDraw);
}
