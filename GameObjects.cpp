#include "GameObjects.h"
#include "Vars.h"
#include "Damage.hpp"
#include <algorithm>
#include "ObjectManager.hpp"

Vector3 GameObject::GetAcceleration()
{
	SDKVECTOR Acceleration;
	gSdkContext->_SdkGetObjectAcceleration(Object, &Acceleration);
	return { Acceleration };
}

Vector3 GameObject::GetPosition()
{
	SDKVECTOR Position;
	gSdkContext->_SdkGetObjectPosition(Object, &Position);
	return { Position };
}

std::string GameObject::GetName()
{
	const char* Name;
	gSdkContext->_SdkGetObjectName(Object, &Name);
	return { Name };
}

bool GameObject::IsAI()
{
	return SDKSTATUS_SUCCESS(gSdkContext->_SdkIsObjectAI(Object));
}

bool GameObject::IsMinion()
{
	return SDKSTATUS_SUCCESS(gSdkContext->_SdkIsObjectMinion(Object));
}

bool GameObject::IsTurret()
{
	return SDKSTATUS_SUCCESS(gSdkContext->_SdkIsObjectTurret(Object));
}

bool GameObject::IsHero()
{
	return SDKSTATUS_SUCCESS(gSdkContext->_SdkIsObjectHero(Object));
}

bool GameObject::IsLaneMinion()
{
	bool LaneMinion;
	gSdkContext->_SdkIsMinionLaneMinion(Object, &LaneMinion);
	return LaneMinion;
}

bool GameObject::IsMonster()
{
	int TeamID;
	gSdkContext->_SdkGetObjectTeamID(Object, &TeamID);
	return IsMinion() && TeamID == TEAM_TYPE_NEUTRAL;
}

SDK_HEALTH AIClient::GetHealth()
{
	SDK_HEALTH Health;
	gSdkContext->_SdkGetUnitHealth(Object, &Health);
	return Health;
}

SDK_ABILITY_RESOURCE AIClient::GetAbilityResource()
{
	SDK_ABILITY_RESOURCE Resource;
	gSdkContext->_SdkGetUnitAbilityResource(Object, ABILITY_SLOT_PRIMARY, &Resource);
	return Resource;
}

lsPenetrationProperty AIClient::GetArmorPenetration()
{
	float Flat, Percent;
	gSdkContext->_SdkGetAIArmorPenetration(Object, &Flat, &Percent);
	return { Flat, Percent };
}

lsPenetrationProperty AIClient::GetMagicPenetration()
{
	float Flat, Percent;
	gSdkContext->_SdkGetAIMagicPenetration(Object, &Flat, &Percent);
	return { Flat, Percent };
}

float AIClient::GetArmor()
{
	float Armor;
	gSdkContext->_SdkGetAIArmor(Object, &Armor);
	return Armor;
}

float AIClient::GetMagicResist()
{
	float MagicResist;
	gSdkContext->_SdkGetAIMagicResist(Object, &MagicResist);
	return MagicResist;
}

float AIClient::GetAttackRange()
{
	float AttackRange;
	gSdkContext->_SdkGetAIAttackRange(Object, &AttackRange);
	return AttackRange;
}

float AIClient::GetRealAttackRange() { return GetAttackRange() + GetBoundingRadius(); }

float AIClient::GetAttackDelay()
{
	float AttackDelay;
	gSdkContext->_SdkGetAIAttackDelay(Object, &AttackDelay);
	return AttackDelay;
}

float AIClient::GetAttackCastDelay()
{
	float AttackCastDelay;
	gSdkContext->_SdkGetAIAttackCastDelay(Object, &AttackCastDelay);
	return AttackCastDelay;
}

lsDamageProperty AIClient::GetAttackDamage()
{
	float Base, Bonus, Total;
	gSdkContext->_SdkGetAIBaseAttackDamage(Object, &Base);
	gSdkContext->_SdkGetAIBonusAttackDamage(Object, &Bonus);
	gSdkContext->_SdkGetAIAttackDamage(Object, &Total);
	return { Base, Bonus, Total };
}

float AIClient::GetAbilityPower()
{
	float AbilityPower;
	gSdkContext->_SdkGetAIAbilityPower(Object, &AbilityPower);
	return AbilityPower;
}

float AIClient::GetCriticalStrike()
{
	float CriticalStrike;
	gSdkContext->_SdkGetAICritChance(Object, &CriticalStrike);
	return CriticalStrike;
}

std::string AIClient::GetAIName()
{
	const char* AIName;
	gSdkContext->_SdkGetAIName(Object, &AIName);
	return { AIName };
}

int AIClient::GetTypeID()
{
	int TypeID;
	gSdkContext->_SdkGetObjectTypeID(Object, &TypeID);
	return TypeID;
}

int AIClient::GetTypeFlags()
{
	int TypeFlags;
	gSdkContext->_SdkGetObjectTypeFlags(Object, &TypeFlags);
	return TypeFlags;
}

int AIClient::GetTeamID()
{
	int TeamID;
	gSdkContext->_SdkGetObjectTeamID(Object, &TeamID);
	return TeamID;
}

int AIClient::GetTargetability()
{
	int Targetability;
	gSdkContext->_SdkGetUnitTargetability(Object, &Targetability);
	return Targetability;
}

float AIClient::GetMovementSpeed()
{
	float MovementSpeed;
	gSdkContext->_SdkGetAIMovementSpeed(Object, &MovementSpeed);
	return MovementSpeed;
}

sNavData AIClient::GetNavData()
{
	sNavData NavData{};
	gSdkContext->_SdkGetAINavData(Object, &NavData.StartPosition, &NavData.EndPosition, &NavData.NextWaypoint,
		&NavData.NumberOfWaypoints, &NavData.Waypoints, &NavData.Velocity, &NavData.IsDash,
		&NavData.DashSpeed, &NavData.DashGravity);
	return NavData;
}

float AIClient::GetBoundingRadius()
{
	float BoundingRadius;
	gSdkContext->_SdkGetObjectBoundingRadius(Object, &BoundingRadius);
	return BoundingRadius;
}

SDK_SPELL AIClient::GetBasicAttack()
{
	SDK_SPELL BasicAttack;
	gSdkContext->_SdkGetAIBasicAttack(Object, &BasicAttack);
	return BasicAttack;
}

int AIClient::GetLevel()
{
	int Level;
	gSdkContext->_SdkGetHeroExperience(Object, nullptr, &Level);
	return Level;
}

unsigned AIClient::GetNetworkID()
{
	unsigned int NetworkID;
	gSdkContext->_SdkGetObjectNetworkID(Object, &NetworkID);
	return NetworkID;
}

Vector3 AIClient::GetServerPosition()
{
	SDKVECTOR ServerPosition;
	gSdkContext->_SdkGetAIServerPosition(Object, &ServerPosition);
	return { ServerPosition };
}

int AIClient::GetProjectileSpeed()
{
	const float ProjectileSpeed = GetBasicAttack().MissileSpeed;
	if (ProjectileSpeed == NULL) return INT_MAX;

	return static_cast<int>(ProjectileSpeed);
}

float AIClient::GetAttackSpeed()
{
	float AttackSpeed;
	gSdkContext->_SdkGetAIAttackSpeed(Object, &AttackSpeed);
	return AttackSpeed;
}

float AIClient::GetPhysicalDamage(const std::shared_ptr<AIClient>& target, float damage) { return Damage::GetPhysicalDamage(this, target, damage); }

float AIClient::GetMagicalDamage(const std::shared_ptr<AIClient>& target, float damage) { return Damage::GetMagicalDamage(this, target, damage); }

bool AIClient::IsDead()
{
	const bool isDeadFirstCheck = GetHealth().Current < 1.0f;
	bool isDeadSecondCheck;
	gSdkContext->_SdkIsObjectDead(Object, &isDeadSecondCheck);

	return isDeadFirstCheck || isDeadSecondCheck;
}

bool AIClient::IsVisible()
{
	bool Visible;
	gSdkContext->_SdkIsUnitVisible(Object, &Visible);
	return Visible;
}

bool AIClient::IsVisibleOnScreen()
{
	bool VisibleOnScreen;
	gSdkContext->_SdkIsObjectVisibleOnScreen(Object, &VisibleOnScreen);
	return VisibleOnScreen;
}

bool AIClient::IsInvulnerable()
{
	bool Invulnerable;
	gSdkContext->_SdkIsUnitInvulnerable(Object, &Invulnerable);
	return Invulnerable;
}

bool AIClient::IsAlly()
{
	return GetTeamID() == pSDK->Player->TeamID;
}

bool AIClient::IsEnemy()
{
	const int TeamID = GetTeamID();
	return TeamID != pSDK->Player->TeamID && TeamID != TEAM_TYPE_NEUTRAL;
}

bool AIClient::IsMoving()
{
	bool IsMoving;
	gSdkContext->_SdkIsAIMoving(Object, &IsMoving);
	return IsMoving;
}

bool AIClient::IsMelee()
{
	int CombatType;
	gSdkContext->_SdkGetAICombatType(Object, &CombatType);
	return CombatType == COMBAT_TYPE_MELEE;
}

bool AIClient::IsRanged()
{
	int CombatType;
	gSdkContext->_SdkGetAICombatType(Object, &CombatType);
	return CombatType == COMBAT_TYPE_RANGED;
}

bool AIClient::IsValid(float range, bool teamCheck, Vector3 from)
{
	if (Object != nullptr)
	{
		bool InRange;
		if (from.X != .0f && from.Y != .0f && from.Z != .0f)
			InRange = from.GetDistanceSquared(GetServerPosition()) <= range * range;
		else
			InRange = pSDK->Player->GetServerPosition().GetDistanceSquared(GetServerPosition()) <= range * range;
		if (!InRange)
			return false;

		if (!teamCheck)
			return !IsDead() && IsVisible() && GetTargetability() >= 4;

		return !IsDead() && IsVisible() && GetTargetability() >= 4 && GetTeamID() != pSDK->Player->TeamID;
	}

	return false;
}

bool AIClient::IsStunned()
{
	auto buffs{ GetBuffs() };
	const auto buffIter = std::find_if(buffs.begin(), buffs.end(), [](sBuffInfo buff)
	{
		return buff.Type == BUFF_TYPE_STUN;
	});
	return buffIter != buffs.end();
}

bool AIClient::IsSnared()
{
	auto buffs{ GetBuffs() };
	const auto buffIter = std::find_if(buffs.begin(), buffs.end(), [](sBuffInfo buff)
	{
		return buff.Type == BUFF_TYPE_SNARE;
	});
	return buffIter != buffs.end();
}

bool AIClient::IsAsleep()
{
	auto buffs{ GetBuffs() };
	const auto buffIter = std::find_if(buffs.begin(), buffs.end(), [](sBuffInfo buff)
	{
		return buff.Type == BUFF_TYPE_ASLEEP;
	});
	return buffIter != buffs.end();
}

bool AIClient::IsKnockedUp()
{
	auto buffs{ GetBuffs() };
	const auto buffIter = std::find_if(buffs.begin(), buffs.end(), [](sBuffInfo buff)
	{
		return buff.Type == BUFF_TYPE_KNOCKUP;
	});
	return buffIter != buffs.end();
}

bool AIClient::IsCharmed()
{
	auto buffs{ GetBuffs() };
	const auto buffIter = std::find_if(buffs.begin(), buffs.end(), [](sBuffInfo buff)
	{
		return buff.Type == BUFF_TYPE_CHARM;
	});
	return buffIter != buffs.end();
}

bool AIClient::IsFeared()
{
	auto buffs{ GetBuffs() };
	const auto buffIter = std::find_if(buffs.begin(), buffs.end(), [](sBuffInfo buff)
	{
		return buff.Type == BUFF_TYPE_FEAR;
	});
	return buffIter != buffs.end();
}

bool AIClient::IsTaunted()
{
	auto buffs{ GetBuffs() };
	const auto buffIter = std::find_if(buffs.begin(), buffs.end(), [](sBuffInfo buff)
	{
		return buff.Type == BUFF_TYPE_TAUNT;
	});
	return buffIter != buffs.end();
}

bool AIClient::IsSilenced()
{
	auto buffs{ GetBuffs() };
	const auto buffIter = std::find_if(buffs.begin(), buffs.end(), [](sBuffInfo buff)
	{
		return buff.Type == BUFF_TYPE_SILENCE;
	});
	return buffIter != buffs.end();
}

bool AIClient::IsBlinded()
{
	auto buffs{ GetBuffs() };
	const auto buffIter = std::find_if(buffs.begin(), buffs.end(), [](sBuffInfo buff)
	{
		return buff.Type == BUFF_TYPE_BLIND;
	});
	return buffIter != buffs.end();
}

bool AIClient::IsRooted()
{
	return IsSnared();
}

bool AIClient::IsEntangled()
{
	auto buffs{ GetBuffs() };
	const auto buffIterOne = std::find_if(buffs.begin(), buffs.end(), [](sBuffInfo buff)
	{
		return buff.Type == BUFF_TYPE_SNARE;
	});
	const auto buffIterTwo = std::find_if(buffs.begin(), buffs.end(), [](sBuffInfo buff)
	{
		return buff.Type == BUFF_TYPE_DISARM;
	});
	return buffIterOne != buffs.end() && buffIterTwo != buffs.end();
}

bool AIClient::IsSlowed()
{
	auto buffs{ GetBuffs() };
	const auto buffIter = std::find_if(buffs.begin(), buffs.end(), [](sBuffInfo buff)
	{
		return buff.Type == BUFF_TYPE_SLOW;
	});
	return buffIter != buffs.end();
}

bool AIClient::IsCrippled()
{
	auto buffs{ GetBuffs() };
	const auto buffIter = std::find_if(buffs.begin(), buffs.end(), [](sBuffInfo buff)
	{
		return buff.Type == BUFF_TYPE_ATTACK_SPEED_DEBUFF;
	});
	return buffIter != buffs.end();
}

bool AIClient::IsDisarmed()
{
	auto buffs{ GetBuffs() };
	const auto buffIter = std::find_if(buffs.begin(), buffs.end(), [](sBuffInfo buff)
	{
		return buff.Type == BUFF_TYPE_DISARM;
	});
	return buffIter != buffs.end();
}

bool AIClient::IsSuppressed()
{
	auto buffs{ GetBuffs() };
	const auto buffIter = std::find_if(buffs.begin(), buffs.end(), [](sBuffInfo buff)
	{
		return buff.Type == BUFF_TYPE_SUPPRESSION;
	});
	return buffIter != buffs.end();
}

bool AIClient::IsInStasis()
{
	auto buffs{ GetBuffs() };
	const auto buffIter = std::find_if(buffs.begin(), buffs.end(), [](sBuffInfo buff)
	{
		return buff.Type == BUFF_TYPE_INVULNERABILITY;
	});
	return buffIter != buffs.end() && GetTargetability() < 4;
}

float AIClient::GetDistance(Vector3 target)
{
	return GetPosition().GetDistance(target);
}

float AIClient::GetDistanceSquared(Vector3 target)
{
	return GetPosition().GetDistanceSquared(target);
}

sBuffInfo AIClient::GetBuff(std::string buffName)
{
	auto buffs{ GetBuffs() };
	sBuffInfo bestEntry{};

	std::for_each(buffs.begin(), buffs.end(), [&](sBuffInfo buff)
	{
		if (buff.Name.find(buffName) != std::string::npos)
		{
			if (buff.StartTime > bestEntry.StartTime)
			{
				bestEntry = buff;
			}
		}
	});

	return bestEntry;
}

int g_LastGetBuffs;
int g_LastTicksCount;
std::vector<sBuffInfo> AIClient::GetBuffs()
{
	g_LastTicksCount = pSDK->Game->GetGameTicks();
	if (Object == pSDK->Player->Object)
	{
		if (g_LastTicksCount - g_LastGetBuffs < 30)
			return ObjectManager::LocalBuffsCache;
	}

	std::vector<sBuffInfo> tempBuffs;

	gSdkContext->_SdkEnumAIBuffs(Object, [](unsigned char Type, float StartTime, float EndTime, const char* Name, void* CasterObject, unsigned int CasterID, int Stacks, bool HasCount, int Count, PSDK_SPELL Spell, void* UserData) -> bool
	{
		UNREFERENCED_PARAMETER(CasterObject);
		UNREFERENCED_PARAMETER(CasterID);
		UNREFERENCED_PARAMETER(HasCount);
		UNREFERENCED_PARAMETER(Spell);

		if (EndTime > pSDK->Game->GetGameTime())
		{
			auto vec = static_cast<std::vector<sBuffInfo>*>(UserData);

			sBuffInfo buffInfo;
			buffInfo.Type = Type;
			buffInfo.Name = std::string(Name);
			buffInfo.Count = Count;
			buffInfo.Stacks = Stacks;
			buffInfo.StartTime = StartTime;
			buffInfo.EndTime = EndTime;

			vec->push_back(buffInfo);
		}

		return true;
	}, &tempBuffs);

	g_LastGetBuffs = g_LastTicksCount;
	if (Object == pSDK->Player->Object)
		ObjectManager::LocalBuffsCache = tempBuffs;

	return tempBuffs;
}

bool AIClient::HasBuff(std::string buffName)
{
	return GetBuff(std::move(buffName)).Stacks > 0;
}

bool AIClient::HasBuffType(int buffType)
{
	auto buffs{ GetBuffs() };
	const auto buffIter = std::find_if(buffs.begin(), buffs.end(), [buffType](sBuffInfo buff)
	{
		return buff.Type == buffType;
	});

	return buffIter != buffs.end();
}

bool AIClient::HasItem(int itemID)
{
	auto items{ GetItems() };
	const auto itemIter = std::find_if(items.begin(), items.end(), [itemID](SDK_ITEM item)
	{
		return item.ItemID == itemID;
	});

	return itemIter != items.end();
}

int g_LastItemsUpdate = 0;
std::vector<SDK_ITEM> AIClient::GetItems()
{
	if (Object == pSDK->Player->Object)
	{
		const int gameTicks = pSDK->Game->GetGameTicks();
		if (gameTicks - g_LastItemsUpdate < 100)
			return ObjectManager::LocalItems;

		ObjectManager::LocalItems.clear();
		for (int i = ITEM_SLOT_START; i <= ITEM_SLOT_MAX; i++)
		{
			SDK_ITEM item;
			gSdkContext->_SdkGetHeroItem(pSDK->Player->Object, static_cast<unsigned char>(i), &item);
			if (item.ItemID == 0)
				continue;

			ObjectManager::LocalItems.push_back(item);
		}

		g_LastItemsUpdate = gameTicks;
		return ObjectManager::LocalItems;
	}

	vector<SDK_ITEM> tempVec;

	gSdkContext->_SdkConsoleClear();
	for (unsigned char i = ITEM_SLOT_START; i <= ITEM_SLOT_MAX; i++)
	{
		SDK_ITEM item;
		gSdkContext->_SdkGetHeroItem(Object, i, &item);
		if (item.ItemID == 0)
			continue;

		tempVec.push_back(item);
	}

	return tempVec;
}

bool AIClient::Compare(const std::shared_ptr<AIClient>& object)
{
	return Object != nullptr && object != nullptr && object->Object != nullptr && IsValid(30000, false) && object->IsValid(30000, false) && GetNetworkID() == object->GetNetworkID();
}

float AIHeroClient::GetLethality()
{
	float Lethality;
	gSdkContext->_SdkGetAILethality(Object, &Lethality);
	return Lethality;
}

bool AIHeroClient::CanCastSpell(int spellSlot)
{
	int CanCastFlags;
	gSdkContext->_SdkCanAICastSpell(Object, static_cast<unsigned char>(spellSlot), nullptr, &CanCastFlags);
	return CanCastFlags == SPELL_CAN_CAST_OK;
}

bool AIHeroClient::CanCastSpellOnTarget(int spellSlot, const std::shared_ptr<AIClient>& target)
{
	return CanCastSpell(spellSlot) && IsValid(GetSpell(spellSlot).CastRange, true, target->GetServerPosition());
}

SDK_SPELL AIHeroClient::GetSpell(int spellSlot)
{
	SDK_SPELL Spell;
	gSdkContext->_SdkGetAISpell(Object, static_cast<unsigned char>(spellSlot), &Spell);
	return Spell;
}

void AILocalPlayer::CastSpell(int spellSlot)
{
	if (spellSlot < SPELL_SLOT_START || spellSlot > SPELL_SLOT_MAX)
		return;
	if (Object == nullptr)
		return;
	if (!CanCastSpell(spellSlot))
		return;

	gSdkContext->_SdkCastSpellLocalPlayer(Object, nullptr, static_cast<unsigned char>(spellSlot), SPELL_CAST_START);
}

void AILocalPlayer::CastSpell(int spellSlot, const std::shared_ptr<AIClient>& target)
{
	if (spellSlot < SPELL_SLOT_START || spellSlot > SPELL_SLOT_MAX)
		return;
	if (Object == nullptr)
		return;
	if (!CanCastSpellOnTarget(spellSlot, target))
		return;

	gSdkContext->_SdkCastSpellLocalPlayer(target->Object, nullptr, static_cast<unsigned char>(spellSlot), SPELL_CAST_START);
}

void AILocalPlayer::CastSpell(int spellSlot, Vector3 targetLocation)
{
	if (spellSlot < SPELL_SLOT_START || spellSlot > SPELL_SLOT_MAX)
		return;
	if (Object == nullptr)
		return;
	if (!CanCastSpell(spellSlot))
		return;

	const float castRange = pSDK->Player->GetSpell(spellSlot).CastRange;
	if (pSDK->Player->GetPosition().GetDistanceSquared(targetLocation) > castRange * castRange)
		return;

	auto Location{ targetLocation.ToSDK() };
	gSdkContext->_SdkCastSpellLocalPlayer(nullptr, &Location, static_cast<unsigned char>(spellSlot), SPELL_CAST_START);
}

void AILocalPlayer::Attack(const std::shared_ptr<AIClient>& target, bool pet)
{
	if (target != nullptr && target->Object != nullptr)
		gSdkContext->_SdkAttackTargetLocalPlayer(target->Object, pet);
}

sDamageInfo AILocalPlayer::GetAutoAttackDamage(const std::shared_ptr<AIClient>& target)
{
	return Damage::GetAutoAttackDamage(target);
}

float AILocalPlayer::GetTimeToHit(const std::shared_ptr<AIClient>& target)
{
	float _time = pSDK->Player->GetAttackCastDelay() * 1000.f - 100.f + static_cast<float>(pSDK->Game->GetPing()) / 2.f;
	if (abs(GetProjectileSpeed() - FLT_MAX) > FLT_EPSILON)
	{
		float _missileSpeed = pSDK->Player->GetBasicAttack().MissileSpeed;
		if (_missileSpeed == 0)
			_missileSpeed = FLT_MAX;

		_time += 1000.f * max(0, pSDK->Player->GetServerPosition().GetDistance(target->GetServerPosition()) - pSDK->Player->GetBoundingRadius()) / _missileSpeed;
	}
	return _time;
}

eTurretType AITurretClient::GetTurretType()
{
	int turretType;
	gSdkContext->_SdkGetTurretInfo(Object, &turretType, nullptr);

	if (turretType == TURRET_POSITION_OUTER)
		return eTurretType::Outer;
	if (turretType == TURRET_POSITION_INNER)
		return eTurretType::Inner;
	if (turretType == TURRET_POSITION_BASE)
		return eTurretType::Inhibitor;
	if (turretType == TURRET_POSITION_NEXUS_1 || turretType == TURRET_POSITION_NEXUS_2)
		return eTurretType::Nexus;
	else if (turretType == TURRET_POSITION_FOUNTAIN)
		return eTurretType::Fountain;

	return eTurretType::None;
}
