#pragma once

#include "IEntityManager.h"

class Damage
{
public:
	// Damage incoming from items
	static sDamageInfo GetStaticItemsDamage(std::shared_ptr<AIClient> source);
	static sDamageInfo GetDynamicItemsDamage(std::shared_ptr<AIClient> source, std::shared_ptr<AIClient> target);

	// Damage incoming from hero passives, spells, etc.
	static void InitStaticHeroDamage();
	static void InitDynamicHeroDamage();
	static sDamageInfo GetStaticHeroDamage();
	static sDamageInfo GetDynamicHeroDamage(std::shared_ptr<AIClient> target);

	// Full damage calculations
	static float GetDamageReductionModifier(AIClient* source, std::shared_ptr<AIClient> target, float amount, eDamageType damageType);
	static float GetPhysicalDamage(AIClient* source, std::shared_ptr<AIClient> target, float damage);
	static float GetMagicalDamage(AIClient* source, std::shared_ptr<AIClient> target, float damage);
	static sDamageInfo GetAutoAttackDamage(std::shared_ptr<AIClient> target);

	static void Initialize();
};
