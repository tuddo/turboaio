#include "TargetSelector.hpp"
#include "cpplinq.hpp"
#include "Vars.h"
#include "HealthPrediction.hpp"
#include "Orbwalker.hpp"
#include "Menu.hpp"
#include "EventHandler.hpp"
#include "Color.h"

using namespace cpplinq;

std::shared_ptr<AIClient> TargetSelector::ForceTarget = nullptr;
std::shared_ptr<AIHeroClient> TargetSelector::ManualForceTarget = nullptr;
std::map<std::shared_ptr<AIClient>, int> TargetSelector::ManualPriorities{};
std::shared_ptr<AIClient> TargetSelector::LaneClearMinion = nullptr;

// kalistaexpungemarker - Stacks ?
// VayneSilveredDebuff - Stacks
// TwitchDeadlyVenom - Count
// ekkostacks - Stacks
// DariusHemo - Stacks
// gnarwproc - Stacks
// tahmkenchpdebuffcounter - Stacks
// VarusWDebuff - Stacks

std::vector<std::string> StackNames = {
	"kalistaexpungemarker", "VayneSilveredDebuff", "TwitchDeadlyVenom", "ekkostacks", "DariusHemo", "gnarwproc",
	"tahmkenchpdebuffcounter", "VarusWDebuff"
};

float TargetSelector::GetAttackRangeToTarget(const std::shared_ptr<AIClient>& target)
{
	float result = pSDK->Player->GetRealAttackRange();

	if (target != nullptr)
	{
		if (target->IsValid(3000.f))
		{
			if (target->GetAIName() == "Caitlyn")
			{
				if (target->GetBuff("caitlynyordletrapinternal").Stacks)
					result += 650.f;
			}

			return result + target->GetBoundingRadius();
		}
	}

	return result;
}

bool TargetSelector::InAutoAttackRange(const std::shared_ptr<AIClient>& target)
{
	if (target != nullptr)
	{
		const float myRange = GetAttackRangeToTarget(target);
		return pSDK->Game->GetDistanceSquared(target->GetServerPosition(), pSDK->Player->GetServerPosition()) <= myRange * myRange;
	}

	return false;
}

void __cdecl TargetSelector_UpdateTarget(void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	if (GetAsyncKeyState(VK_LBUTTON) & 1)
	{
		shared_ptr<AIHeroClient> selectedTarget = nullptr;

		for (const auto& hero : pSDK->EntityManager->GetHeroes(1500, false, true))
		{
			if (!hero->IsValid(1500))
				continue;

			bool mouseOver;
			gSdkContext->_SdkGetUnitIsMouseOver(hero->Object, &mouseOver);
			if (mouseOver)
			{
				selectedTarget = hero;
				break;
			}
		}

		TargetSelector::ManualForceTarget = selectedTarget;
	}

	if (TargetSelector::ManualForceTarget != nullptr)
	{
		if (TargetSelector::ManualForceTarget->Object != nullptr)
		{
			if (!TargetSelector::ManualForceTarget->IsValid(1500))
			{
				TargetSelector::ManualForceTarget = nullptr;
				return;
			}
		}
	}

	if (TargetSelector::ForceTarget != nullptr)
	{
		if (TargetSelector::ForceTarget->Object != nullptr)
		{
			if (!TargetSelector::ForceTarget->IsValid(1500))
				TargetSelector::ForceTarget = nullptr;
		}
	}
}

bool TargetSelector::ShouldWait()
{
	const auto minions = from(pSDK->EntityManager->GetLaneMinions(pSDK->Player->GetRealAttackRange(), false, true)) >> where([](std::shared_ptr<AIClient> minion)
	{
		if (minion->IsValid(pSDK->Player->GetRealAttackRange(), false))
		{
			const float healthPred = HealthPrediction::GetHealthPredictionLaneClear(
				minion,
				static_cast<int>(pSDK->Player->GetAttackDelay() * 1000 * 2)
			);

			if (healthPred < pSDK->Player->GetAutoAttackDamage(minion).GetTotal())
				return true;
		}

		return false;
	}) >> cpplinq::count();

	return minions != 0;
}

std::shared_ptr<AIHeroClient> TargetSelector::GetBestHeroTarget(float range, bool forOrbwalker)
{
	const int _modeInt = pSDK->Menu->GetInt("Base.TargetSelector.Mode");
	const auto Mode = static_cast<eTargetingMode>(_modeInt);
	
	std::shared_ptr<AIHeroClient> bestTarget = nullptr;

	float _range = range;
	if (forOrbwalker)
		range = 2000.f;

	for (const auto& hero : pSDK->EntityManager->GetHeroes(range, false, true))
	{
		if (hero == nullptr)
			continue;
		if (hero->GetNetworkID() == pSDK->Player->GetNetworkID())
			continue;
		if (!hero->IsValid(range))
			continue;
		if (hero->IsInvulnerable())
			continue;

		if (hero->GetTypeFlags() & OBJECT_FLAG_HERO)
		{
			if (forOrbwalker)
				_range = GetAttackRangeToTarget(hero);

			auto localPos{ pSDK->Player->GetPosition() };
			localPos.Y = .0f;
			auto heroPos{ hero->GetPosition() };
			heroPos.Y = .0f;

			if (pSDK->Game->GetDistanceSquared(localPos, heroPos) <= _range * _range)
			{
				if (bestTarget == nullptr)
				{
					bestTarget = hero;
					continue;
				}

				const SDK_HEALTH heroHealth = hero->GetHealth();
				const SDK_HEALTH bestTargetHP = bestTarget->GetHealth();

				float cmp = .0f;
				if (Mode == eTargetingMode::AutoPriority) cmp = (1 + heroHealth.Current + heroHealth.PhysicalShield + heroHealth.AllShield) * pSDK->TargetSelector->GetPriorityMod(hero) / pSDK->Player->GetAutoAttackDamage(hero).GetTotal();
				else if (Mode == eTargetingMode::MostAD) cmp = hero->GetAttackDamage().Total;
				else if (Mode == eTargetingMode::MostAP) cmp = hero->GetAbilityPower();
				else if (Mode == eTargetingMode::Closest) cmp = pSDK->Game->GetDistanceSquared(pSDK->Player->GetServerPosition(), hero->GetServerPosition());
				else if (Mode == eTargetingMode::NearMouse) cmp = pSDK->Game->GetDistanceSquared(hero->GetServerPosition(), pSDK->Game->GetWorldCursorPosition());
				else if (Mode == eTargetingMode::LessAttack) cmp = (1 + heroHealth.Current + heroHealth.PhysicalShield + heroHealth.AllShield) / pSDK->Player->GetAutoAttackDamage(hero).GetTotal();

				switch (Mode)
				{
				case eTargetingMode::AutoPriority:
					if (cmp < (1 + bestTargetHP.Current + bestTargetHP.PhysicalShield + bestTargetHP.AllShield) * pSDK->TargetSelector->GetPriorityMod(bestTarget) / pSDK->Player->GetAutoAttackDamage(bestTarget).GetTotal())
						bestTarget = hero;
					break;
				case eTargetingMode::MostAD:
					if (cmp > bestTarget->GetAttackDamage().Total)
						bestTarget = hero;
					break;
				case eTargetingMode::MostAP:
					if (cmp > bestTarget->GetAbilityPower())
						bestTarget = hero;
					break;
				case eTargetingMode::Closest:
					if (cmp < pSDK->Game->GetDistanceSquared(pSDK->Player->GetServerPosition(), bestTarget->GetServerPosition()))
						bestTarget = hero;
					break;
				case eTargetingMode::NearMouse:
					if (cmp < pSDK->Game->GetDistanceSquared(pSDK->Game->GetWorldCursorPosition(), bestTarget->GetServerPosition()))
						bestTarget = hero;
					break;
				case eTargetingMode::LessAttack:
					if (cmp < (1 + bestTargetHP.Current + bestTargetHP.PhysicalShield + bestTargetHP.AllShield) / pSDK->Player->GetAutoAttackDamage(bestTarget).GetTotal())
						bestTarget = hero;
					break;
				default:
					break;
				}
			}
		}
	}

	return bestTarget;
}

void TargetSelector::Initialize()
{
	pSDK->EventHandler->RegisterEvent(eEventType::GameScene, TargetSelector_UpdateTarget);
}

void TargetSelector::EnemyHeroesCreated()
{
	std::vector<std::shared_ptr<AIHeroClient>> sortedVec = from(pSDK->EntityManager->GetHeroes(100000.f, false, true)) >> orderby_ascending([](std::shared_ptr<AIHeroClient> AI)
	{
		const SDK_HEALTH health = AI->GetHealth();
		return (health.Current + health.PhysicalShield + health.AllShield) / pSDK->Player->GetAutoAttackDamage(AI).GetTotal();
	}) >> to_vector();

	int currentPriority = 5;
	for (auto& vecEntry : sortedVec)
	{
		ManualPriorities[vecEntry] = currentPriority;
		currentPriority--;
	}
}

std::shared_ptr<AIClient> TargetSelector::GetManualTarget()
{
	return ManualForceTarget;
}

std::shared_ptr<AIClient> TargetSelector::GetForcedTarget()
{
	return ForceTarget;
}

void TargetSelector::SetForcedTarget(std::shared_ptr<AIClient> target)
{
	ForceTarget = target;
}

std::shared_ptr<AIClient> TargetSelector::GetBestTarget(float range, bool considerManualTarget, bool forOrbwalker)
{
	const OrbwalkerMode orbMode = pSDK->Orbwalker->GetOrbwalkerMode();

	#pragma region Killable Minion

	if (orbMode == OrbwalkerMode::LastHit || orbMode == OrbwalkerMode::LaneClear || orbMode == OrbwalkerMode::Harras)
	{
		auto minions = from(pSDK->EntityManager->GetMinions(range, false, true)) >> where([range](std::shared_ptr<AIClient> minion)
		{
			return minion->IsValid(range);
		}) >> orderby_descending([](std::shared_ptr<AIClient> minion)
		{
			return minion->GetAIName().find("Siege") != std::string::npos;
		}) >> thenby_descending([](std::shared_ptr<AIClient> minion)
		{
			return minion->GetAIName().find("Super") != std::string::npos;
		}) >> thenby_ascending([](std::shared_ptr<AIClient> minion)
		{
			return minion->GetHealth().Current;
		}) >> to_vector();

		for (const auto& minion : minions)
		{
			const SDK_HEALTH _minionHealth = minion->GetHealth();
			if (_minionHealth.Current < pSDK->Player->GetAutoAttackDamage(minion).GetTotal())
			{
				return minion;
			}

			if (_minionHealth.Max <= 10)
			{
				if (_minionHealth.Current <= 1)
				{
					return minion;
				}
			}
			else
			{
				const float _predHealth = pSDK->HealthPrediction->GetHealthPrediction(minion, 0);
				if (_predHealth > 0 && _predHealth < pSDK->Player->GetAutoAttackDamage(minion).GetTotal())
				{
					return minion;
				}
			}
		}
	}

	#pragma endregion

	#pragma region Manual Forced Target

	if (considerManualTarget)
	{
		// Manual forced target
		if (ManualForceTarget != nullptr)
		{
			if (ManualForceTarget->Object != nullptr)
			{
				float _range = .0f;
				if (forOrbwalker)
					_range = GetAttackRangeToTarget(ManualForceTarget);
				else
					_range = range;

				if (ManualForceTarget->IsValid(_range))
					return ManualForceTarget;
			}
		}
	}

	#pragma endregion

	#pragma region Forced Target

	if (ForceTarget != nullptr)
	{
		if (ForceTarget->Object != nullptr)
		{
			float _range = .0f;
			if (forOrbwalker)
				_range = GetAttackRangeToTarget(ForceTarget);
			else
				_range = range;

			if (ForceTarget->IsValid(_range))
				return ForceTarget;
		}
	}

	#pragma endregion

	#pragma region Turrets / Inhibitors / Nexus

	if (orbMode == OrbwalkerMode::LaneClear || orbMode == OrbwalkerMode::Harras || orbMode == OrbwalkerMode::LastHit)
	{
		auto nexus = from(pSDK->EntityManager->GetNexus(range, false, true)) >> where([range](std::shared_ptr<AIClient> nexus)
		{
			return nexus->IsValid(range);
		}) >> to_vector();
		if (!nexus.empty())
			return nexus.front();

		auto inhibitors = from(pSDK->EntityManager->GetInhibitors(range, false, true)) >> where([range](std::shared_ptr<AIClient> inhibitor)
		{
			return inhibitor->IsValid(range);
		}) >> orderby_descending([](std::shared_ptr<AIClient> inhibitor)
		{
			return inhibitor->GetHealth().Current;
		}) >> to_vector();
		if (!inhibitors.empty())
			return inhibitors.front();

		auto turrets = from(pSDK->EntityManager->GetTurrets(range, false, true)) >> where([range](std::shared_ptr<AITurretClient> turret)
		{
			return turret->IsValid(range);
		}) >> orderby_descending([](std::shared_ptr<AITurretClient> turret)
		{
			return turret->GetHealth().Current;
		}) >> to_vector();
		if (!turrets.empty())
			return turrets.front();
	}

	#pragma endregion

	#pragma region Champions

	if (orbMode == OrbwalkerMode::Combo || orbMode == OrbwalkerMode::Harras || orbMode == OrbwalkerMode::LaneClear && pSDK->Menu->GetBool("BBase.Orbwalker.AttackHeroes"))
	{
		const auto bestTarget = GetBestHeroTarget(range, forOrbwalker);
		if (bestTarget != nullptr)
		{
			return bestTarget;
		}
	}

	#pragma endregion

	#pragma region Jungle Clear

	if (orbMode == OrbwalkerMode::LaneClear || orbMode == OrbwalkerMode::Harras)
	{
		const auto jungleVec = pSDK->EntityManager->GetJungle(2000.f);
		const auto jungle = from(jungleVec) >> where([range](std::shared_ptr<AIClient> minion)
		{
			if (minion != nullptr)
			{
				if (minion->IsValid(range, false) && minion->GetTeamID() == TEAM_TYPE_NEUTRAL)
				{
					return true;
				}
			}

			return false;
		});

		if (pSDK->Menu->GetBool("BBase.Orbwalker.PrioritizeSmall"))
		{
			const auto result = jungle >> orderby_ascending([](std::shared_ptr<AIClient> minion)
			{
				return minion->GetHealth().Max;
			}) >> to_vector();

			if (!result.empty())
				return result.front();
		}

		const auto result = jungle >> orderby_descending([](std::shared_ptr<AIClient> minion)
		{
			return minion->GetHealth().Max;
		}) >> to_vector();

		if (!result.empty())
		{
			return result.front();
		}
	}

	#pragma endregion

	#pragma region Lane Clear

	if (orbMode == OrbwalkerMode::LaneClear)
	{
		if (!ShouldWait())
		{
			if (LaneClearMinion != nullptr)
			{
				if (LaneClearMinion->IsValid(pSDK->Player->GetRealAttackRange()))
				{
					const SDK_HEALTH _minionHealth = LaneClearMinion->GetHealth();
					if (_minionHealth.Max <= 10)
					{
						return LaneClearMinion;
					}

					const float _predHealth = HealthPrediction::GetHealthPredictionLaneClear(LaneClearMinion, static_cast<int>(pSDK->Player->GetAttackDelay() * 1000.f * 2.f));
					if (_predHealth >= 2.f * pSDK->Player->GetAutoAttackDamage(LaneClearMinion).GetTotal() || abs(_predHealth - _minionHealth.Current) < FLT_EPSILON)
					{
						return LaneClearMinion;
					}
				}
			}

			auto _validMinions = from(pSDK->EntityManager->GetLaneMinions(range, false, true)) >> where([range](std::shared_ptr<AIClient> minion)
			{
				return minion->IsValid(range) && minion->GetTeamID() != TEAM_TYPE_NEUTRAL;
			}) >> to_vector();

			for (auto& minion : _validMinions)
			{
				const SDK_HEALTH _minionHealth = minion->GetHealth();

				if (_minionHealth.Max <= 10)
				{
					LaneClearMinion = minion;
					return minion;
				}

				const float predHealth = HealthPrediction::GetHealthPredictionLaneClear(
					minion,
					static_cast<int>(pSDK->Player->GetAttackDelay() * 1000 * 2)
				);
				if (predHealth >= 2 * pSDK->Player->GetAutoAttackDamage(minion).GetTotal() || abs(predHealth - _minionHealth.Current) < FLT_EPSILON)
				{
					LaneClearMinion = minion;
					return minion;
				}
			}
		}
	}

	#pragma endregion

	return nullptr;
}

int TargetSelector::GetPriority(std::shared_ptr<AIHeroClient> target)
{
	const int priority = ManualPriorities[target];
	if (priority == 0)
		return 1;

	return ManualPriorities[target];
}

float TargetSelector::GetPriorityMod(std::shared_ptr<AIHeroClient> target)
{
	switch (ManualPriorities[target])
	{
	case 2:
		return 1.5f;
	case 3:
		return 1.75f;
	case 4:
		return 2.f;
	case 5:
		return 2.5f;
	default:
		return 1.f;
	}
}