#pragma once

#include "IEntityManager.h"
#include "Math.hpp"

struct EntityManager : IEntityManager
{
	std::shared_ptr<AIClient> GetAIByID(unsigned int netID) override;
	std::shared_ptr<AIClient> GetAIByObject(void* object) override;
	std::vector<std::shared_ptr<AIClient>> GetMinions(float range, bool ally, bool enemy) override;
	std::vector<std::shared_ptr<AIClient>> GetLaneMinions(float range, bool ally, bool enemy) override;
	std::vector<std::shared_ptr<AIClient>> GetJungle(float range) override;
	std::vector<std::shared_ptr<AIHeroClient>> GetHeroes(float range, bool ally, bool enemy) override;
	std::vector<std::shared_ptr<AITurretClient>> GetTurrets(float range, bool ally, bool enemy) override;
	std::vector<std::shared_ptr<AIClient>> GetInhibitors(float range, bool ally, bool enemy) override;
	std::vector<std::shared_ptr<AIClient>> GetNexus(float range, bool ally, bool enemy) override;
};