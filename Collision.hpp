#pragma once

#include "IEntityManager.h"
#include "Math.hpp"
#include "IPrediction.h"
#include "ICollision.h"

struct Collision : ICollision
{
	struct WALL_INFO
	{
		GameObject* WallObject;
		int WallLevel;
		int WallCast;
		Vector2 WallCastPosition;
	} static Wall;

	static void Initialize();

	bool GetWallCollision(Vector2& startPos, Vector2& endPos, float width) override;
};
