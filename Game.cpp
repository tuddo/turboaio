#include "Game.h"
#include "Vars.h"
#include "Utils.hpp"
#include "ObjectManager.hpp"

unsigned Game::GetPing()
{
	unsigned int Ping;
	gSdkContext->_SdkGetNetPing(&Ping);
	return Ping;
}

float Game::GetGameTime()
{
	float GameTime;
	gSdkContext->_SdkGetGameTime(&GameTime);
	return GameTime;
}

unsigned Game::GetGameTicks()
{
	const float GameTime = GetGameTime();
	return static_cast<unsigned int>(GameTime * 1000);
}

float Game::GetDistance(Vector3 vector1, Vector3 vector2)
{
	return vector1.GetDistance(vector2);
}

float Game::GetDistanceSquared(Vector3 vector1, Vector3 vector2)
{
	return vector1.GetDistanceSquared(vector2);
}

Vector3 Game::GetWorldCursorPosition()
{
	SDKPOINT CursorScreenPosition = Utils::GetDirectXCursorPos();
	SDKVECTOR WorldScreenPosition;
	gSdkContext->_SdkScreenToWorld(&CursorScreenPosition, &WorldScreenPosition);

	return WorldScreenPosition;
}

int Game::CountEnemiesAtPosition(Vector3 position, float range)
{
	int Count = 0;

	const float squaredRange = range * range;
	for (auto& hero : ObjectManager::EnemyHeroes)
	{
		if (hero.second->GetDistanceSquared(position) <= squaredRange)
			Count++;
	}

	return Count;
}

bool Game::IsChatOpened()
{
	bool ChatOpened;
	gSdkContext->_SdkIsChatActive(&ChatOpened);
	return ChatOpened;
}

bool Game::IsOverlayOpened()
{
	bool OverlayOpened;
	gSdkContext->_SdkUiIsOverlayVisible(&OverlayOpened);
	return OverlayOpened;
}

void Game::SendChat(std::string message)
{
	gSdkContext->_SdkSendChatLocalPlayer(message.c_str());
}

void Game::ShowEmote(unsigned char emoteID)
{
	gSdkContext->_SdkShowEmoteLocalPlayer(emoteID);
}

bool Game::IsHeroInGame(std::string heroName)
{
	for (auto& hero : ObjectManager::Heroes)
	{
		if (hero.second->GetName().find(heroName) != std::string::npos)
			return true;
	}

	return false;
}