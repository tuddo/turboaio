#pragma once

#include "IGame.h"

struct Game : IGame
{
	unsigned GetPing() override;
	float GetGameTime() override;
	unsigned GetGameTicks() override;
	float GetDistance(Vector3 vector1, Vector3 vector2) override;
	float GetDistanceSquared(Vector3 vector1, Vector3 vector2) override;
	Vector3 GetWorldCursorPosition() override;
	int CountEnemiesAtPosition(Vector3 position, float range) override;
	bool IsChatOpened() override;
	bool IsOverlayOpened() override;
	void SendChat(std::string message) override;
	void ShowEmote(unsigned char emoteID) override;
	bool IsHeroInGame(std::string heroName) override;
};