#pragma once

#include "IEntityManager.h"

struct ITargetSelector
{
	// Target Selector 
	virtual std::shared_ptr<AIClient> GetManualTarget() = 0;
	virtual std::shared_ptr<AIClient> GetForcedTarget() = 0;
	virtual void SetForcedTarget(std::shared_ptr<AIClient> target) = 0;
	virtual std::shared_ptr<AIClient> GetBestTarget(float range, bool considerManualTarget = true, bool forOrbwalker = false) = 0;
	virtual int GetPriority(std::shared_ptr<AIHeroClient> target) = 0;
	virtual float GetPriorityMod(std::shared_ptr<AIHeroClient> target) = 0;
};
