#pragma once

#include <Windows.h>
#include "../sdkapi.h"
#include "Vars.h"

enum class SpellState : int
{
	Ready = SPELL_CAN_CAST_OK,
	NotLearned = SPELL_CAN_CAST_FLAG_NOT_LEARNED,
	Disabled = SPELL_CAN_CAST_FLAG_DISABLED,
	Suppressed = SPELL_CAN_CAST_FLAG_CROWD_CONTROLLED,
	Cooldown = SPELL_CAN_CAST_FLAG_COOLDOWN0,
	NoMana = SPELL_CAN_CAST_FLAG_NO_MANA,
	Unknown = SPELL_CAN_CAST_FLAG_UNKNOWN0
};

enum class SpellToggleState : int
{
	None = SPELL_TOGGLE_STATE_NONE,
	Off = SPELL_TOGGLE_STATE_OFF,
	On = SPELL_TOGGLE_STATE_ON,
	Unknown = -1
};

enum class SpellSlot : int
{
	Q,
	W,
	E,
	R,
	Summoner1,
	Summoner2,
	Item1,
	Item2,
	Item3,
	Item4,
	Item5,
	Item6,
	Trinket,
	Recall,
	BasicAttack = 65,
	Unknown
};

struct Spell
{
	SpellSlot Slot;
	float Range;
	float Delay;
	float Speed;
	float Width;

	SDK_SPELL SDKSpell() const
	{
		return pSDK->Player->GetSpell(static_cast<int>(Slot));
	}
	bool IsValid() const
	{
		const int nSlot = static_cast<int>(Slot);
		return nSlot >= ITEM_SLOT_START && nSlot <= ITEM_SLOT_MAX;
	}
	bool IsLearned() const
	{
		return IsValid() && SDKSpell().Learned;
	}
	int Level() const
	{
		return IsValid() ? SDKSpell().Level : 0;
	}
	std::string Name() const
	{
		if (!IsValid())
			return nullptr;

		const auto scriptName = SDKSpell().ScriptName ;
		return scriptName ? std::string(scriptName) : nullptr;
	}
	SpellState State() const
	{
		if (!IsValid())
			return SpellState::Unknown;

		int canCastFlags = 1;
		gSdkContext->_SdkCanAICastSpell(pSDK->Player->Object, static_cast<unsigned char>(Slot), nullptr, &canCastFlags);
		return static_cast<SpellState>(canCastFlags);
	}
	SpellToggleState ToggleState() const
	{
		return IsValid() ? static_cast<SpellToggleState>(SDKSpell().ToggleState) : SpellToggleState::Unknown;
	}
	bool IsReady() const
	{
		return State() == SpellState::Ready;
	}
	bool IsOnCooldown() const
	{
		return IsValid() && State() >= SpellState::Cooldown;
	}
	float ManaCost() const
	{
		if (!IsValid() || !IsLearned())
			return 0.f;

		return SDKSpell().ManaCost;
	}
	int CurrentAmmo() const
	{
		return IsValid() ? SDKSpell().CurrentAmmo : 0;
	}
	float GetRange() const
	{
		return Range != .0f ? Range : SDKSpell().CastRange;
	}
	float GetDelay() const
	{
		return Delay != .0f ? Delay : SDKSpell().TotalDelay;
	}
	float GetSpeed() const
	{
		return Speed != .0f ? Speed : SDKSpell().MissileSpeed;
	}
	float GetWidth() const
	{
		return Width != .0f ? Width : SDKSpell().LineWidth;
	}
	bool IsInRange(shared_ptr<AIClient>& Target) const
	{
		return pSDK->Player->GetPosition().GetDistance(Target->GetPosition()) <= GetRange();
	}
	bool IsInRange(Vector3 Target) const
	{
		return pSDK->Player->GetPosition().GetDistance(Target) <= GetRange();
	}
	void Cast(void* Target, bool Release = false) const
	{
		if (!IsValid() || State() != SpellState::Ready)
			return;

		gSdkContext->_SdkCastSpellLocalPlayer(Target, nullptr, static_cast<unsigned char>(Slot), Release);
	}
	void Cast(Vector3 Position, bool Release = false) const
	{
		if (!IsValid() || State() != SpellState::Ready)
			return;

		SDKVECTOR sdkPosition = Position.ToSDK();
		gSdkContext->_SdkCastSpellLocalPlayer(nullptr, &sdkPosition, static_cast<unsigned char>(Slot), Release);
	}
	void Cast(shared_ptr<AIClient>& Target, bool Release = false) const
	{
		Cast(Target->Object, Release);
	}
};