#pragma once

#include <Windows.h>
#include "../sdkapi.h"
#include <vector>
#include "IEntityManager.h"
#include <map>

enum ObjectType {
	typeAIHeroClient = 1,
	typeAIMinion = 3,
	typeAITurret = 6,
	typeObjGeneralParticleEmitter = 8,
	typeMissileClient = 10,
	typeObjBarracksDampener = 13,
	typeObjHQ = 21,
	typeLevelPropGameObject = 23,
	typeObjShop = 25,
};

class ObjectManager
{
public:
	static std::vector<SDK_ITEM> LocalItems;
	static std::vector<sBuffInfo> LocalBuffsCache;

	static std::map<unsigned int, std::shared_ptr<AIHeroClient>> AllyHeroes;
	static std::map<unsigned int, std::shared_ptr<AIClient>> AllyMinions;
	static std::map<unsigned int, std::shared_ptr<AIClient>> AllyInhibitors;
	static std::map<unsigned int, std::shared_ptr<AITurretClient>> AllyTurrets;
	static std::map<unsigned int, std::shared_ptr<GameObject>> Ally;

	static std::map<unsigned int, std::shared_ptr<AIHeroClient>> EnemyHeroes;
	static std::map<unsigned int, std::shared_ptr<AIClient>> EnemyMinions;
	static std::map<unsigned int, std::shared_ptr<AIClient>> EnemyInhibitors;
	static std::map<unsigned int, std::shared_ptr<AITurretClient>> EnemyTurrets;
	static std::map<unsigned int, std::shared_ptr<GameObject>> Enemy;

	static std::map<unsigned int, std::shared_ptr<AIHeroClient>> Heroes;
	static std::map<unsigned int, std::shared_ptr<AIClient>> Inhibitors;
	static std::map<unsigned int, std::shared_ptr<AIClient>> Nexus;
	static std::map<unsigned int, std::shared_ptr<AIClient>> Jungle;
	static std::map<unsigned int, std::shared_ptr<AIClient>> Minions;
	static std::map<unsigned int, std::shared_ptr<AITurretClient>> Turrets;

	static std::vector<std::shared_ptr<AIClient>> GetMinions(bool ally, bool enemy, float range);
	static std::vector<std::shared_ptr<AIClient>> GetLaneMinions(bool ally, bool enemy, float range);
	static std::vector<std::shared_ptr<AIClient>> GetJungle(float range);
	static std::vector<std::shared_ptr<AIHeroClient>> GetHeroes(bool ally, bool enemy, float range);
	static std::vector<std::shared_ptr<AITurretClient>> GetTurrets(bool ally, bool enemy, float range);
	static std::vector<std::shared_ptr<AIClient>> GetInhibitors(bool ally, bool enemy, float range);
	static std::vector<std::shared_ptr<AIClient>> GetNexus(bool ally, bool enemy, float range);

	static void Initialize();
};
